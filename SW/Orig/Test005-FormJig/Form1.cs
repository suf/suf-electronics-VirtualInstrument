﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;
using SUF.Instrument.HP3478A;

namespace Test005_FormJig
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            InstrumentSettingsFormBase form = new FormHP3478AInstrumentSettings();
            form.Init();
            form.ShowDialog();
            form.Dispose();
        }
    }
}
