﻿namespace SUF.Instrument.AvrGPIB
{
    partial class FormAvrGPIBConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOwnAddr = new System.Windows.Forms.Label();
            this.numOwnAddr = new System.Windows.Forms.NumericUpDown();
            this.chkOwnAddrAuto = new System.Windows.Forms.CheckBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSerialSettings = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.tbxName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numOwnAddr)).BeginInit();
            this.SuspendLayout();
            // 
            // lblOwnAddr
            // 
            this.lblOwnAddr.AutoSize = true;
            this.lblOwnAddr.Location = new System.Drawing.Point(9, 48);
            this.lblOwnAddr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOwnAddr.Name = "lblOwnAddr";
            this.lblOwnAddr.Size = new System.Drawing.Size(93, 13);
            this.lblOwnAddr.TabIndex = 0;
            this.lblOwnAddr.Text = "Interface Address:";
            // 
            // numOwnAddr
            // 
            this.numOwnAddr.Location = new System.Drawing.Point(106, 47);
            this.numOwnAddr.Margin = new System.Windows.Forms.Padding(2);
            this.numOwnAddr.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numOwnAddr.Name = "numOwnAddr";
            this.numOwnAddr.Size = new System.Drawing.Size(34, 20);
            this.numOwnAddr.TabIndex = 1;
            // 
            // chkOwnAddrAuto
            // 
            this.chkOwnAddrAuto.AutoSize = true;
            this.chkOwnAddrAuto.Location = new System.Drawing.Point(145, 48);
            this.chkOwnAddrAuto.Margin = new System.Windows.Forms.Padding(2);
            this.chkOwnAddrAuto.Name = "chkOwnAddrAuto";
            this.chkOwnAddrAuto.Size = new System.Drawing.Size(185, 17);
            this.chkOwnAddrAuto.TabIndex = 2;
            this.chkOwnAddrAuto.Text = "Automatic (Get from the Interface)";
            this.chkOwnAddrAuto.UseVisualStyleBackColor = true;
            this.chkOwnAddrAuto.CheckedChanged += new System.EventHandler(this.chkOwnAddrAuto_CheckedChanged);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(11, 86);
            this.btnOk.Margin = new System.Windows.Forms.Padding(2);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(56, 25);
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(72, 86);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(56, 25);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnSerialSettings
            // 
            this.btnSerialSettings.Location = new System.Drawing.Point(216, 86);
            this.btnSerialSettings.Margin = new System.Windows.Forms.Padding(2);
            this.btnSerialSettings.Name = "btnSerialSettings";
            this.btnSerialSettings.Size = new System.Drawing.Size(97, 25);
            this.btnSerialSettings.TabIndex = 10;
            this.btnSerialSettings.Text = "&Serial settings";
            this.btnSerialSettings.UseVisualStyleBackColor = true;
            this.btnSerialSettings.Click += new System.EventHandler(this.BtnSerialSettings_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(8, 13);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 11;
            this.lblName.Text = "Name:";
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(57, 10);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(265, 20);
            this.tbxName.TabIndex = 12;
            // 
            // FormAvrGPIBConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 123);
            this.Controls.Add(this.tbxName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.btnSerialSettings);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.chkOwnAddrAuto);
            this.Controls.Add(this.numOwnAddr);
            this.Controls.Add(this.lblOwnAddr);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FormAvrGPIBConfig";
            this.Text = "GPIB Interface";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAvrGPIBConfig_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numOwnAddr)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOwnAddr;
        private System.Windows.Forms.NumericUpDown numOwnAddr;
        private System.Windows.Forms.CheckBox chkOwnAddrAuto;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSerialSettings;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox tbxName;
    }
}