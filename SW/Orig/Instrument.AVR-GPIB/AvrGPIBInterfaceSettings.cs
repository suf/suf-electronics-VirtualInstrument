﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO.Ports;

using SUF.Instrument.Serial;

namespace SUF.Instrument.AvrGPIB
{
    public class AvrGPIBInterfaceSettings
    {
        public AvrGPIBInterfaceSettings()
        {
            this.Address = -1;
            this.port = new SerialPort();
            this.port.BaudRate = 115200;
            this.port.NewLine = "\r\n";
        }
        private AvrGPIB _interface;
        public string Name;
        public SerialPort port;
        public int Address;
        public AvrGPIB GetInterface()
        {
            if(this._interface == null)
            {
                this._interface = new AvrGPIB(this.port, this.Address);
            }
            return this._interface;
        }
        public override string ToString()
        {
            return this.Name;
        }
    }
}
