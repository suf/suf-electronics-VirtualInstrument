﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument.AvrGPIB
{
    public sealed class AvrGPIBInterfaceCollection : IEnumerable<AvrGPIBInterfaceSettings>
    {
        private AvrGPIBInterfaceCollection()
        {
            this._interfaces = new Dictionary<int, AvrGPIBInterfaceSettings>();
        }
        private static readonly Lazy<AvrGPIBInterfaceCollection> lazy = new Lazy<AvrGPIBInterfaceCollection>(() => new AvrGPIBInterfaceCollection());
        public static AvrGPIBInterfaceCollection GetAvrGPIBInterfaceCollection
        {
            get
            {
                return lazy.Value;
            }
        }
        private int _currentId = 0;
        private Dictionary<int,AvrGPIBInterfaceSettings> _interfaces;
        public int Add(AvrGPIBInterfaceSettings GPIBInterface)
        {
            int id = this._currentId++;
            this._interfaces.Add(id, GPIBInterface);
            return id;
        }
        public void Remove(int id)
        {
            if(this._interfaces.ContainsKey(id))
            {
                // if the interface has connections it can't be deleted. The check must be implemented later.
                this._interfaces.Remove(id);
            }
        }
        public int GetIdByConnection(AvrGPIBInterfaceSettings IfSettings)
        {

            foreach (int key in this._interfaces.Keys)
            {
                if (this._interfaces[key] == IfSettings)
                {
                    return key;
                }
            }
            return -1;
        }


        public IEnumerator<AvrGPIBInterfaceSettings> GetEnumerator()
        {
            return this._interfaces.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._interfaces.Values.GetEnumerator();
        }
    }
}
