﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;
using SUF.Instrument.Serial;

namespace SUF.Instrument.AvrGPIB
{
    public partial class FormAvrGPIBConnection : ConnectionFormBase
    {
        // public new AvrGPIBConnectionSettings Settings;
        // public bool IsSaved = false;
        public FormAvrGPIBConnection(AvrGPIBConnectionSettings settings)
        {
            InitializeComponent();
            if (settings != null)
            {
                this.Settings = settings;
            }
            else
            {
                this.Settings = new AvrGPIBConnectionSettings();
                // !!!! Register into the Connection list singleton - not yet implemented !!!!
            }
            this.UpdateInterfaces();
            this.chkDevSADEnable.Checked = ((AvrGPIBConnectionSettings)this.Settings).SAD > 0;
            this.numDevSAD.Enabled = this.chkDevSADEnable.Checked;
            if (((AvrGPIBConnectionSettings)this.Settings).SAD > 0)
            {
                this.numDevSAD.Value = ((AvrGPIBConnectionSettings)this.Settings).SAD - 96;
            }
            this.numDevPAD.Value = ((AvrGPIBConnectionSettings)this.Settings).PAD;
            tbxName.Text = ((AvrGPIBConnectionSettings)this.Settings).Name;
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if (this.cbxInterfaces.SelectedItem.GetType() == typeof(AvrGPIBInterfaceSettings))
            {
                ((AvrGPIBConnectionSettings)this.Settings).SAD = this.chkDevSADEnable.Checked ? Convert.ToInt32(this.numDevSAD.Value + 96) : 0;
                ((AvrGPIBConnectionSettings)this.Settings).PAD = Convert.ToInt32(this.numDevPAD.Value);
                ((AvrGPIBConnectionSettings)this.Settings).GPIBInterface = (AvrGPIBInterfaceSettings)this.cbxInterfaces.SelectedItem;
                ((AvrGPIBConnectionSettings)this.Settings).Name = this.tbxName.Text;
                this.IsSaved = true;
            }
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGPIBInterface_Click(object sender, EventArgs e)
        {
            AvrGPIBInterfaceSettings GPIBInterface;
            GPIBInterface = this.cbxInterfaces.SelectedItem.GetType() == typeof(AvrGPIBInterfaceSettings) ? (AvrGPIBInterfaceSettings)this.cbxInterfaces.SelectedItem : null;
            FormAvrGPIBConfig formAvrGPIBConfig = new FormAvrGPIBConfig(GPIBInterface);
            formAvrGPIBConfig.ShowDialog();
            if(formAvrGPIBConfig.IsSaved)
            {
                ((AvrGPIBConnectionSettings)this.Settings).GPIBInterface = formAvrGPIBConfig.Settings;
                UpdateInterfaces();
            }
            formAvrGPIBConfig.Dispose();
        }
        public void UpdateInterfaces()
        {
            this.cbxInterfaces.Items.Clear();
            this.cbxInterfaces.SelectedIndex = this.cbxInterfaces.Items.Add("<...NEW...>");
            int ItemID;
            foreach (AvrGPIBInterfaceSettings GPIBInterface in AvrGPIBInterfaceCollection.GetAvrGPIBInterfaceCollection)
            {
                ItemID = this.cbxInterfaces.Items.Add(GPIBInterface);
                if(GPIBInterface == ((AvrGPIBConnectionSettings)this.Settings).GPIBInterface)
                {
                    this.cbxInterfaces.SelectedIndex = ItemID;
                }
            }
        }

        private void chkDevSADEnable_CheckedChanged(object sender, EventArgs e)
        {
            this.numDevSAD.Enabled = this.chkDevSADEnable.Checked;
        }
    }
}
