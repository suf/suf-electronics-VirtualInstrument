﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;
using System.IO.Ports;

namespace SUF.Instrument.AvrGPIB
{
    public class AvrGPIB : IDisposable
    {
        private SerialPort _com;
        private int _ownAddress; // We need to know our own address to avoid own/remote conflict
        private int _currentPAD = -1;
        private int _currentSAD = -1;
        private int _readTimeoutMs = 200;
        private bool _Lock = true;
        public AvrGPIB() { }
        public AvrGPIB(SerialPort com)
        {
            this.Init(com, -1);
        }
        public AvrGPIB(SerialPort com, int OwnAddress)
        {
            this.Init(com, OwnAddress);
        }

        private void Init(SerialPort com, int OwnAddress)
        {
            this._com = com;
            this._com.NewLine = "\r\n";
            // connect
            if (!this._com.IsOpen)
            {
                this._com.Open();
            }
            // switch off autosave
            _com.WriteLine("++savecfg 0");
            _com.WriteLine("++echo 0");
            _com.WriteLine("++dbg_level 3");
            _com.WriteLine("++eot_enable 1");
            _com.WriteLine("++eot_char 0");
            _com.WriteLine("++read_tmo_ms 200");
            // Clear buffer
            _com.ReadExisting();
            // switch off echo
            // get/set local address
            if (OwnAddress == -1)
            {
                _com.WriteLine("++addr_own");
                this._ownAddress = Int32.Parse(_com.ReadLine());
            }
            else
            {
                this._ownAddress = OwnAddress;
                _com.WriteLine("++addr_own " + this._ownAddress.ToString());
            }
            this._Lock = false;
        }
        public InstrumentRetCode Send(int PAD, int SAD, string Command)
        {
            if (!this._Lock)
            {
                this._Lock = true;
                if (this._ownAddress == PAD)
                {
                    this._Lock = false;
                    return InstrumentRetCode.AddressConflict;
                }
                if (this._currentPAD != PAD || this._currentPAD != SAD)
                {
                    this._com.WriteLine("++addr_rem " + PAD.ToString() + (SAD != 0 ? " " + SAD.ToString() : ""));
                    this._currentPAD = PAD;
                    this._currentSAD = SAD;
                }
                if (Command != "")
                {
                    _com.WriteLine(Command);
                }
            }
            else
            {
                return InstrumentRetCode.Busy;
            }
            this._Lock = false;
            return InstrumentRetCode.OK;
        }
        public InstrumentRetCode Read(int PAD, int SAD, string Command, out string value)
        {
            string readValue;
            if (!this._Lock)
            {
                this._Lock = true;
                value = "";
                if(this._ownAddress == PAD)
                {
                    this._Lock = false;
                    return InstrumentRetCode.AddressConflict;
                }
                if (this._currentPAD != PAD || this._currentPAD != SAD)
                {
                    this._com.WriteLine("++addr_rem " + PAD.ToString() + (SAD != 0 ? " " + SAD.ToString() : ""));
                    this._currentPAD = PAD;
                    this._currentSAD = SAD;
                }
                if(Command != "")
                {
                    _com.WriteLine(Command);
                }
                do
                {
                    readValue = "";
                    _com.WriteLine("++read eoi");
                    Thread.Sleep(this._readTimeoutMs);
                    if (_com.BytesToRead > 0)
                    {
                        readValue = _com.ReadTo("\r\n").TrimEnd('\r', '\n');
                    }
                }
                while (readValue == "");
            }
            else
            {
                value = "";
                return InstrumentRetCode.Busy;
            }
            value = readValue;
            this._Lock = false;
            return InstrumentRetCode.OK;
        }

        public void Dispose()
        {
            if(this._com.IsOpen)
            {
                this._com.Close();
            }
        }
    }
}
