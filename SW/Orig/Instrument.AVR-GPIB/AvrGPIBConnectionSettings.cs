﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SUF.Instrument;
using SUF.Instrument.Serial;

namespace SUF.Instrument.AvrGPIB
{
    [PluginDescriptor("Avr GPIB")]
    [PluginSettingsClass("SUF.Instrument.AvrGPIB.FormAvrGPIBConnection")]
    public class AvrGPIBConnectionSettings : ConnectionSettings
    {
        public AvrGPIBConnectionSettings()
        {

        }
        public AvrGPIBInterfaceSettings GPIBInterface;
        public int PAD;
        public int SAD;

        public string Name;

        private AvrGPIBConnection _connection;
        // private int _connectionID;

        public override Connection GetConnection()
        {
            /*
            AvrGPIB gpib = this.GPIBInterface.GetInterface();
            // handle the singleton object
            if (this._connection == null)
            {
                foreach (ConnectionSettings conn in ConnectionSettingsCollection.GetConnectionSettingCollection)
                {
                    if (conn.GetType() == typeof(AvrGPIBConnection))
                    {
                        if (((AvrGPIBConnectionSettings)conn).PAD == this.PAD && ((AvrGPIBConnectionSettings)conn).SAD == this.SAD && ((AvrGPIBConnection)conn).GPIBInterface == this._connection.GPIBInterface)
                        {
                            this._connectionID = ConnectionSettingsCollection.GetConnectionSettingCollection.GetIdByConnection(conn);
                            this._connection = (AvrGPIBConnection)conn;
                            return conn;
                        }
                    }
                }
                this._connection = new AvrGPIBConnection(gpib,this.PAD,this.SAD);
                this._connectionID = ConnectionSettingsCollection.GetConnectionSettingCollection.Add(this._connection);
            }
            */
            return this._connection;
        }
        public override bool Equals(object obj)
        {
            if(obj.GetType() == typeof(AvrGPIBConnectionSettings))
            {
                return ((AvrGPIBConnectionSettings)obj).PAD == this.PAD && ((AvrGPIBConnectionSettings)obj).SAD == this.SAD && ((AvrGPIBConnectionSettings)obj).GPIBInterface == this.GPIBInterface;
            }
            return base.Equals(obj);
        }
        public override string ToString()
        {
            string ifName = "unknown";
            if(this.Name != null && this.Name != "")
            {
                return this.Name;
            }
            if(this.GPIBInterface != null)
            {
                if(this.GPIBInterface.Name != null)
                {
                    ifName = this.GPIBInterface.Name;
                }
            }
            return "AvrGPIB:" + ifName + ":" + this.PAD.ToString() + (this.SAD > 0 ? ":" + this.SAD.ToString() : "");
        }
    }
}
