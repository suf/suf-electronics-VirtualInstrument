﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO.Ports;

using SUF.Instrument;
using SUF.Instrument.Serial;

namespace SUF.Instrument.AvrGPIB
{
    public partial class FormAvrGPIBConfig : Form
    {
        private int _interfaceId = -1;
        public AvrGPIBInterfaceSettings Settings;
        public bool IsSaved = false;
        public FormAvrGPIBConfig(AvrGPIBInterfaceSettings settings)
        {
            InitializeComponent();
            if (settings != null)
            {
                this.Settings = settings;
                this.tbxName.Text = settings.Name;
            }
            else
            {
                this.Settings = new AvrGPIBInterfaceSettings();
                this._interfaceId = AvrGPIBInterfaceCollection.GetAvrGPIBInterfaceCollection.Add(this.Settings);
                this.tbxName.Text = "GPIB:" + this._interfaceId.ToString();
            }
            this.chkOwnAddrAuto.Checked = this.Settings.Address < 0;
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            this.Settings.Address = this.chkOwnAddrAuto.Checked ? -1 : Convert.ToInt32(this.numOwnAddr.Value);
            this.Settings.Name = this.tbxName.Text;
            this.IsSaved = true;
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSerialSettings_Click(object sender, EventArgs e)
        {
            FormSerialConfig serialConfig = new FormSerialConfig(this.Settings.port);
            serialConfig.ShowDialog();
            if(serialConfig.IsSaved)
            {
                this.Settings.port = serialConfig.Com;
            }
            serialConfig.Dispose();
        }

        private void chkOwnAddrAuto_CheckedChanged(object sender, EventArgs e)
        {
            this.numOwnAddr.Enabled = !this.chkOwnAddrAuto.Checked;
        }

        private void FormAvrGPIBConfig_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this._interfaceId > -1 && !this.IsSaved)
            {
                AvrGPIBInterfaceCollection.GetAvrGPIBInterfaceCollection.Remove(this._interfaceId);
            }
        }
    }
}
