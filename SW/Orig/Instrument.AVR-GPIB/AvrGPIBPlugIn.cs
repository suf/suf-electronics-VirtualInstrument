﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

using SUF.Instrument;

namespace SUF.Instrument.AvrGPIB
{
    public class AvrGPIBPlugIn : PlugIn
    {
        public AvrGPIBPlugIn() { }
        public override Type typeSettings
        {
            get
            {
                return typeof(AvrGPIBConnectionSettings);
            }
        }

        public override ConnectionFormBase SettingsDialog(ConnectionSettings settings)
        {
            return new FormAvrGPIBConnection((AvrGPIBConnectionSettings)settings);
        }
        public override string ToString()
        {
            return "AvrGPIB - Arduino IEEE488 / HP - IB / GP - IB";
        }
    }
}
