﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SUF.Instrument;


namespace SUF.Instrument.AvrGPIB
{
    public class AvrGPIBConnection : Connection
    {
        private AvrGPIB _gpib;
        private int _PAD;
        private int _SAD;

        public int PAD
        {
            get
            {
                return this._PAD;
            }
        }
        public int SAD
        {
            get
            {
                return this._SAD;
            }
        }
        public AvrGPIB GPIBInterface
        {
            get
            {
                return this._gpib;
            }
        }

        public AvrGPIBConnection(AvrGPIB gpib, int PAD, int SAD)
        {
            this._gpib = gpib;
            this._PAD = PAD;
            this._SAD = SAD;
        }

        public override bool IsOpen => throw new NotImplementedException();

        public override InstrumentRetCode Close()
        {
            throw new NotImplementedException();
        }

        public override InstrumentRetCode Open()
        {
            throw new NotImplementedException();
        }

        public override InstrumentRetCode Read(string Command, out string value)
        {
            return this._gpib.Read(this._PAD, this._SAD, Command, out value);
        }

        public override InstrumentRetCode Send(string Command)
        {
            return this._gpib.Send(this._PAD, this._SAD, Command);
        }
    }
}
