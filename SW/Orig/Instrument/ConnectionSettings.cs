﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Connection settings base class
    /// </summary>
    public abstract class ConnectionSettings
    {
        public abstract Connection GetConnection();
    }
}
