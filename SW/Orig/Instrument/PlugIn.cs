﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace SUF.Instrument
{
    /// <summary>
    /// Instrument Interface plugin (deprecated)
    /// </summary>
    public abstract class PlugIn
    {
        public abstract Type typeSettings { get; }
        public abstract ConnectionFormBase SettingsDialog(ConnectionSettings settings);
    }
}
