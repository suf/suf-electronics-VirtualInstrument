﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    public enum InstrumentRetCode
    {
        OK = 0,
        Busy = 1,
        AddressConflict = 2,
        NotImplemented = 3
    }
}
