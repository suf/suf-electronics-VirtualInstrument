﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Collection of the controller plugins
    /// </summary>
    public sealed class ControllerTypeCollection : IEnumerable<ControllerPlugin>
    {
        private ControllerTypeCollection()
        {
            this.ControllerTypes = new List<ControllerPlugin>();
        }
        private static readonly Lazy<ControllerTypeCollection> lazy = new Lazy<ControllerTypeCollection>(() => new ControllerTypeCollection());
        public static ControllerTypeCollection GetControllerTypeCollection
        {
            get
            {
                return lazy.Value;
            }
        }
        public List<ControllerPlugin> ControllerTypes;
        public int Add(Type controllerType, string descriptor)
        {
            ControllerTypes.Add(new ControllerPlugin(controllerType, descriptor));
            return ControllerTypes.Count - 1;
        }
        public void AddSettingsType(int ControllerID, Type SettingsType)
        {
            ControllerTypes[ControllerID].SetSettingsType(SettingsType);
        }
        public IEnumerator<ControllerPlugin> GetEnumerator()
        {
            return this.ControllerTypes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.ControllerTypes.GetEnumerator();
        }

    }
    public class ControllerPlugin
    {
        public ControllerPlugin(Type controllerType, string descriptor)
        {
            _controllerType = controllerType;
            _descriptor = descriptor;
        }
        private Type _controllerType;
        private Type _controllerSettingsType;
        private string _descriptor;
        public string Name
        {
            get
            {
                return (_descriptor != null) ? _descriptor : _controllerType.ToString();
            }
        }
        public Type ControllerType
        {
            get
            {
                return this._controllerType;
            }
        }
        public Controller GetController()
        {
            return (Controller)Activator.CreateInstance(_controllerType);
        }
        public ControllerSettingsFormBase GetSettingsForm()
        {
            if (this._controllerSettingsType != null)
            {
                return (ControllerSettingsFormBase)Activator.CreateInstance(_controllerSettingsType);
            }
            else
            {
                return null;
            }
        }
        public void SetSettingsType(Type settingsType)
        {
            this._controllerSettingsType = settingsType;
        }
        public override string ToString()
        {
            return Name;
        }
        public ControllerPlugin Value
        {
            get
            {
                return this;
            }
        }
    }
}
