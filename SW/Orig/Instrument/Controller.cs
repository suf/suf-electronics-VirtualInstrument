﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Controller base class
    /// </summary>
    public class Controller : ResultProvider
    {
        public List<Instrument> instruments = new List<Instrument>();
        public List<Filter> filters = new List<Filter>();
        public List<ITarget> targets = new List<ITarget>();
        public List<ResultDescriptor> resultDescriptors = new List<ResultDescriptor>();
        public bool Chain;
        public bool Async;
        public bool IsRunning = false;
        public virtual void Start() { }
        public virtual void Stop() { }
        /// <summary>
        /// This method copy all of the parameters from another controller object
        /// </summary>
        /// <param name="source"></param>
        public void FromController(Controller source)
        {
            base._guid = source._guid;
            base.Name = source.Name;

            this.instruments = source.instruments;
            this.filters = source.filters;
            this.targets = source.targets;
            // generated - no copy needed
            // this.resultDescriptors = source.resultDescriptors;
            this.Chain = source.Chain;
            this.Async = source.Async;
        }
        public List<ResultDescriptor> GetAvailableDescriptors(Guid ConsumerID)
        {
            // This is used only by the Instruments and Filters, where just a subset of results available based on the execution order
            List<ResultDescriptor> AvailableDescriptors = new List<ResultDescriptor>();
            foreach(ResultDescriptor current in this.resultDescriptors)
            {
                if(current.SourceGuid == ConsumerID)
                {
                    return AvailableDescriptors;
                }
                else
                {
                    AvailableDescriptors.Add(current);
                }
            }
            return AvailableDescriptors;
        }
        public Controller() { }
    }
}
