﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    public interface ITarget
    {
        void Init(List<ResultDescriptor> resultDescriptors);
        void SetName(string name);
        string GetName();
    }
}
