﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace SUF.Instrument
{
    public class ControllerSettingsFormBase : Form
    {
        public Controller controller;
        public bool IsSaved = false;
        public virtual void Init(Controller controller) { }
    }
}
