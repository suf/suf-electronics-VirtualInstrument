﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;

namespace SUF.Instrument
{
    /// <summary>
    /// Connection setting form base class
    /// </summary>
    public class ConnectionFormBase : Form
    {
        public ConnectionSettings Settings;
        public bool IsSaved = false;
    }
}
