﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Collection of the target plugins
    /// </summary>
    public sealed class TargetTypeCollection : IEnumerable<TargetPlugin>
    {
        private TargetTypeCollection()
        {
            this.TargetTypes = new List<TargetPlugin>();
        }
        private static readonly Lazy<TargetTypeCollection> lazy = new Lazy<TargetTypeCollection>(() => new TargetTypeCollection());
        public static TargetTypeCollection GetTargetTypeCollection
        {
            get
            {
                return lazy.Value;
            }
        }
        public List<TargetPlugin> TargetTypes;
        public int Add(Type targetType, string descriptor)
        {
            TargetTypes.Add(new TargetPlugin(targetType, descriptor));
            return TargetTypes.Count - 1;
        }
        public TargetPlugin GetTargetPlugin(Type TargetType)
        {
            foreach (TargetPlugin plugin in this.TargetTypes)
            {
                if (TargetType == plugin.TargetType)
                {
                    return plugin;
                }
            }
            return null;
        }
        public void AddSettingsType(int TargetID, Type SettingsType)
        {
            TargetTypes[TargetID].SetSettingsType(SettingsType);
        }
        public IEnumerator<TargetPlugin> GetEnumerator()
        {
            return this.TargetTypes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.TargetTypes.GetEnumerator();
        }

    }
    public class TargetPlugin
    {
        public TargetPlugin(Type targetType, string descriptor)
        {
            _targetType = targetType;
            _descriptor = descriptor;
        }
        private Type _targetType;
        private Type _targetSettingsType;
        private string _descriptor;
        public string Name
        {
            get
            {
                return (_descriptor != null) ? _descriptor : _targetType.ToString();
            }
        }
        public Type TargetType
        {
            get
            {
                return this._targetType;
            }
        }
        public ITarget GetTarget()
        {
            return (ITarget)Activator.CreateInstance(_targetType);
        }
        public TargetSettingsFormBase GetSettingsForm()
        {
            if (this._targetSettingsType != null)
            {
                return (TargetSettingsFormBase)Activator.CreateInstance(_targetSettingsType);
            }
            else
            {
                return null;
            }
        }
        public void SetSettingsType(Type settingsType)
        {
            this._targetSettingsType = settingsType;
        }
        public override string ToString()
        {
            return Name;
        }
        public TargetPlugin Value
        {
            get
            {
                return this;
            }
        }
    }

}
