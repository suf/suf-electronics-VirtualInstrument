﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Base type of the instrument (measuring) objects
    /// </summary>
    public class Instrument : ResultProvider
    {
        public Connection conn;
        public virtual void Measure(MeasurementObject results)
        { }
        public virtual void Init()
        { }
    }
}
