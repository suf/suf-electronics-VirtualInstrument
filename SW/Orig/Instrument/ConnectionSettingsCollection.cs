﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Collection of the connection settings
    /// </summary>
    public sealed class ConnectionSettingsCollection : IEnumerable<ConnectionSettings>
    {
        private ConnectionSettingsCollection()
        {
            this._connectionSettings = new Dictionary<int, ConnectionSettings>();
        }
        private static readonly Lazy<ConnectionSettingsCollection> lazy = new Lazy<ConnectionSettingsCollection>(() => new ConnectionSettingsCollection());
        public static ConnectionSettingsCollection GetConnectionSettingCollection
        {
            get
            {
                return lazy.Value;
            }
        }
        private int _currentId = 0;
        private Dictionary<int, ConnectionSettings> _connectionSettings;
        public int Add(ConnectionSettings connSettings)
        {
            int id = GetIdByConnection(connSettings);
            if (id == -1)
            {
                id = this._currentId++;
                this._connectionSettings.Add(id, connSettings);
            }
            return id;
        }
        public void Remove(int id)
        {
            if (this._connectionSettings.ContainsKey(id))
            {
                this._connectionSettings.Remove(id);
            }
        }
        public int GetIdByConnection(ConnectionSettings connSettings)
        {

            foreach(int key in this._connectionSettings.Keys)
            {
                if(this._connectionSettings[key] == connSettings || this._connectionSettings[key].Equals(connSettings))
                {
                    return key;
                }
            }
            return -1;
        }

        public IEnumerator<ConnectionSettings> GetEnumerator()
        {
            return this._connectionSettings.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._connectionSettings.Values.GetEnumerator();
        }
    }

}
