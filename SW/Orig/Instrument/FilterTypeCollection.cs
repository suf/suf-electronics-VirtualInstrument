﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Collection of the Filter plugins
    /// </summary>
    public sealed class FilterTypeCollection : IEnumerable<FilterPlugin>
    {
        private FilterTypeCollection()
        {
            this.FilterTypes = new List<FilterPlugin>();
        }
        private static readonly Lazy<FilterTypeCollection> lazy = new Lazy<FilterTypeCollection>(() => new FilterTypeCollection());
        public static FilterTypeCollection GetFilterTypeCollection
        {
            get
            {
                return lazy.Value;
            }
        }
        public List<FilterPlugin> FilterTypes;
        public int Add(Type filterType, string descriptor)
        {
            FilterTypes.Add(new FilterPlugin(filterType, descriptor));
            return FilterTypes.Count - 1;
        }
        public FilterPlugin GetFilterPlugin(Type FilterType)
        {
            foreach (FilterPlugin plugin in this.FilterTypes)
            {
                if (FilterType == plugin.FilterType)
                {
                    return plugin;
                }
            }
            return null;
        }
        public void AddSettingsType(int FilterID, Type SettingsType)
        {
            FilterTypes[FilterID].SetSettingsType(SettingsType);
        }
        public IEnumerator<FilterPlugin> GetEnumerator()
        {
            return this.FilterTypes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.FilterTypes.GetEnumerator();
        }

    }
    public class FilterPlugin
    {
        public FilterPlugin(Type filterType, string descriptor)
        {
            _filterType = filterType;
            _descriptor = descriptor;
        }
        private Type _filterType;
        private Type _filterSettingsType;
        private string _descriptor;
        public string Name
        {
            get
            {
                return (_descriptor != null) ? _descriptor : _filterType.ToString();
            }
        }
        public Type FilterType
        {
            get
            {
                return this._filterType;
            }
        }
        public Filter GetFilter()
        {
            return (Filter)Activator.CreateInstance(_filterType);
        }
        public FilterSettingsFormBase GetSettingsForm()
        {
            if (this._filterSettingsType != null)
            {
                return (FilterSettingsFormBase)Activator.CreateInstance(_filterSettingsType);
            }
            else
            {
                return null;
            }
        }
        public void SetSettingsType(Type settingsType)
        {
            this._filterSettingsType = settingsType;
        }
        public override string ToString()
        {
            return Name;
        }
        public FilterPlugin Value
        {
            get
            {
                return this;
            }
        }
    }
}
