﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    public abstract class Connection
    {
        public abstract InstrumentRetCode Read(string Command, out string value);
        // public abstract InstrumentRetCode Read(out string value);
        public abstract InstrumentRetCode Send(string Command);
        public abstract InstrumentRetCode Open();
        public abstract InstrumentRetCode Close();
        public abstract bool IsOpen{get;}
    }
}
