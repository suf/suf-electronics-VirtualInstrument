﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    public class MeasurementObject
    {
        public MeasurementObject()
        {
            this.Values = new Dictionary<int,MeasurementValue>();
        }
        public DateTime TimeStamp;
        public int SequenceNumber;
        public Dictionary<int, MeasurementValue> Values;
    }

    public class MeasurementValue
    {
        public MeasurementValue(double value, MetricPrefix prefix)
        {
            // convert from prefix
            this.value = value * Math.Pow(10, Convert.ToDouble(prefix));
        }

        public double value;
        public override string ToString()
        {
            // calculate the prefix
            // use logarithm to find the correct value
            return base.ToString();
        }
        public string ToString(int digits)
        {
            // calculate the prefix
            return base.ToString();
        }
    }
    public class ResultDescriptor
    {
        public ResultDescriptor() { }
        public Guid SourceGuid;
        public int ChannelID;
        public string SourceName;
        public string ChannelName;
        public MeasureUnit unit;
        // public string Description;
        // public string Name;
        public string Name
        {
            get
            {
                return SourceName + " | " + ChannelName;
            }
        }
        public override string ToString()
        {
            return this.Name;
        }
    }

    public enum MetricPrefix
    {
        [Description("y")]
        yocto = -24,
        [Description("z")]
        zepto = -21,
        [Description("a")]
        atto = -18,
        [Description("f")]
        femto = -15,
        [Description("p")]
        pico = -12,
        [Description("n")]
        nano = -9,
        [Description("u")]
        micro = -6,
        [Description("m")]
        milli = -3,
        [Description("c")]
        centi = -2,
        [Description("d")]
        deci = -1,
        [Description("")]
        none = 0,
        [Description("da")]
        deca = 1,
        [Description("y")]
        hecto = 2,
        [Description("k")]
        kilo = 3,
        [Description("M")]
        mega = 6,
        [Description("G")]
        giga = 9,
        [Description("T")]
        tera = 12,
        [Description("P")]
        peta = 15,
        [Description("E")]
        exa = 18,
        [Description("Z")]
        zeta = 21,
        [Description("Y")]
        yotta = 24
    }
    public enum MeasureUnit
    {
        [Description("Unknown")]
        unknown,
        [Description("V")]
        volt,
        [Description("A")]
        ampere,
        [Description("hz")]
        hertz,
        [Description("ohm")]
        ohm,
        [Description("F")]
        farad,
        [Description("H")]
        henry,
        [Description("°K")]
        kelvin,
        [Description("°F")]
        farenheit,
        [Description("°C")]
        celsius,
        [Description("°")]
        degree,
        [Description("%")]
        percent,
        [Description("dB")]
        decibel,
        [Description("dBv")]
        dBv,
        [Description("dBm")]
        dBm,
        [Description("W")]
        watt,
        [Description("s")]
        second
    }
}
