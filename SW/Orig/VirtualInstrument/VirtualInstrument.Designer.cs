﻿namespace VirtualInstrument
{
    partial class frmVirtualInstrument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.avrGPIBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblController = new System.Windows.Forms.Label();
            this.lblInstrument = new System.Windows.Forms.Label();
            this.lblTargets = new System.Windows.Forms.Label();
            this.dgvController = new System.Windows.Forms.DataGridView();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colControllerType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colAction = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colControllerSettings = new System.Windows.Forms.DataGridViewButtonColumn();
            this.lblFilter = new System.Windows.Forms.Label();
            this.dgvInstrument = new System.Windows.Forms.DataGridView();
            this.colInstrumentName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInstrumentType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colInstrumentSettings = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgvFilter = new System.Windows.Forms.DataGridView();
            this.colFilterName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFilterType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colFilterSettings = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgvTarget = new System.Windows.Forms.DataGridView();
            this.colTargetName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTargetType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colTargetSettings = new System.Windows.Forms.DataGridViewButtonColumn();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstrument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTarget)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(504, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectionsToolStripMenuItem,
            this.avrGPIBToolStripMenuItem,
            this.serialToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "&Settings";
            // 
            // connectionsToolStripMenuItem
            // 
            this.connectionsToolStripMenuItem.Name = "connectionsToolStripMenuItem";
            this.connectionsToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.connectionsToolStripMenuItem.Text = "Connections";
            this.connectionsToolStripMenuItem.Click += new System.EventHandler(this.connectionsToolStripMenuItem_Click);
            // 
            // avrGPIBToolStripMenuItem
            // 
            this.avrGPIBToolStripMenuItem.Name = "avrGPIBToolStripMenuItem";
            this.avrGPIBToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.avrGPIBToolStripMenuItem.Text = "AvrGPIB";
            // 
            // serialToolStripMenuItem
            // 
            this.serialToolStripMenuItem.Name = "serialToolStripMenuItem";
            this.serialToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.serialToolStripMenuItem.Text = "Serial";
            // 
            // lblController
            // 
            this.lblController.AutoSize = true;
            this.lblController.Location = new System.Drawing.Point(9, 30);
            this.lblController.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblController.Name = "lblController";
            this.lblController.Size = new System.Drawing.Size(51, 13);
            this.lblController.TabIndex = 1;
            this.lblController.Text = "Controller";
            // 
            // lblInstrument
            // 
            this.lblInstrument.AutoSize = true;
            this.lblInstrument.Location = new System.Drawing.Point(9, 171);
            this.lblInstrument.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblInstrument.Name = "lblInstrument";
            this.lblInstrument.Size = new System.Drawing.Size(56, 13);
            this.lblInstrument.TabIndex = 2;
            this.lblInstrument.Text = "Instrument";
            // 
            // lblTargets
            // 
            this.lblTargets.AutoSize = true;
            this.lblTargets.Location = new System.Drawing.Point(9, 452);
            this.lblTargets.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTargets.Name = "lblTargets";
            this.lblTargets.Size = new System.Drawing.Size(43, 13);
            this.lblTargets.TabIndex = 3;
            this.lblTargets.Text = "Targets";
            // 
            // dgvController
            // 
            this.dgvController.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvController.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colName,
            this.colControllerType,
            this.colAction,
            this.colControllerSettings});
            this.dgvController.Location = new System.Drawing.Point(9, 46);
            this.dgvController.Margin = new System.Windows.Forms.Padding(2);
            this.dgvController.Name = "dgvController";
            this.dgvController.RowHeadersWidth = 51;
            this.dgvController.RowTemplate.Height = 24;
            this.dgvController.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvController.Size = new System.Drawing.Size(486, 122);
            this.dgvController.TabIndex = 4;
            this.dgvController.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvController_CellContentClick);
            this.dgvController.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvController_CellValueChanged);
            this.dgvController.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvController_RowEnter);
            // 
            // colName
            // 
            this.colName.HeaderText = "Name";
            this.colName.MinimumWidth = 125;
            this.colName.Name = "colName";
            this.colName.Width = 125;
            // 
            // colControllerType
            // 
            this.colControllerType.HeaderText = "Type";
            this.colControllerType.MinimumWidth = 125;
            this.colControllerType.Name = "colControllerType";
            this.colControllerType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colControllerType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colControllerType.Width = 125;
            // 
            // colAction
            // 
            this.colAction.HeaderText = "Action";
            this.colAction.MinimumWidth = 50;
            this.colAction.Name = "colAction";
            this.colAction.ReadOnly = true;
            this.colAction.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colAction.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colAction.Text = "Start";
            this.colAction.UseColumnTextForButtonValue = true;
            this.colAction.Width = 50;
            // 
            // colControllerSettings
            // 
            this.colControllerSettings.HeaderText = "Settings";
            this.colControllerSettings.MinimumWidth = 6;
            this.colControllerSettings.Name = "colControllerSettings";
            this.colControllerSettings.Text = "Settings";
            this.colControllerSettings.UseColumnTextForButtonValue = true;
            this.colControllerSettings.Width = 125;
            // 
            // lblFilter
            // 
            this.lblFilter.AutoSize = true;
            this.lblFilter.Location = new System.Drawing.Point(9, 311);
            this.lblFilter.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(29, 13);
            this.lblFilter.TabIndex = 5;
            this.lblFilter.Text = "Filter";
            // 
            // dgvInstrument
            // 
            this.dgvInstrument.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInstrument.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colInstrumentName,
            this.colInstrumentType,
            this.colInstrumentSettings});
            this.dgvInstrument.Location = new System.Drawing.Point(9, 187);
            this.dgvInstrument.Margin = new System.Windows.Forms.Padding(2);
            this.dgvInstrument.Name = "dgvInstrument";
            this.dgvInstrument.RowHeadersWidth = 51;
            this.dgvInstrument.RowTemplate.Height = 24;
            this.dgvInstrument.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvInstrument.Size = new System.Drawing.Size(486, 122);
            this.dgvInstrument.TabIndex = 6;
            this.dgvInstrument.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInstrument_CellContentClick);
            this.dgvInstrument.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInstrument_CellValueChanged);
            // 
            // colInstrumentName
            // 
            this.colInstrumentName.HeaderText = "Name";
            this.colInstrumentName.MinimumWidth = 125;
            this.colInstrumentName.Name = "colInstrumentName";
            this.colInstrumentName.Width = 125;
            // 
            // colInstrumentType
            // 
            this.colInstrumentType.HeaderText = "Type";
            this.colInstrumentType.MinimumWidth = 125;
            this.colInstrumentType.Name = "colInstrumentType";
            this.colInstrumentType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colInstrumentType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colInstrumentType.Width = 125;
            // 
            // colInstrumentSettings
            // 
            this.colInstrumentSettings.HeaderText = "Settings";
            this.colInstrumentSettings.MinimumWidth = 75;
            this.colInstrumentSettings.Name = "colInstrumentSettings";
            this.colInstrumentSettings.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colInstrumentSettings.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colInstrumentSettings.Text = "Settings";
            this.colInstrumentSettings.UseColumnTextForButtonValue = true;
            this.colInstrumentSettings.Width = 75;
            // 
            // dgvFilter
            // 
            this.dgvFilter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFilter.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colFilterName,
            this.colFilterType,
            this.colFilterSettings});
            this.dgvFilter.Location = new System.Drawing.Point(9, 327);
            this.dgvFilter.Margin = new System.Windows.Forms.Padding(2);
            this.dgvFilter.Name = "dgvFilter";
            this.dgvFilter.RowHeadersWidth = 51;
            this.dgvFilter.RowTemplate.Height = 24;
            this.dgvFilter.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvFilter.Size = new System.Drawing.Size(486, 122);
            this.dgvFilter.TabIndex = 7;
            this.dgvFilter.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFilter_CellContentClick);
            this.dgvFilter.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFilter_CellValueChanged);
            // 
            // colFilterName
            // 
            this.colFilterName.HeaderText = "Name";
            this.colFilterName.MinimumWidth = 125;
            this.colFilterName.Name = "colFilterName";
            this.colFilterName.Width = 125;
            // 
            // colFilterType
            // 
            this.colFilterType.HeaderText = "Type";
            this.colFilterType.MinimumWidth = 125;
            this.colFilterType.Name = "colFilterType";
            this.colFilterType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colFilterType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colFilterType.Width = 125;
            // 
            // colFilterSettings
            // 
            this.colFilterSettings.HeaderText = "Settings";
            this.colFilterSettings.MinimumWidth = 75;
            this.colFilterSettings.Name = "colFilterSettings";
            this.colFilterSettings.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colFilterSettings.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colFilterSettings.Text = "Settings";
            this.colFilterSettings.UseColumnTextForButtonValue = true;
            this.colFilterSettings.Width = 75;
            // 
            // dgvTarget
            // 
            this.dgvTarget.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTarget.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colTargetName,
            this.colTargetType,
            this.colTargetSettings});
            this.dgvTarget.Location = new System.Drawing.Point(9, 468);
            this.dgvTarget.Margin = new System.Windows.Forms.Padding(2);
            this.dgvTarget.Name = "dgvTarget";
            this.dgvTarget.RowHeadersWidth = 51;
            this.dgvTarget.RowTemplate.Height = 24;
            this.dgvTarget.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvTarget.Size = new System.Drawing.Size(486, 122);
            this.dgvTarget.TabIndex = 8;
            this.dgvTarget.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTarget_CellContentClick);
            this.dgvTarget.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTarget_CellValueChanged);
            // 
            // colTargetName
            // 
            this.colTargetName.HeaderText = "Name";
            this.colTargetName.MinimumWidth = 125;
            this.colTargetName.Name = "colTargetName";
            this.colTargetName.Width = 125;
            // 
            // colTargetType
            // 
            this.colTargetType.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.colTargetType.HeaderText = "Type";
            this.colTargetType.MinimumWidth = 125;
            this.colTargetType.Name = "colTargetType";
            this.colTargetType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colTargetType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colTargetType.Width = 125;
            // 
            // colTargetSettings
            // 
            this.colTargetSettings.HeaderText = "Settings";
            this.colTargetSettings.MinimumWidth = 75;
            this.colTargetSettings.Name = "colTargetSettings";
            this.colTargetSettings.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colTargetSettings.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colTargetSettings.Text = "Settings";
            this.colTargetSettings.UseColumnTextForButtonValue = true;
            this.colTargetSettings.Width = 75;
            // 
            // frmVirtualInstrument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 614);
            this.Controls.Add(this.dgvTarget);
            this.Controls.Add(this.dgvFilter);
            this.Controls.Add(this.dgvInstrument);
            this.Controls.Add(this.lblFilter);
            this.Controls.Add(this.dgvController);
            this.Controls.Add(this.lblTargets);
            this.Controls.Add(this.lblInstrument);
            this.Controls.Add(this.lblController);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmVirtualInstrument";
            this.Text = "Virtual Instrument";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstrument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTarget)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem avrGPIBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serialToolStripMenuItem;
        private System.Windows.Forms.Label lblController;
        private System.Windows.Forms.Label lblInstrument;
        private System.Windows.Forms.Label lblTargets;
        private System.Windows.Forms.DataGridView dgvController;
        private System.Windows.Forms.Label lblFilter;
        private System.Windows.Forms.DataGridView dgvInstrument;
        private System.Windows.Forms.DataGridView dgvFilter;
        private System.Windows.Forms.DataGridView dgvTarget;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colControllerType;
        private System.Windows.Forms.DataGridViewButtonColumn colAction;
        private System.Windows.Forms.DataGridViewButtonColumn colControllerSettings;
        private System.Windows.Forms.DataGridViewTextBoxColumn colInstrumentName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colInstrumentType;
        private System.Windows.Forms.DataGridViewButtonColumn colInstrumentSettings;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFilterName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colFilterType;
        private System.Windows.Forms.DataGridViewButtonColumn colFilterSettings;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTargetName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colTargetType;
        private System.Windows.Forms.DataGridViewButtonColumn colTargetSettings;
    }
}

