﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Ivi.Visa.Interop;
using SUF.Instrument;

namespace SUF.Instrument.VISA
{
    public partial class FormVISAConnection : ConnectionFormBase
    {
        // public new VISAConnectionSettings Settings;
        public FormVISAConnection(VISAConnectionSettings settings)
        {
            InitializeComponent();
            this.Settings = settings != null ? settings : new VISAConnectionSettings();

            IResourceManager rm = new ResourceManager();
            string[] devices = rm.FindRsrc("?+:INSTR");
            int ItemID;
            foreach (string device in devices)
            {
                ItemID = this.cbxConnStr.Items.Add(device);
                if((string)this.cbxConnStr.Items[ItemID] == ((VISAConnectionSettings)this.Settings).ConnectionString)
                {
                    this.cbxConnStr.SelectedIndex = ItemID;
                }
            }
            tbxName.Text = ((VISAConnectionSettings)this.Settings).Name;
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if(this.cbxConnStr.SelectedItem != null)
            {
                ((VISAConnectionSettings)this.Settings).ConnectionString = (string)this.cbxConnStr.SelectedItem;
                ((VISAConnectionSettings)this.Settings).Name = this.tbxName.Text;
                IsSaved = true;
            }
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
