﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SUF.Instrument;

namespace SUF.Instrument.VISA
{
    [PluginDescriptor("VISA")]
    [PluginSettingsClass("SUF.Instrument.VISA.FormVISAConnection")]
    public class VISAConnectionSettings : ConnectionSettings
    {
        public VISAConnectionSettings()
        {

        }
        public string ConnectionString;
        private VISAConnection _connection;
        private int _connectionID;

        public string Name;

        public override Connection GetConnection()
        {
            /*
            if(this._connection == null)
            {
                foreach(Connection conn in ConnectionSettingsCollection.GetConnectionSettingCollection)
                {
                    if(conn.GetType() == typeof(VISAConnection))
                    {
                        if(((VISAConnection)conn).ConnectionString == this.ConnectionString)
                        {
                            this._connectionID = ConnectionSettingsCollection.GetConnectionSettingCollection.GetIdByConnection(conn);
                            this._connection = (VISAConnection)conn;
                            return conn;
                        }
                    }
                }
                this._connection = new VISAConnection(this.ConnectionString);
                this._connectionID = ConnectionSettingsCollection.GetConnectionSettingCollection.Add(this._connection);
            }
            */
            return this._connection;
        }
        public override bool Equals(object obj)
        {
            if(obj.GetType() == typeof(VISAConnectionSettings))
            {
                return ((VISAConnectionSettings)obj).ConnectionString == this.ConnectionString;
            }
            return base.Equals(obj);
        }
        public override string ToString()
        {
            if(this.Name != null && this.Name != "")
            {
                return this.Name;
            }
            return "VISA:" + this.ConnectionString;
        }
    }
}
