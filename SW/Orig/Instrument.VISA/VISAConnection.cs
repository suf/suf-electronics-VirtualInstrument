﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SUF.Instrument;
using Ivi.Visa.Interop;

namespace SUF.Instrument.VISA
{
    public class VISAConnection : Connection
    {
        IResourceManager _rm;
        FormattedIO488 _gpib;
        IVisaSession _session;
        public string ConnectionString;
        public VISAConnection(string VisaConStr)
        {
            this.ConnectionString = VisaConStr;
            this._rm = new ResourceManager();
            this._gpib = new FormattedIO488();
            this._session = this._rm.Open(VisaConStr, mode: AccessMode.NO_LOCK, openTimeout: 2000);
            this._gpib.IO = (IMessage)this._session;
        }

        public override bool IsOpen
        {
            get
            {
                return true;
            }
        }

        public override InstrumentRetCode Close()
        {
            this._session.Close();
            return InstrumentRetCode.OK;
        }

        public override InstrumentRetCode Open()
        {
            return InstrumentRetCode.OK;
        }

        public override InstrumentRetCode Read(string Command, out string value)
        {
            this._gpib.WriteString(Command, true);
            value = this._gpib.ReadString();
            return InstrumentRetCode.OK;
        }

        public override InstrumentRetCode Send(string Command)
        {
            this._gpib.WriteString(Command, true);
            return InstrumentRetCode.OK;
        }
    }
}
