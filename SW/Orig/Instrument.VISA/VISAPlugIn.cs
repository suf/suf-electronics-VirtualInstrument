﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SUF.Instrument;

namespace SUF.Instrument.VISA
{
    public class VISAPlugIn : PlugIn
    {
        public VISAPlugIn() { }
        public override Type typeSettings
        {
            get
            {
                return typeof(VISAConnectionSettings);
            }
        }

        public override ConnectionFormBase SettingsDialog(ConnectionSettings settings)
        {
            return new FormVISAConnection((VISAConnectionSettings)settings);
        }
        public override string ToString()
        {
            return "VISA - Virtual Instrument Software Architecture";
        }
    }
}
