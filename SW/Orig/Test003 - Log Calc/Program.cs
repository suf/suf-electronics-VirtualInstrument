﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test003_LogCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            double Fstart = 20;
            double Fstop = 20000;
            double PointsPerDecade = 10;
            double SweepPoint = Fstart;
            double i = 0;
            do
            {
                Console.WriteLine(SweepPoint);
                i++;
                SweepPoint = Fstart * Math.Pow(10,i/PointsPerDecade);
            }
            while (SweepPoint <= Fstop);
        }
    }
}
