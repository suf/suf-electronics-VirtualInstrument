﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

using System.IO.Ports;
using SUF.Instrument;
using SUF.Instrument.AvrGPIB;

using HP8903;

namespace Test001
{
    class Program
    {
        static void Main(string[] args)
        {
            SerialPort com = new SerialPort("COM3");
            com.BaudRate = 115200;
            com.NewLine = "\r\n";
            com.DtrEnable = true;
            com.RtsEnable = true;
            // com.Handshake = Handshake.XOnXOff;
            AvrGPIB gpib = new AvrGPIB(com);
            Connection AACon = new AvrGPIBConnection(gpib, 28, 0);

            Source AASource = new Source(AACon);
            Measure AAMeasure = new Measure(AACon);
            AASource.variableParam = GeneratorParam.Frequency;
            AASource.variableStart = 20;
            AASource.variableStartUnit = Units.Hz;
            AASource.variableStop = 20;
            AASource.variableStopUnit = Units.kHz;
            AASource.variableScaleType = ScaleType.Log;
            AASource.fixedValue = 1;
            AASource.fixedValueUnit = Units.V;
            AASource.fixedParam = GeneratorParam.Amplitude;
            AASource.pointsPerDecade = 100;
            AAMeasure.measure = Measurements.AC_Level;
            AAMeasure.scale = ScaleType.Lin;
            AASource.Execute(AAMeasure);
            for(int i = 0; i < AAMeasure.Results.Count; i++)
            {
                Console.WriteLine("F: " + AAMeasure.Results[i].variable + "Hz V:" + AAMeasure.Results[i].value.ToString("0.000000", CultureInfo.InvariantCulture));
            }
        }
    }
}
