﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

using SUF.Instrument;

namespace HP8903
{
    public class Source
    {
        private Connection _instrument;
        public Source(Connection instrument)
        {
            this._instrument = instrument;
        }
        /// <summary>
        /// Validate the parameters
        /// </summary>
        /// <returns></returns>
        private bool Validate()
        {
            this._measurementPoints = new List<double>();
            double absStart;
            double absStop;
            double SweepPoint;
            int i = 0;
            switch (this.variableParam)
            {
                case GeneratorParam.Frequency:
                    if (!(this.variableStartUnit == Units.Hz || this.variableStartUnit == Units.kHz)) return false;
                    absStart = GetFrequencyInHz(this.variableStart, this.variableStartUnit);
                    absStop = GetFrequencyInHz(this.variableStop, this.variableStopUnit);
                    if (absStart < 20 || absStart > 100000 || absStop < 20 || absStop > 100000) return false;
                    this.variableValuetUnit = Units.Hz; // We are converting the values to Hz
                    switch(this.variableScaleType)
                    {
                        case ScaleType.Log:
                            if (this.pointsPerDecade == 0) return false;
                            if(absStart < absStop)
                            {
                                // Counting up
                                SweepPoint = absStart;
                                do
                                {
                                    this._measurementPoints.Add(SweepPoint);
                                    i++;
                                    SweepPoint = absStart * Math.Pow(10, (double)i / this.pointsPerDecade);
                                }
                                while (SweepPoint <= absStop);
                            }
                            else
                            {
                                // Counting down
                            }
                            break;
                    }
                    break;
                case GeneratorParam.Amplitude:
                    break;
                default:
                    return false;
            }
            return true;
        }
        private double GetFrequencyInHz(double freq, Units unit)
        {
            switch(unit)
            {
                case Units.Hz:
                    return freq;
                case Units.kHz:
                    return freq * 1000;
            }
            return 0;
        }
        public void Init()
        {
            this._instrument.Send(this.fixedParam.GetCommand() + this.fixedValue.ToString("0.000", CultureInfo.InvariantCulture) + this.fixedValueUnit.GetCommand());
        }
        public void Execute(Measure measure)
        {
            this.Init();
            measure.Init();
            if(this.Validate())
            {
                for(int i = 0; i<this._measurementPoints.Count; i++)
                {
                    this._instrument.Send(variableParam.GetCommand() + this._measurementPoints[i].ToString("0.00", CultureInfo.InvariantCulture) + this.variableValuetUnit.GetCommand());
                    this.variableValue = this._measurementPoints[i];
                    measure.Read(this);
                }

            }
        }

        public GeneratorParam variableParam;
        public double variableStart;
        public Units variableStartUnit;
        public double variableStop;
        public Units variableStopUnit;
        public double variableStep; // in Hz, Volt, or Decibel
        public Units variableStepUnit;
        public int pointsPerDecade = 0; // if logartimic frequency used
        public ScaleType variableScaleType;

        public double variableValue;
        public Units variableValuetUnit;

        public double fixedValue;
        public Units fixedValueUnit;
        public GeneratorParam fixedParam;

        private List<double> _measurementPoints;
    }
}
