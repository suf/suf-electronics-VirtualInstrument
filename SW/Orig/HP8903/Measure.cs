﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SUF.Instrument;

namespace HP8903
{
    public class Measure
    {
        Connection _instrument;
        public Measure(Connection instrument)
        {
            this._instrument = instrument;
            this.Results = new List<Result>();
        }
        /// <summary>
        /// Render the initialization string for the measurement setup
        /// </summary>
        /// <returns></returns>
        public string RenderInit()
        {
            string command = "";
            command += this.scale.GetCommand();    // Lin/Log
            command += this.measure.GetCommand();  // What to measure
            command += Trigger.Hold.GetCommand();   // Init finished, stop the analyzator
            return command;
        }
        public void Init()
        {
            string init = this.RenderInit();
            this._instrument.Send(init);
        }

        public void Read(Source src)
        {
            string readVal;
            Result result = new Result();
            result.variable = src.variableValue;
            result.variableUnit = src.variableValuetUnit;
            result.variableParam = src.variableParam;
            this._instrument.Read(ReadDisplay.Right.GetCommand() + Trigger.DelayedTrigger.GetCommand(), out readVal);
            result.valueStr = readVal;
            result.measurement = this.measure;
            this.Results.Add(result);
        }
        // Instrument Parameters
        public Measurements measure;
        public ScaleType scale;
        // Collected data
        public List<Result> Results;
    }
}
