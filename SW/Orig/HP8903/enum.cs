﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.ComponentModel;

namespace HP8903
{
    public static class ExtensionMethods
    {
        public static string GetCommand<T>(this T e) where T : IConvertible
        {
            if (e is Enum)
            {
                Type type = e.GetType();
                Array values = System.Enum.GetValues(type);

                foreach (int val in values)
                {
                    if (val == e.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var memInfo = type.GetMember(type.GetEnumName(val));
                        var descriptionAttribute = memInfo[0]
                            .GetCustomAttributes(typeof(DescriptionAttribute), false)
                            .FirstOrDefault() as DescriptionAttribute;

                        if (descriptionAttribute != null)
                        {
                            return descriptionAttribute.Description;
                        }
                    }
                }
            }
            return null; // could also return string.Empty
        }
    }

    public enum GeneratorParam
    {
        [Description("FR")]
        Frequency,
        [Description("AP")]
        Amplitude
    }

    public enum Units
    {
        [Description("HZ")]
        Hz,
        [Description("KZ")]
        kHz,
        [Description("VL")]
        V,
        [Description("MV")]
        mV,
        [Description("DB")]
        dB,
        [Description("DV")]
        dBm
    }

    public enum Measurements
    {
        [Description("S1")]
        DC_Level,
        [Description("M1")]
        AC_Level,
        [Description("A0")]
        RMS,
        [Description("A1")]
        AVG,
        [Description("M3")]
        Distortion,
        [Description("S3")]
        Distortion_Level,
        [Description("M2")]
        SINAD,
        [Description("S2")]
        SNR
    }

    public enum HP_Filters
    {
        [Description("H1")]
        Left,
        [Description("M2")]
        Right,
        [Description("H0")]
        Off
    }

    public enum LP_Filters
    {
        [Description("L1")]
        filter30kHz,
        [Description("L2")]
        filter80kHz,
        [Description("L0")]
        Off
    }

    public enum Ratio
    {
        [Description("R1")]
        On,
        [Description("R0")]
        Off
    }

    public enum ScaleType
    {
        [Description("LG")]
        Log,
        [Description("LN")]
        Lin
    }

    public enum Trigger
    {
        [Description("T0")]
        FreeRun,
        [Description("T1")]
        Hold,
        [Description("T2")]
        Trigger,
        [Description("T3")]
        DelayedTrigger
    }

    public enum ReadDisplay
    {
        [Description("RL")]
        Left,
        [Description("RR")]
        Right
    }
}
