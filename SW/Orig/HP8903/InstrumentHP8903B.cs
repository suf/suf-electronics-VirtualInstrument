﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SUF.Instrument;

namespace SUF.Instrument.HP8903
{
    [PluginDescriptor("HP 8903B Audio Analyzer")]
    [PluginSettingsClass("SUF.Instrument.HP8903.FormHP8903InstrumentSettings")]
    public class InstrumentHP8903B : Instrument
    {
        public InstrumentHP8903B() { }
    }
}
