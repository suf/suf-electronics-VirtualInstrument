﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;

namespace HP8903
{
    public class Result
    {
        public Result() { }
        public string valueStr
        {
            set
            {
                this.value = Double.Parse(value, CultureInfo.InvariantCulture);
            }
        }
        public double value;
        public Units valueUnit;
        public Measurements measurement;
        public double variable;
        public Units variableUnit;
        public GeneratorParam variableParam;
        public GeneratorParam variableType;
    }
}
