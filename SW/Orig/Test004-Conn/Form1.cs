﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;
using SUF.Instrument.InstrumentType;
using SUF.Instrument.AvrGPIB;
using SUF.Instrument.VISA;

namespace Test004_Conn
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void SettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConnectionSettings settings = new AvrGPIBConnectionSettings();
            FormConnection connDialog = new FormConnection(settings);
            connDialog.ShowDialog();
            if (connDialog.IsSaved)
            {
                settings = connDialog.Settings;
            }
            connDialog.Dispose();
        }
    }
}
