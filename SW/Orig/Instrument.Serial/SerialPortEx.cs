﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO.Ports;

namespace SUF.Instrument.Serial
{
    public class SerialPortEx : SerialPort
    {
        public SerialPortEx() : base() { }
        public SerialPortEx(string portName) : base(portName) { }
        public SerialPortEx(System.ComponentModel.IContainer container) : base(container) { }
        public SerialPortEx(string portName, int baudRate) : base(portName, baudRate) { }
        public SerialPortEx(string portName, int baudRate, Parity parity) : base(portName, baudRate, parity) { }
        public SerialPortEx(string portName, int baudRate, Parity parity, int dataBits) : base(portName, baudRate, parity, dataBits) { }
        public SerialPortEx(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits) : base(portName, baudRate, parity, dataBits, stopBits) { }
        public SerialPortEx Value
        {
            get
            {
                return this;
            }
        }
    }
}
