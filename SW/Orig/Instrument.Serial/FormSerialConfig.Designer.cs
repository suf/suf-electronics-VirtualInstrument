﻿namespace SUF.Instrument.Serial
{
    partial class FormSerialConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbxPort = new System.Windows.Forms.ComboBox();
            this.lblPort = new System.Windows.Forms.Label();
            this.lblBaud = new System.Windows.Forms.Label();
            this.lblHandshake = new System.Windows.Forms.Label();
            this.lblParity = new System.Windows.Forms.Label();
            this.lblStopBits = new System.Windows.Forms.Label();
            this.lblBits = new System.Windows.Forms.Label();
            this.chkRtsDtr = new System.Windows.Forms.CheckBox();
            this.cbxBaud = new System.Windows.Forms.ComboBox();
            this.cbxHandshake = new System.Windows.Forms.ComboBox();
            this.cbxParity = new System.Windows.Forms.ComboBox();
            this.cbxStopBits = new System.Windows.Forms.ComboBox();
            this.cbxBits = new System.Windows.Forms.ComboBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbxPort
            // 
            this.cbxPort.FormattingEnabled = true;
            this.cbxPort.Location = new System.Drawing.Point(88, 10);
            this.cbxPort.Name = "cbxPort";
            this.cbxPort.Size = new System.Drawing.Size(282, 21);
            this.cbxPort.TabIndex = 0;
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(13, 13);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(29, 13);
            this.lblPort.TabIndex = 1;
            this.lblPort.Text = "Port:";
            // 
            // lblBaud
            // 
            this.lblBaud.AutoSize = true;
            this.lblBaud.Location = new System.Drawing.Point(13, 40);
            this.lblBaud.Name = "lblBaud";
            this.lblBaud.Size = new System.Drawing.Size(35, 13);
            this.lblBaud.TabIndex = 2;
            this.lblBaud.Text = "Baud:";
            // 
            // lblHandshake
            // 
            this.lblHandshake.AutoSize = true;
            this.lblHandshake.Location = new System.Drawing.Point(13, 67);
            this.lblHandshake.Name = "lblHandshake";
            this.lblHandshake.Size = new System.Drawing.Size(65, 13);
            this.lblHandshake.TabIndex = 3;
            this.lblHandshake.Text = "Handshake:";
            // 
            // lblParity
            // 
            this.lblParity.AutoSize = true;
            this.lblParity.Location = new System.Drawing.Point(13, 94);
            this.lblParity.Name = "lblParity";
            this.lblParity.Size = new System.Drawing.Size(36, 13);
            this.lblParity.TabIndex = 4;
            this.lblParity.Text = "Parity:";
            // 
            // lblStopBits
            // 
            this.lblStopBits.AutoSize = true;
            this.lblStopBits.Location = new System.Drawing.Point(215, 67);
            this.lblStopBits.Name = "lblStopBits";
            this.lblStopBits.Size = new System.Drawing.Size(51, 13);
            this.lblStopBits.TabIndex = 5;
            this.lblStopBits.Text = "Stop bits:";
            // 
            // lblBits
            // 
            this.lblBits.AutoSize = true;
            this.lblBits.Location = new System.Drawing.Point(215, 40);
            this.lblBits.Name = "lblBits";
            this.lblBits.Size = new System.Drawing.Size(27, 13);
            this.lblBits.TabIndex = 6;
            this.lblBits.Text = "Bits:";
            // 
            // chkRtsDtr
            // 
            this.chkRtsDtr.AutoSize = true;
            this.chkRtsDtr.Location = new System.Drawing.Point(272, 93);
            this.chkRtsDtr.Name = "chkRtsDtr";
            this.chkRtsDtr.Size = new System.Drawing.Size(76, 17);
            this.chkRtsDtr.TabIndex = 7;
            this.chkRtsDtr.Text = "RTS/DTR";
            this.chkRtsDtr.UseVisualStyleBackColor = true;
            // 
            // cbxBaud
            // 
            this.cbxBaud.FormattingEnabled = true;
            this.cbxBaud.Location = new System.Drawing.Point(88, 37);
            this.cbxBaud.Name = "cbxBaud";
            this.cbxBaud.Size = new System.Drawing.Size(121, 21);
            this.cbxBaud.TabIndex = 8;
            // 
            // cbxHandshake
            // 
            this.cbxHandshake.FormattingEnabled = true;
            this.cbxHandshake.Location = new System.Drawing.Point(88, 64);
            this.cbxHandshake.Name = "cbxHandshake";
            this.cbxHandshake.Size = new System.Drawing.Size(121, 21);
            this.cbxHandshake.TabIndex = 9;
            // 
            // cbxParity
            // 
            this.cbxParity.FormattingEnabled = true;
            this.cbxParity.Location = new System.Drawing.Point(88, 91);
            this.cbxParity.Name = "cbxParity";
            this.cbxParity.Size = new System.Drawing.Size(121, 21);
            this.cbxParity.TabIndex = 10;
            // 
            // cbxStopBits
            // 
            this.cbxStopBits.FormattingEnabled = true;
            this.cbxStopBits.Location = new System.Drawing.Point(272, 64);
            this.cbxStopBits.Name = "cbxStopBits";
            this.cbxStopBits.Size = new System.Drawing.Size(98, 21);
            this.cbxStopBits.TabIndex = 11;
            // 
            // cbxBits
            // 
            this.cbxBits.FormattingEnabled = true;
            this.cbxBits.Items.AddRange(new object[] {
            "8",
            "7"});
            this.cbxBits.Location = new System.Drawing.Point(272, 37);
            this.cbxBits.Name = "cbxBits";
            this.cbxBits.Size = new System.Drawing.Size(98, 21);
            this.cbxBits.TabIndex = 12;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(12, 130);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 32);
            this.btnOk.TabIndex = 13;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(93, 130);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 32);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // FormSerialConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 167);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.cbxBits);
            this.Controls.Add(this.cbxStopBits);
            this.Controls.Add(this.cbxParity);
            this.Controls.Add(this.cbxHandshake);
            this.Controls.Add(this.cbxBaud);
            this.Controls.Add(this.chkRtsDtr);
            this.Controls.Add(this.lblBits);
            this.Controls.Add(this.lblStopBits);
            this.Controls.Add(this.lblParity);
            this.Controls.Add(this.lblHandshake);
            this.Controls.Add(this.lblBaud);
            this.Controls.Add(this.lblPort);
            this.Controls.Add(this.cbxPort);
            this.Name = "FormSerialConfig";
            this.Text = "Serial Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxPort;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.Label lblBaud;
        private System.Windows.Forms.Label lblHandshake;
        private System.Windows.Forms.Label lblParity;
        private System.Windows.Forms.Label lblStopBits;
        private System.Windows.Forms.Label lblBits;
        private System.Windows.Forms.CheckBox chkRtsDtr;
        private System.Windows.Forms.ComboBox cbxBaud;
        private System.Windows.Forms.ComboBox cbxHandshake;
        private System.Windows.Forms.ComboBox cbxParity;
        private System.Windows.Forms.ComboBox cbxStopBits;
        private System.Windows.Forms.ComboBox cbxBits;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
    }
}