﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO.Ports;

namespace SUF.Instrument.Serial
{
    public partial class FormSerialConfig : Form
    {
        public bool IsSaved = false;
        public SerialPort Com;
        public FormSerialConfig(SerialPort com)
        {
            InitializeComponent();
            int ItemID;
            if (com != null)
            {
                this.Com = com;
            }
            else
            {
                this.Com = new SerialPort();
            }
            // populate the dialog

            // Port
            cbxPort.Items.Clear();
            foreach (SerialInfo comPort in SerialInfo.GetCOMPortsInfo())
            {
                // if it is our own port or not on the list
                if (!SerialCollection.GetSerialCollection.Contains(comPort.Name) || this.Com.PortName == comPort.Name)
                {
                    ItemID = cbxPort.Items.Add(comPort);
                    if (this.Com.PortName == comPort.Name)
                    {
                        cbxPort.SelectedIndex = ItemID;
                    }
                }
            }
            cbxPort.Enabled = !Com.IsOpen;

            // Baud
            int[] BaudRates = { 115200, 57600, 38400, 19200, 9600, 4800, 2400, 1200, 600, 300 };
            foreach (int item in BaudRates)
            {
                ItemID = cbxBaud.Items.Add(item);
                if (item == this.Com.BaudRate)
                {
                    cbxBaud.SelectedIndex = ItemID;
                }
            }
            cbxBaud.Enabled = !Com.IsOpen;
            // Data Bits
            int[] Bits = { 8, 7 };
            foreach (int item in Bits)
            {
                ItemID = cbxBits.Items.Add(item);
                if (item == this.Com.DataBits)
                {
                    cbxBits.SelectedIndex = ItemID;
                }
            }
            cbxBits.Enabled = !Com.IsOpen;
            // Handshake
            foreach (Handshake item in Enum.GetValues(typeof(Handshake)))
            {
                ItemID = cbxHandshake.Items.Add(item);
                if (item == this.Com.Handshake)
                {
                    cbxHandshake.SelectedIndex = ItemID;
                }
            }
            cbxHandshake.Enabled = !Com.IsOpen;
            // Parity
            foreach (Parity item in Enum.GetValues(typeof(Parity)))
            {
                ItemID = cbxParity.Items.Add(item);
                if (item == this.Com.Parity)
                {
                    cbxParity.SelectedIndex = ItemID;
                }
            }
            cbxParity.Enabled = !Com.IsOpen;
            // Stop bits
            foreach (StopBits item in Enum.GetValues(typeof(StopBits)))
            {
                ItemID = cbxStopBits.Items.Add(item);
                if (item == this.Com.StopBits)
                {
                    cbxStopBits.SelectedIndex = ItemID;
                }
            }
            cbxStopBits.Enabled = !Com.IsOpen;
            // RTS/DTR
            chkRtsDtr.Checked = this.Com.RtsEnable;
            chkRtsDtr.Enabled = !Com.IsOpen;
        }
        private void BtnOk_Click(object sender, EventArgs e)
        {
            if(!this.Com.IsOpen)
            {
                if (cbxPort.SelectedItem != null)
                {
                    if(this.Com.PortName != ((SerialInfo)cbxPort.SelectedItem).Name)
                    {
                        // changing the port name, must be removed from the global list to release
                        SerialCollection.GetSerialCollection.Remove(this.Com.PortName);
                        this.Com.PortName = ((SerialInfo)cbxPort.SelectedItem).Name;
                    }
                    if (!SerialCollection.GetSerialCollection.Contains(this.Com.PortName))
                    {
                        SerialCollection.GetSerialCollection.Add(this.Com);
                    }
                    this.Com.BaudRate = ((int)cbxBaud.SelectedItem);
                    this.Com.DataBits = ((int)cbxBits.SelectedItem);
                    this.Com.Handshake = ((Handshake)cbxHandshake.SelectedItem);
                    this.Com.Parity = ((Parity)cbxParity.SelectedItem);
                    this.Com.StopBits = ((StopBits)cbxStopBits.SelectedItem);
                    this.Com.RtsEnable = chkRtsDtr.Checked;
                    this.Com.DtrEnable = chkRtsDtr.Checked;
                    IsSaved = true;
                }
            }
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
