﻿namespace SUF.Instrument.HP3478A
{
    partial class FormHP3478AInstrumentSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFunction = new System.Windows.Forms.Label();
            this.cbxFunction = new System.Windows.Forms.ComboBox();
            this.lblRange = new System.Windows.Forms.Label();
            this.cbxRange = new System.Windows.Forms.ComboBox();
            this.lblPrecision = new System.Windows.Forms.Label();
            this.cbxPrecision = new System.Windows.Forms.ComboBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnConnection = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblFunction
            // 
            this.lblFunction.AutoSize = true;
            this.lblFunction.Location = new System.Drawing.Point(12, 9);
            this.lblFunction.Name = "lblFunction";
            this.lblFunction.Size = new System.Drawing.Size(66, 17);
            this.lblFunction.TabIndex = 0;
            this.lblFunction.Text = "Function:";
            // 
            // cbxFunction
            // 
            this.cbxFunction.FormattingEnabled = true;
            this.cbxFunction.Location = new System.Drawing.Point(84, 6);
            this.cbxFunction.Name = "cbxFunction";
            this.cbxFunction.Size = new System.Drawing.Size(121, 24);
            this.cbxFunction.TabIndex = 1;
            this.cbxFunction.SelectedIndexChanged += new System.EventHandler(this.cbxFunction_SelectedIndexChanged);
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Location = new System.Drawing.Point(12, 39);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(54, 17);
            this.lblRange.TabIndex = 2;
            this.lblRange.Text = "Range:";
            // 
            // cbxRange
            // 
            this.cbxRange.FormattingEnabled = true;
            this.cbxRange.Location = new System.Drawing.Point(84, 36);
            this.cbxRange.Name = "cbxRange";
            this.cbxRange.Size = new System.Drawing.Size(121, 24);
            this.cbxRange.TabIndex = 3;
            // 
            // lblPrecision
            // 
            this.lblPrecision.AutoSize = true;
            this.lblPrecision.Location = new System.Drawing.Point(12, 69);
            this.lblPrecision.Name = "lblPrecision";
            this.lblPrecision.Size = new System.Drawing.Size(70, 17);
            this.lblPrecision.TabIndex = 4;
            this.lblPrecision.Text = "Precision:";
            // 
            // cbxPrecision
            // 
            this.cbxPrecision.FormattingEnabled = true;
            this.cbxPrecision.Location = new System.Drawing.Point(84, 66);
            this.cbxPrecision.Name = "cbxPrecision";
            this.cbxPrecision.Size = new System.Drawing.Size(121, 24);
            this.cbxPrecision.TabIndex = 5;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(15, 109);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 35);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(97, 109);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 34);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnConnection
            // 
            this.btnConnection.Location = new System.Drawing.Point(208, 109);
            this.btnConnection.Name = "btnConnection";
            this.btnConnection.Size = new System.Drawing.Size(92, 34);
            this.btnConnection.TabIndex = 8;
            this.btnConnection.Text = "Connection";
            this.btnConnection.UseVisualStyleBackColor = true;
            // 
            // FormHP3478AInstrumentSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 162);
            this.Controls.Add(this.btnConnection);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.cbxPrecision);
            this.Controls.Add(this.lblPrecision);
            this.Controls.Add(this.cbxRange);
            this.Controls.Add(this.lblRange);
            this.Controls.Add(this.cbxFunction);
            this.Controls.Add(this.lblFunction);
            this.Name = "FormHP3478AInstrumentSettings";
            this.Text = "HP3478A Settings";
            this.Load += new System.EventHandler(this.FormHP3478AInstrumentSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFunction;
        private System.Windows.Forms.ComboBox cbxFunction;
        private System.Windows.Forms.Label lblRange;
        private System.Windows.Forms.ComboBox cbxRange;
        private System.Windows.Forms.Label lblPrecision;
        private System.Windows.Forms.ComboBox cbxPrecision;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnConnection;
    }
}