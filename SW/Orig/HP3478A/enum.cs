﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.ComponentModel;

namespace SUF.Instrument.HP3478A
{
    public static class ExtensionMethods
    {
        public static string GetCommand<T>(this T e) where T : IConvertible
        {
            if (e is Enum)
            {
                Type type = e.GetType();
                Array values = System.Enum.GetValues(type);

                foreach (int val in values)
                {
                    if (val == e.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var memInfo = type.GetMember(type.GetEnumName(val));
                        var descriptionAttribute = memInfo[0]
                            .GetCustomAttributes(typeof(DescriptionAttribute), false)
                            .FirstOrDefault() as DescriptionAttribute;

                        if (descriptionAttribute != null)
                        {
                            return descriptionAttribute.Description;
                        }
                    }
                }
            }
            return null; // could also return string.Empty
        }
    }

    public enum MeasurementFunction
    {
        [Description("F1")]
        DC_Volts,
        [Description("F2")]
        AC_Volts,
        [Description("F3")]
        Ohms_2wire,
        [Description("F4")]
        Ohms_4wire,
        [Description("F5")]
        DC_Current,
        [Description("F6")]
        AC_Current,
        [Description("F7")]
        Ohms_Extended
    }
    public enum Range
    {
        [Description("R-2")]
        DCV_30mV,
        [Description("R-1")]
        DCV_300mV,
        [Description("R-1")]
        ACV_300mV,
        [Description("R-1")]
        DCI_300mA,
        [Description("R-1")]
        ACI_300mA,
        [Description("R0")]
        DCV_3V,
        [Description("R0")]
        ACV_3V,
        [Description("R0")]
        DCI_3A,
        [Description("R0")]
        ACI_3A,
        [Description("R1")]
        DCV_30V,
        [Description("R1")]
        ACV_30V,
        [Description("R1")]
        R_30ohm,
        [Description("R2")]
        DCV_300V,
        [Description("R2")]
        ACV_300V,
        [Description("R2")]
        R_300ohm,
        [Description("R3")]
        R_3kohm,
        [Description("R4")]
        R_30kohm,
        [Description("R5")]
        R_300kohm,
        [Description("R6")]
        R_3Mohm,
        [Description("R7")]
        R_30Mohm,
        [Description("RA")]
        DCV_Auto,
        [Description("RA")]
        ACV_Auto,
        [Description("RA")]
        DCI_Auto,
        [Description("RA")]
        ACI_Auto,
        [Description("RA")]
        R_Auto
    }

    public enum Precision
    {
        [Description("N3")]
        Digit_35,
        [Description("N4")]
        Digit_45,
        [Description("N5")]
        Digit_55,
    }

    public enum Trigger
    {
        [Description("T1")]
        Internal,
        [Description("T2")]
        External,
        [Description("T3")]
        Single,
        [Description("T4")]
        Hold,
        [Description("T5")]
        Fast
    }
}
