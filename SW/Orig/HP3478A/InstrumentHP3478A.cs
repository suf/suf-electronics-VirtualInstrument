﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;

using SUF.Instrument;

namespace SUF.Instrument.HP3478A
{
    [PluginDescriptor("HP 3478A Multimeter")]
    [PluginSettingsClass("SUF.Instrument.HP3478A.FormHP3478AInstrumentSettings")]
    public class InstrumentHP3478A : Instrument
    {
        public ResultDescriptor resultDescriptor;
        public InstrumentHP3478A()
        {
            base.sourceDescriptors = new List<ResultDescriptor>();
            resultDescriptor = new ResultDescriptor();
            resultDescriptor.SourceGuid = base.InstanceID;
            resultDescriptor.ChannelID = 0;
            base.sourceDescriptors.Add(resultDescriptor);
            this.func = MeasurementFunction.DC_Volts;
        }
        private MeasurementFunction _func;
        public MeasurementFunction func
        {
            get
            {
                return this._func;
            }
            set
            {
                this._func = value;
                this.resultDescriptor.ChannelName = value.ToString();
                this.resultDescriptor.unit = Function2Unit(value);
            }
        }
        private MeasureUnit Function2Unit(MeasurementFunction func)
        {
            switch(func)
            {
                case MeasurementFunction.AC_Current:
                    return MeasureUnit.ampere;
                case MeasurementFunction.AC_Volts:
                    return MeasureUnit.volt;
                case MeasurementFunction.DC_Current:
                    return MeasureUnit.ampere;
                case MeasurementFunction.DC_Volts:
                    return MeasureUnit.volt;
                case MeasurementFunction.Ohms_2wire:
                    return MeasureUnit.ohm;
                case MeasurementFunction.Ohms_4wire:
                    return MeasureUnit.ohm;
                case MeasurementFunction.Ohms_Extended:
                    return MeasureUnit.ohm;
            }
            return MeasureUnit.unknown;
        }
        public Range range = Range.DCV_Auto;
        public Precision precision = Precision.Digit_55;
        public Trigger trigger; // Required?
        public bool IsInitialized = false;
        public int ResultID;
        public override void Measure(MeasurementObject results)
        {
            string result;
            MeasurementValue value;
            InstrumentRetCode ret = conn.Read(Trigger.Single.GetCommand(), out result);
            if(ret == InstrumentRetCode.OK)
            {
                value = new MeasurementValue(Convert.ToDouble(result, CultureInfo.InvariantCulture),MetricPrefix.none);
                results.Values.Add(ResultID, value);
            }
        }
        public override void Init()
        {
            if(!IsInitialized)
            {
                if(!conn.IsOpen)
                {
                    conn.Open();
                    conn.Send(func.GetCommand() + range.GetCommand() + precision.GetCommand() + Trigger.Hold.GetCommand());
                }
                IsInitialized = true;
            }
        }
    }
}
