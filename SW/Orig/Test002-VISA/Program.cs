﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ivi.Visa.Interop;

namespace Test002_VISA
{
    class Program
    {
        static void Main(string[] args)
        {
            IResourceManager rm = new ResourceManager();
            FormattedIO488 iee488 = new FormattedIO488();
            IVisaSession session;
            string idn;
            string[] devices = rm.FindRsrc("?+:INSTR");
            foreach(string device in devices)
            {
                session = rm.Open(device, mode: AccessMode.NO_LOCK, openTimeout: 2000);
                iee488.IO = (IMessage)session;
                iee488.WriteString("*IDN?", true);
                idn = iee488.ReadString();
                Console.WriteLine(device + " - " + idn);
                session.Close();
            }
        }
    }
}
