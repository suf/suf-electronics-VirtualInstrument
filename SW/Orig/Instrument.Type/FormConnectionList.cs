﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;

namespace SUF.Instrument.InstrumentType
{
    public partial class FormConnectionList : Form
    {
        public FormConnectionList()
        {
            InitializeComponent();
            UpdateForm();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FormConnection connDialog = new FormConnection(null);
            connDialog.ShowDialog();
            if(connDialog.IsSaved)
            {
                // instantiate the connection, if doesn't exist yet - maybe better to keep the settings in the singleton instead of the connection object itself?
                // conn.Settings.GetConnection();
                ConnectionSettingsCollection.GetConnectionSettingCollection.Add(connDialog.Settings);
                UpdateForm();
            }
            connDialog.Dispose();
        }
        private void UpdateForm()
        {
            this.lbxConnection.Items.Clear();
            foreach (ConnectionSettings conn in ConnectionSettingsCollection.GetConnectionSettingCollection)
            {
                this.lbxConnection.Items.Add(conn);
            }
        }

        private void lbxConnection_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ConnectionFormBase connectionSettingsForm;
            int index = this.lbxConnection.IndexFromPoint(e.Location);
            if(index != ListBox.NoMatches)
            {
                connectionSettingsForm = ConnectionTypes.Item(this.lbxConnection.Items[index].GetType()).SettingsDialog((ConnectionSettings)this.lbxConnection.Items[index]);
                connectionSettingsForm.ShowDialog();
                connectionSettingsForm.Dispose();
                UpdateForm();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            ConnectionFormBase connectionSettingsForm;
            connectionSettingsForm = ConnectionTypes.Item(this.lbxConnection.SelectedItem.GetType()).SettingsDialog((ConnectionSettings)this.lbxConnection.SelectedItem);
            connectionSettingsForm.ShowDialog();
            connectionSettingsForm.Dispose();
            UpdateForm();
        }
    }
}
