﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SUF.Instrument;
using SUF.Instrument.VISA;
using SUF.Instrument.AvrGPIB;

namespace SUF.Instrument.InstrumentType
{
    public static class ConnectionTypes
    {
        // private static List<PlugIn> _connTypeList;
        private static Dictionary<Type, PlugIn> _connTypeList;
        public static int Count
        {
            get
            {
                return _connTypeList.Count;
            }
        }
        public static PlugIn Item(Type pluginType)
        {
            return _connTypeList[pluginType];
        }
        public static PlugIn Item(int index)
        {
            return _connTypeList.Values.ElementAt(index);
        }
        static ConnectionTypes()
        {
            _connTypeList = new Dictionary<Type, PlugIn>();
            _connTypeList.Add(typeof(VISAConnectionSettings),new VISAPlugIn());
            _connTypeList.Add(typeof(AvrGPIBConnectionSettings), new AvrGPIBPlugIn());
        }
    }
    /*
    public class ConnectionType
    {
        public string Name;
        public Type TypeConn;
        public ConnectionType(Type type, string name)
        {
            this.Name = name;
            this.TypeConn = type;
        }
        public override string ToString()
        {
            return this.Name;
        }
    }
    */
}
