﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;

namespace SUF.Instrument.InstrumentType
{
    public partial class FormConnection : ConnectionFormBase
    {
        public new ConnectionSettings Settings;
        public FormConnection(ConnectionSettings settings)
        {
            InitializeComponent();
            this.Settings = settings;
            int ItemID;
            for(int i = 0; i < ConnectionTypes.Count; i++)
            {
                ItemID = cbxConnType.Items.Add(ConnectionTypes.Item(i));
                if (settings != null)
                {
                    if (((PlugIn)cbxConnType.Items[ItemID]).typeSettings == settings.GetType())
                    {
                        cbxConnType.SelectedIndex = ItemID;
                    }
                }
            }
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            IsSaved = true;
            this.Close();
        }

        private void BtnSettings_Click(object sender, EventArgs e)
        {
            ConnectionFormBase connectionSettingsForm;
            if(this.cbxConnType.SelectedItem != null)
            {
                connectionSettingsForm = ((PlugIn)this.cbxConnType.SelectedItem).SettingsDialog(this.Settings);
                connectionSettingsForm.ShowDialog();
                if (connectionSettingsForm.IsSaved)
                {
                    this.Settings = connectionSettingsForm.Settings;
                }
                connectionSettingsForm.Dispose();
            }
        }
    }
}
