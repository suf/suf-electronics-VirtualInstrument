﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SUF.Instrument.Standard
{
    public partial class FormTargetDigitalSettings : TargetSettingsFormBase, IResultConsumerSettings
    {
        public FormTargetDigitalSettings()
        {
            InitializeComponent();
        }

        public void SetAvailableResults(List<ResultDescriptor> resultDescriptors)
        {
            int ItemID;
            this.cbxSource.Items.Clear();
            foreach(ResultDescriptor rd in resultDescriptors)
            {
                ItemID = this.cbxSource.Items.Add(rd);
                if(base.target != null)
                {
                    if(((FormTargetDigital)base.target).resultDescriptor != null)
                    {
                        if (((FormTargetDigital)base.target).resultDescriptor.SourceGuid == rd.SourceGuid && ((FormTargetDigital)base.target).resultDescriptor.ChannelID == rd.ChannelID)
                        {
                            this.cbxSource.SelectedIndex = ItemID;
                        }
                    }
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            ((FormTargetDigital)base.target).resultDescriptor = this.cbxSource.SelectedItem as ResultDescriptor;
            base.IsSaved = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormTargetDigitalSettings_Load(object sender, EventArgs e)
        {
            if(base.target != null)
            {
                base.target = new FormTargetDigital();
            }
        }
    }
}
