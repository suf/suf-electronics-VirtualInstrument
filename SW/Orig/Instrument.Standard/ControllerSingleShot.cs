﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SUF.Instrument;

namespace SUF.Instrument.Standard
{
    [PluginDescriptor("Single Shot Controller")]
    public class ControllerSingleShot : Controller
    {
        public ControllerSingleShot()
        {

        }
        public override void Start()
        {
            MeasurementObject results = new MeasurementObject();
            results.TimeStamp = DateTime.Now;
            results.SequenceNumber = 1;
            if (!this.Async)
            {
                foreach (Instrument instrument in this.instruments)
                {
                    if (instrument != null)
                    {
                        instrument.Init();
                        instrument.Measure(results);
                    }
                }
                foreach (ITarget target in this.targets)
                {
                    if(target != null)
                    {
                        target.Init(this.resultDescriptors);
                        if (target is IResultConsumer)
                        {
                            ((IResultConsumer)target).ProcessResult(results);
                        }
                    }
                }
            }
        }
    }
}
