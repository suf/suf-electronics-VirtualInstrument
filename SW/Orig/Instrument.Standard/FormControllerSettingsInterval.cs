﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;

namespace SUF.Instrument.Standard
{
    public partial class FormControllerSettingsInterval : ControllerSettingsFormBase
    {
        public FormControllerSettingsInterval()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            // update controller values with the visual element values
            ((ControllerInterval)this.controller).Interval = new TimeSpan(Convert.ToInt32(this.numHour.Value), Convert.ToInt32(this.numMin.Value), Convert.ToInt32(this.numSec.Value));
            this.IsSaved = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormControllerSettingsInterval_Load(object sender, EventArgs e)
        {
            if (this.controller == null)
            {
                this.controller = new ControllerInterval();
            }
            if (this.controller.GetType() != typeof(ControllerInterval))
            {
                this.controller = new ControllerInterval();
            }
            // populate visual elements
            this.numHour.Value = ((ControllerInterval)this.controller).Interval.Hours;
            this.numMin.Value = ((ControllerInterval)this.controller).Interval.Minutes;
            this.numSec.Value = ((ControllerInterval)this.controller).Interval.Seconds;
        }
    }
}
