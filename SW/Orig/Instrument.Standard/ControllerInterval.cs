﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SUF.Instrument;

namespace SUF.Instrument.Standard
{
    [PluginDescriptor("Interval Controller")]
    [PluginSettingsClass("SUF.Instrument.Standard.FormControllerSettingsInterval")]
    public class ControllerInterval : Controller
    {
        public ControllerInterval() { }
        public TimeSpan Interval = new TimeSpan();
    }
}
