﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SUF.Instrument.Standard
{
    [PluginDescriptor("Digital Display")]
    [PluginSettingsClass("SUF.Instrument.Standard.FormTargetDigitalSettings")]
    public partial class FormTargetDigital : Form, ITarget, IResultConsumer
    {
        public ResultDescriptor resultDescriptor;

        public int resultId;
        public MeasureUnit unit;
        private string _name;
        public FormTargetDigital()
        {
            InitializeComponent();
        }

        public string GetName()
        {
            return this._name;
        }
        
        public void Init(List<ResultDescriptor> resultDescriptors)
        {
            this.unit = resultDescriptors[resultId].unit;
        }
        
        public void ProcessResult(MeasurementObject results)
        {
            // handle the measurement prefix - maybe remove the prefix from the MeasurementValue object and calculate for display here?
            this.lblDisplay.Text = results.Values[resultId].value.ToString() + " " + this.unit.GetDescription();
        }

        public void SetName(string name)
        {
            this._name = name;
        }

        private void lblDisplay_Click(object sender, EventArgs e)
        {

        }
    }
}
