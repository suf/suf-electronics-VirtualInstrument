﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;

namespace SUF.Instrument.AvrGPIB
{
    public partial class FormAvrGPIBInterfaceList : Form
    {
        public bool IsSaved = false;
        public AvrGPIBInterface GPIBInterface;
        public FormAvrGPIBInterfaceList()
        {
            InitializeComponent();

            this.lbxInterfaces.DisplayMember = "Name";
            this.lbxInterfaces.ValueMember = "Value";
            this.lbxInterfaces.DataSource = AvrGPIBInterfaceCollection.GetAvrGPIBInterfaceCollection.GPIBInterfaces;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FormAvrGPIBInterfaceSettings phiDialog = new FormAvrGPIBInterfaceSettings();
            phiDialog.ShowDialog();
            if(phiDialog.IsSaved)
            {
                if (phiDialog.GPIBInterface.Port != null)
                {
                    if(phiDialog.GPIBInterface.Name == "" || phiDialog.GPIBInterface.Name == null)
                    {
                        phiDialog.GPIBInterface.Name = "AvrGPIB:" + phiDialog.GPIBInterface.Port.PortName;
                    }
                    AvrGPIBInterfaceCollection.GetAvrGPIBInterfaceCollection.GPIBInterfaces.Add(phiDialog.GPIBInterface);
                    AvrGPIBInterfaceCollection.GetAvrGPIBInterfaceCollection.GPIBInterfaces.ResetBindings();
                }
            }
            phiDialog.Dispose();
        }

        private void lbxInterfaces_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.lbxInterfaces.IndexFromPoint(e.Location) >= 0)
            {
                EditInterface((AvrGPIBInterface)this.lbxInterfaces.Items[this.lbxInterfaces.IndexFromPoint(e.Location)]);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditInterface((AvrGPIBInterface)this.lbxInterfaces.SelectedItem);
        }

        private void EditInterface(AvrGPIBInterface phi)
        {
            FormAvrGPIBInterfaceSettings settingsForm = new FormAvrGPIBInterfaceSettings();
            settingsForm.GPIBInterface = phi;
            settingsForm.ShowDialog();
            if(settingsForm.IsSaved)
            {
                AvrGPIBInterfaceCollection.GetAvrGPIBInterfaceCollection.GPIBInterfaces.ResetBindings();
            }
            settingsForm.Dispose();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.GPIBInterface = (AvrGPIBInterface)this.lbxInterfaces.SelectedItem;
            this.IsSaved = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            // check if no connection uses it before remove
        }
        public static void SetupConnection(ref AvrGPIBInterface phi)
        {
            FormAvrGPIBInterfaceList interfaceListForm = new FormAvrGPIBInterfaceList();
            if (phi != null)
            {
                interfaceListForm.GPIBInterface = phi;
            }
            interfaceListForm.ShowDialog();
            if (interfaceListForm.IsSaved)
            {
                phi = interfaceListForm.GPIBInterface;
            }
            interfaceListForm.Dispose();
        }

        private void FormConnectionList_Load(object sender, EventArgs e)
        {
            if(this.GPIBInterface != null)
            {
                foreach(AvrGPIBInterface phi in this.lbxInterfaces.Items)
                {
                    if(phi == this.GPIBInterface)
                    {
                        this.lbxInterfaces.SelectedItem = this.GPIBInterface;
                        break;
                    }
                }
            }
        }
    }
}
