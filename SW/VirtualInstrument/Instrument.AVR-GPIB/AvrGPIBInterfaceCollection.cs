﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument.AvrGPIB
{
    public sealed class AvrGPIBInterfaceCollection
    {
        private AvrGPIBInterfaceCollection() { }
        private static readonly Lazy<AvrGPIBInterfaceCollection> lazy = new Lazy<AvrGPIBInterfaceCollection>(() => new AvrGPIBInterfaceCollection());
        public static AvrGPIBInterfaceCollection GetAvrGPIBInterfaceCollection
        {
            get
            {
                return lazy.Value;
            }
        }
        public BindingList<AvrGPIBInterface> GPIBInterfaces = new BindingList<AvrGPIBInterface>();
        /*
        private int _currentId = 0;
        private Dictionary<int,AvrGPIBInterface> _interfaces;
        public int Add(AvrGPIBInterface GPIBInterface)
        {
            int id = this._currentId++;
            this._interfaces.Add(id, GPIBInterface);
            return id;
        }
        public void Remove(int id)
        {
            if(this._interfaces.ContainsKey(id))
            {
                // if the interface has connections it can't be deleted. The check must be implemented later.
                this._interfaces.Remove(id);
            }
        }
        public int GetIdByConnection(AvrGPIBInterface IfSettings)
        {

            foreach (int key in this._interfaces.Keys)
            {
                if (this._interfaces[key] == IfSettings)
                {
                    return key;
                }
            }
            return -1;
        }


        public IEnumerator<AvrGPIBInterface> GetEnumerator()
        {
            return this._interfaces.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._interfaces.Values.GetEnumerator();
        }
        */
    }
}
