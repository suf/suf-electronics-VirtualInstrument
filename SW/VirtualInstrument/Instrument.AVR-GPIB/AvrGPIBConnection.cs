﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SUF.Instrument;


namespace SUF.Instrument.AvrGPIB
{
    [PluginDescriptor("AvrGPIB - Arduino IEEE488 / HP - IB / GP - IB")]
    [PluginSettingsClass("SUF.Instrument.AvrGPIB.FormAvrGPIBConnectionSettings")]
    public class AvrGPIBConnection : Connection
    {
        public AvrGPIBInterface GPIBInterface;
        private int _PAD;
        private int _SAD;
        private string _Name = "";

        public int PAD
        {
            get
            {
                return this._PAD;
            }
            set
            {
                this._PAD = value;
            }
        }
        public int SAD
        {
            get
            {
                return this._SAD;
            }
            set
            {
                this._SAD = value;
            }
        }

        public AvrGPIBConnection() {}

        public override bool IsOpen
        {
            get
            {
                if(this.GPIBInterface != null)
                {
                    if(this.GPIBInterface.Port != null)
                    {
                        return this.GPIBInterface.Port.IsOpen;
                    }
                }
                return false;
            }
        }

        public override PhysicalConnectionType ConnectionType
        {
            get
            {
                return PhysicalConnectionType.GPIB;
            }
        }

        public override InstrumentRetObject Close()
        {
            throw new NotImplementedException();
        }

        public override InstrumentRetObject Open()
        {
            if (this.GPIBInterface != null)
            {
                if(!this.GPIBInterface.IsOpen)
                {
                    return this.GPIBInterface.Open();
                }
                /*
                if (this.GPIBInterface.Port != null)
                {
                    this.GPIBInterface.Port.Open();
                    return InstrumentRetCode.OK;
                }
                */
            }
            return new InstrumentRetObject(InstrumentRetCode.MissingPHY);
        }

        public override InstrumentRetObject Read(string Command, out string value)
        {
            return this.GPIBInterface.Read(this._PAD, this._SAD, Command, out value, this.ReadTerminator.GetDescription());
        }
        public override InstrumentRetObject Send(string Command)
        {
            return this.GPIBInterface.Send(this._PAD, this._SAD, Command);
        }
        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(AvrGPIBConnection))
            {
                return ((AvrGPIBConnection)obj).PAD == this.PAD && ((AvrGPIBConnection)obj).SAD == this.SAD && ((AvrGPIBConnection)obj).GPIBInterface == this.GPIBInterface;
            }
            return base.Equals(obj);
        }
     
        public override string Name
        {
            get
            {
                return this._Name;
            }
            set
            {
                this._Name = value;
            }
        }
        public override string ToString()
        {
            return this._Name;
        }


    }
}
