﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.IO;
using System.IO.Ports;
using System.CodeDom;
using SUF.Instrument.Serial;

namespace SUF.Instrument.AvrGPIB
{
    public class AvrGPIBInterface
    {
        public AvrGPIBInterface() { }
        public SerialPortEx Port;
        private int _OwnPAD = -1;
        private int _currentPAD = -1;
        private int _currentSAD = -1;
        private int _readTimeoutMs = 200;
        private bool _Lock = false;
        private string _name = "";

        public bool IsOpen
        {
            get
            {
                if(this.Port != null)
                {
                    return this.Port.IsOpen;
                }
                return false;
            }
        }
        public int OwnAddress
        {
            get
            {
                return this._OwnPAD;
            }
            set
            {
                this._OwnPAD = value;
                if (this.Port != null)
                {
                    if (this.Port.IsOpen)
                    {
                        Port.WriteLine("++addr_own " + this._OwnPAD.ToString());
                    }
                }
            }
        }
        public InstrumentRetObject Open()
        {
            if(this.Port != null)
            {
                if (!this._Lock)
                {
                    this._Lock = true;
                    if (!this.Port.IsOpen)
                    {
                        this.Port.NewLine = "\r\n";
                        // connect
                        this.Port.Open();
                        // switch off autosave
                        Port.WriteLine("++savecfg 0");
                        Port.WriteLine("++echo 0");
                        Port.WriteLine("++dbg_level 3");
                        Port.WriteLine("++eot_enable 1");
                        Port.WriteLine("++eot_char 0");
                        Port.WriteLine("++read_tmo_ms 200");
                        // Clear buffer
                        Port.ReadExisting();
                        // switch off echo
                        // get/set local address
                        if (this._OwnPAD == -1)
                        {
                            Port.WriteLine("++addr_own");
                            this._OwnPAD = Int32.Parse(Port.ReadLine());
                        }
                    }
                    this._Lock = false;
                }
                else
                {
                    return new InstrumentRetObject(InstrumentRetCode.Busy);
                }
            }
            else
            {
                return new InstrumentRetObject(InstrumentRetCode.MissingPHY);
            }
            return new InstrumentRetObject(InstrumentRetCode.OK);
        }
        public InstrumentRetObject Send(int PAD, int SAD, string Command)
        {
            if (!this._Lock)
            {
                this._Lock = true;
                if (this._OwnPAD == PAD)
                {
                    this._Lock = false;
                    return new InstrumentRetObject(InstrumentRetCode.AddressConflict);
                }
                if (this._currentPAD != PAD || this._currentPAD != SAD)
                {
                    this.Port.WriteLine("++addr_rem " + PAD.ToString() + (SAD != 0 ? " " + SAD.ToString() : ""));
                    this._currentPAD = PAD;
                    this._currentSAD = SAD;
                }
                if (Command != "")
                {
                    this.Port.WriteLine(Command);
                }
                this._Lock = false;
            }
            else
            {
                return new InstrumentRetObject(InstrumentRetCode.Busy);
            }
            return new InstrumentRetObject(InstrumentRetCode.OK);
        }
        public InstrumentRetObject Read(int PAD, int SAD, string Command, out string value, string endStr)
        {
            string readValue;
            if (!this._Lock)
            {
                this._Lock = true;
                value = "";
                if (this._OwnPAD == PAD)
                {
                    this._Lock = false;
                    return new InstrumentRetObject(InstrumentRetCode.AddressConflict);
                }
                if (this._currentPAD != PAD || this._currentPAD != SAD)
                {
                    this.Port.WriteLine("++addr_rem " + PAD.ToString() + (SAD != 0 ? " " + SAD.ToString() : ""));
                    this._currentPAD = PAD;
                    this._currentSAD = SAD;
                }
                if (Command != "")
                {
                    this.Port.WriteLine(Command);
                }
                do
                {
                    readValue = "";
                    this.Port.WriteLine("++read eoi");
                    Thread.Sleep(this._readTimeoutMs);
                    if (this.Port.BytesToRead > 0)
                    {
                        readValue = this.Port.ReadTo(endStr).TrimEnd('\r', '\n', '\0');
                    }
                }
                while (readValue == "");
                this._Lock = false;
            }
            else
            {
                value = "";
                return new InstrumentRetObject(InstrumentRetCode.Busy);
            }
            value = readValue;
            return new InstrumentRetObject(InstrumentRetCode.OK);
        }
        public AvrGPIBInterface Value
        {
            get
            {
                return this;
            }
        }
        public string Name
        {
            get
            {
                if(this._name == null)
                {
                    return this._name = "";
                }
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }
        public override string ToString()
        {
            return this._name != "" ? this._name : "unknown";
        }

        public void Dispose()
        {
            if (this.Port != null)
            {
                if (this.Port.IsOpen)
                {
                    this.Port.Close();
                }
                // Remove from the serial port singleton !!!
            }
        }
    }
}
