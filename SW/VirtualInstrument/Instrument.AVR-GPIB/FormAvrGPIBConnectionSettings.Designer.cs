﻿namespace SUF.Instrument.AvrGPIB
{
    partial class FormAvrGPIBConnectionSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInterface = new System.Windows.Forms.Label();
            this.lblDevPAD = new System.Windows.Forms.Label();
            this.numDevPAD = new System.Windows.Forms.NumericUpDown();
            this.lblDevSAD = new System.Windows.Forms.Label();
            this.numDevSAD = new System.Windows.Forms.NumericUpDown();
            this.chkDevSADEnable = new System.Windows.Forms.CheckBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnGPIBInterface = new System.Windows.Forms.Button();
            this.cbxInterfaces = new System.Windows.Forms.ComboBox();
            this.lblName = new System.Windows.Forms.Label();
            this.tbxName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numDevPAD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDevSAD)).BeginInit();
            this.SuspendLayout();
            // 
            // lblInterface
            // 
            this.lblInterface.AutoEllipsis = true;
            this.lblInterface.AutoSize = true;
            this.lblInterface.Location = new System.Drawing.Point(12, 46);
            this.lblInterface.Name = "lblInterface";
            this.lblInterface.Size = new System.Drawing.Size(67, 17);
            this.lblInterface.TabIndex = 0;
            this.lblInterface.Text = "Interface:";
            // 
            // lblDevPAD
            // 
            this.lblDevPAD.AutoSize = true;
            this.lblDevPAD.Location = new System.Drawing.Point(12, 75);
            this.lblDevPAD.Name = "lblDevPAD";
            this.lblDevPAD.Size = new System.Drawing.Size(87, 17);
            this.lblDevPAD.TabIndex = 3;
            this.lblDevPAD.Text = "Device PAD:";
            // 
            // numDevPAD
            // 
            this.numDevPAD.Location = new System.Drawing.Point(141, 73);
            this.numDevPAD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numDevPAD.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numDevPAD.Name = "numDevPAD";
            this.numDevPAD.Size = new System.Drawing.Size(45, 22);
            this.numDevPAD.TabIndex = 4;
            // 
            // lblDevSAD
            // 
            this.lblDevSAD.AutoSize = true;
            this.lblDevSAD.Location = new System.Drawing.Point(12, 103);
            this.lblDevSAD.Name = "lblDevSAD";
            this.lblDevSAD.Size = new System.Drawing.Size(87, 17);
            this.lblDevSAD.TabIndex = 5;
            this.lblDevSAD.Text = "Device SAD:";
            // 
            // numDevSAD
            // 
            this.numDevSAD.Location = new System.Drawing.Point(141, 101);
            this.numDevSAD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numDevSAD.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numDevSAD.Name = "numDevSAD";
            this.numDevSAD.Size = new System.Drawing.Size(45, 22);
            this.numDevSAD.TabIndex = 6;
            // 
            // chkDevSADEnable
            // 
            this.chkDevSADEnable.AutoSize = true;
            this.chkDevSADEnable.Location = new System.Drawing.Point(193, 102);
            this.chkDevSADEnable.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkDevSADEnable.Name = "chkDevSADEnable";
            this.chkDevSADEnable.Size = new System.Drawing.Size(74, 21);
            this.chkDevSADEnable.TabIndex = 7;
            this.chkDevSADEnable.Text = "Enable";
            this.chkDevSADEnable.UseVisualStyleBackColor = true;
            this.chkDevSADEnable.CheckedChanged += new System.EventHandler(this.chkDevSADEnable_CheckedChanged);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(15, 139);
            this.btnOk.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 31);
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(96, 139);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 31);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnGPIBInterface
            // 
            this.btnGPIBInterface.Location = new System.Drawing.Point(245, 139);
            this.btnGPIBInterface.Margin = new System.Windows.Forms.Padding(4);
            this.btnGPIBInterface.Name = "btnGPIBInterface";
            this.btnGPIBInterface.Size = new System.Drawing.Size(100, 31);
            this.btnGPIBInterface.TabIndex = 10;
            this.btnGPIBInterface.Text = "Interface";
            this.btnGPIBInterface.UseVisualStyleBackColor = true;
            this.btnGPIBInterface.Click += new System.EventHandler(this.btnGPIBInterface_Click);
            // 
            // cbxInterfaces
            // 
            this.cbxInterfaces.FormattingEnabled = true;
            this.cbxInterfaces.Location = new System.Drawing.Point(88, 42);
            this.cbxInterfaces.Margin = new System.Windows.Forms.Padding(4);
            this.cbxInterfaces.Name = "cbxInterfaces";
            this.cbxInterfaces.Size = new System.Drawing.Size(256, 24);
            this.cbxInterfaces.TabIndex = 11;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(15, 13);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 17);
            this.lblName.TabIndex = 12;
            this.lblName.Text = "Name:";
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(88, 13);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(257, 22);
            this.tbxName.TabIndex = 13;
            // 
            // FormAvrGPIBConnectionSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 191);
            this.Controls.Add(this.tbxName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.cbxInterfaces);
            this.Controls.Add(this.btnGPIBInterface);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.chkDevSADEnable);
            this.Controls.Add(this.numDevSAD);
            this.Controls.Add(this.lblDevSAD);
            this.Controls.Add(this.numDevPAD);
            this.Controls.Add(this.lblDevPAD);
            this.Controls.Add(this.lblInterface);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormAvrGPIBConnectionSettings";
            this.Text = "GPIB Connection";
            this.Load += new System.EventHandler(this.FormAvrGPIBConnectionSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numDevPAD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDevSAD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInterface;
        private System.Windows.Forms.Label lblDevPAD;
        private System.Windows.Forms.NumericUpDown numDevPAD;
        private System.Windows.Forms.Label lblDevSAD;
        private System.Windows.Forms.NumericUpDown numDevSAD;
        private System.Windows.Forms.CheckBox chkDevSADEnable;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnGPIBInterface;
        private System.Windows.Forms.ComboBox cbxInterfaces;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox tbxName;
    }
}