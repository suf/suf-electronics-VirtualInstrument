﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO.Ports;

using SUF.Instrument;
using SUF.Instrument.Serial;

namespace SUF.Instrument.AvrGPIB
{
    public partial class FormAvrGPIBInterfaceSettings : Form
    {
        // private int _interfaceId = -1;
        public AvrGPIBInterface GPIBInterface;
        public bool IsSaved = false;
        public FormAvrGPIBInterfaceSettings()
        {
            InitializeComponent();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            this.GPIBInterface.OwnAddress = this.chkOwnAddrAuto.Checked ? -1 : Convert.ToInt32(this.numOwnAddr.Value);
            if (this.GPIBInterface.Port != null)
            {
                if (this.tbxName.Text == "")
                {
                    this.GPIBInterface.Name = "AvrGPIB:" + this.GPIBInterface.Port.PortName;
                }
                else
                {
                    this.GPIBInterface.Name = this.tbxName.Text;
                }
                this.IsSaved = true;
            }
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSerialSettings_Click(object sender, EventArgs e)
        {
            FormSerialPortList serialDialog = new FormSerialPortList();
            serialDialog.port = this.GPIBInterface.Port;
            serialDialog.ShowDialog();
            if(serialDialog.IsSaved)
            {
                this.GPIBInterface.Port = serialDialog.port;
            }
            serialDialog.Dispose();
        }

        private void chkOwnAddrAuto_CheckedChanged(object sender, EventArgs e)
        {
            this.numOwnAddr.Enabled = !this.chkOwnAddrAuto.Checked;
        }

        private void FormAvrGPIBConfig_FormClosing(object sender, FormClosingEventArgs e)
        {
            /*
            if (this._interfaceId > -1 && !this.IsSaved)
            {
                AvrGPIBInterfaceCollection.GetAvrGPIBInterfaceCollection.Remove(this._interfaceId);
            }
            */
        }

        private void FormAvrGPIBInterfaceSettings_Load(object sender, EventArgs e)
        {
            if(this.GPIBInterface == null)
            {
                this.GPIBInterface = new AvrGPIBInterface();
            }
            this.tbxName.Text = this.GPIBInterface.Name;
            this.chkOwnAddrAuto.Checked = this.GPIBInterface.OwnAddress < 0;
        }
    }
}
