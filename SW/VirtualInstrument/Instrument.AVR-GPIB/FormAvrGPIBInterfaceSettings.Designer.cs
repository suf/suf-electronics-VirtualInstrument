﻿namespace SUF.Instrument.AvrGPIB
{
    partial class FormAvrGPIBInterfaceSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOwnAddr = new System.Windows.Forms.Label();
            this.numOwnAddr = new System.Windows.Forms.NumericUpDown();
            this.chkOwnAddrAuto = new System.Windows.Forms.CheckBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSerialSettings = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.tbxName = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numOwnAddr)).BeginInit();
            this.SuspendLayout();
            // 
            // lblOwnAddr
            // 
            this.lblOwnAddr.AutoSize = true;
            this.lblOwnAddr.Location = new System.Drawing.Point(12, 59);
            this.lblOwnAddr.Name = "lblOwnAddr";
            this.lblOwnAddr.Size = new System.Drawing.Size(123, 17);
            this.lblOwnAddr.TabIndex = 0;
            this.lblOwnAddr.Text = "Interface Address:";
            // 
            // numOwnAddr
            // 
            this.numOwnAddr.Location = new System.Drawing.Point(141, 58);
            this.numOwnAddr.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numOwnAddr.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numOwnAddr.Name = "numOwnAddr";
            this.numOwnAddr.Size = new System.Drawing.Size(45, 22);
            this.numOwnAddr.TabIndex = 1;
            // 
            // chkOwnAddrAuto
            // 
            this.chkOwnAddrAuto.AutoSize = true;
            this.chkOwnAddrAuto.Location = new System.Drawing.Point(193, 59);
            this.chkOwnAddrAuto.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkOwnAddrAuto.Name = "chkOwnAddrAuto";
            this.chkOwnAddrAuto.Size = new System.Drawing.Size(244, 21);
            this.chkOwnAddrAuto.TabIndex = 2;
            this.chkOwnAddrAuto.Text = "Automatic (Get from the Interface)";
            this.chkOwnAddrAuto.UseVisualStyleBackColor = true;
            this.chkOwnAddrAuto.CheckedChanged += new System.EventHandler(this.chkOwnAddrAuto_CheckedChanged);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(15, 106);
            this.btnOk.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 31);
            this.btnOk.TabIndex = 8;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(96, 106);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 31);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnSerialSettings
            // 
            this.btnSerialSettings.Location = new System.Drawing.Point(288, 106);
            this.btnSerialSettings.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSerialSettings.Name = "btnSerialSettings";
            this.btnSerialSettings.Size = new System.Drawing.Size(129, 31);
            this.btnSerialSettings.TabIndex = 10;
            this.btnSerialSettings.Text = "&Serial settings";
            this.btnSerialSettings.UseVisualStyleBackColor = true;
            this.btnSerialSettings.Click += new System.EventHandler(this.BtnSerialSettings_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(11, 16);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 17);
            this.lblName.TabIndex = 11;
            this.lblName.Text = "Name:";
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(76, 12);
            this.tbxName.Margin = new System.Windows.Forms.Padding(4);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(352, 22);
            this.tbxName.TabIndex = 12;
            // 
            // FormAvrGPIBInterfaceSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(445, 151);
            this.Controls.Add(this.tbxName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.btnSerialSettings);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.chkOwnAddrAuto);
            this.Controls.Add(this.numOwnAddr);
            this.Controls.Add(this.lblOwnAddr);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormAvrGPIBInterfaceSettings";
            this.Text = "GPIB Interface";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAvrGPIBConfig_FormClosing);
            this.Load += new System.EventHandler(this.FormAvrGPIBInterfaceSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numOwnAddr)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOwnAddr;
        private System.Windows.Forms.NumericUpDown numOwnAddr;
        private System.Windows.Forms.CheckBox chkOwnAddrAuto;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSerialSettings;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox tbxName;
    }
}