﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SUF.Instrument;
using Ivi.Visa.Interop;

namespace SUF.Instrument.VISA
{
    [PluginDescriptor("VISA")]
    [PluginSettingsClass("SUF.Instrument.VISA.FormVISAConnectionSettings")]
    public class VISAConnection : Connection
    {
        IResourceManager _rm;
        FormattedIO488 _gpib;
        IVisaSession _session;
        private bool _isOpen;
        private string _name = "";
        public string ConnectionString = "";
        public override string Name 
        {
            get
            {
                if(this._name == "")
                {
                    if(this.ConnectionString != "")
                    {
                        this._name = "VISA:" + this.ConnectionString;
                    }
                }
                return this._name;
            }
            set => this._name = value;
        }
        public VISAConnection()
        {
            this._rm = new ResourceManager();
            this._gpib = new FormattedIO488();
        }

        public override bool IsOpen
        {
            get
            {
                return _isOpen;
            }
        }

        public override PhysicalConnectionType ConnectionType
        {
            get
            {
                string connType = this.ConnectionString.Substring(0, ConnectionString.IndexOf(":")).Trim().ToUpper();
                switch(connType)
                {
                    case "GPIB":
                        return PhysicalConnectionType.GPIB;
                    default:
                        return PhysicalConnectionType.Unknown;
                }
            }
        }

        public override InstrumentRetObject Close()
        {
            InstrumentRetObject retObj = new InstrumentRetObject(InstrumentRetCode.OK);
            if (_isOpen)
            {
                try
                {
                    this._session.Close();
                    _isOpen = false;
                }
                catch (Exception e)
                {
                    retObj.RetCode = InstrumentRetCode.PHYCloseError;
                    retObj.RetInfo = new object[] { e };
                }
            }
            else
            {
                retObj.RetCode = InstrumentRetCode.PHYAlreadyClosed;
            }
            return retObj;
        }

        public override InstrumentRetObject Open()
        {
            InstrumentRetObject retObj = new InstrumentRetObject(InstrumentRetCode.OK);
            if (!_isOpen)
            {
                try
                {
                    this._session = this._rm.Open(this.ConnectionString, mode: AccessMode.NO_LOCK, openTimeout: 2000);
                    this._gpib.IO = (IMessage)this._session;
                    // this._gpib.IO.Timeout = 5000;
                    _isOpen = true;
                }
                catch(Exception e)
                {
                    retObj.RetCode = InstrumentRetCode.PHYOpenError;
                    retObj.RetInfo = new object[] { e };
                }
            }
            else
            {
                retObj.RetCode = InstrumentRetCode.PHYAlreadyOpen;
            }
            return retObj;
        }

        public override InstrumentRetObject Read(string Command, out string value)
        {
            InstrumentRetObject retObj = new InstrumentRetObject(InstrumentRetCode.OK);
            value = "";
            if (_isOpen)
            {
                try
                {
                    this._gpib.WriteString(Command, true);
                    value = this._gpib.ReadString();
                }
                catch (Exception e)
                {
                    retObj.RetCode = InstrumentRetCode.PHYCommunicationError;
                    retObj.RetInfo = new object[] { e };
                }
            }
            else
            {
                retObj.RetCode = InstrumentRetCode.PHYNotOpen;
            }
            return retObj;
        }

        public override InstrumentRetObject Send(string Command)
        {
            InstrumentRetObject retObj = new InstrumentRetObject(InstrumentRetCode.OK);
            if (_isOpen)
            {
                try
                {
                    this._gpib.WriteString(Command, true);
                }
                catch (Exception e)
                {
                    retObj.RetCode = InstrumentRetCode.PHYCommunicationError;
                    retObj.RetInfo = new object[] { e };
                }
            }
            else
            {
                retObj.RetCode = InstrumentRetCode.PHYNotOpen;
            }
            return retObj;
        }
    }
}
