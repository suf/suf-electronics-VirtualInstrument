﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Ivi.Visa.Interop;
using SUF.Instrument;

namespace SUF.Instrument.VISA
{
    public partial class FormVISAConnectionSettings : ConnectionSettingsFormBase
    {
        // public new VISAConnectionSettings Settings;
        public FormVISAConnectionSettings()
        {
            InitializeComponent();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            if(this.cbxConnStr.SelectedItem != null)
            {
                ((VISAConnection)base.connection).ConnectionString = (string)this.cbxConnStr.SelectedItem;
                ((VISAConnection)base.connection).Name = this.tbxName.Text;
                base.IsSaved = true;
            }
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormVISAConnectionSettings_Load(object sender, EventArgs e)
        {
            IResourceManager rm = new ResourceManager();
            string[] devices = rm.FindRsrc("?+:INSTR");
            int ItemID;
            if(!(base.connection is VISAConnection))
            {
                base.connection = new VISAConnection();
            }
            foreach (string device in devices)
            {
                ItemID = this.cbxConnStr.Items.Add(device);
                if (base.connection is VISAConnection)
                {
                    if ((string)this.cbxConnStr.Items[ItemID] == ((VISAConnection)base.connection).ConnectionString)
                    {
                        this.cbxConnStr.SelectedIndex = ItemID;
                    }
                }
            }
            if (base.connection is VISAConnection)
            {
                tbxName.Text = ((VISAConnection)base.connection).Name;
            }
        }
    }
}
