﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SUF.Instrument.HP8903
{
    public partial class FormHP8903InstrumentSettings : InstrumentSettingsFormBase, IResultConsumerSettings
    {
        public FormHP8903InstrumentSettings()
        {
            InitializeComponent();
        }
        private List<TabElement> _tabElements;

        private bool IsLoaded = false;
        private void FormHP8903InstrumentSettings_Load(object sender, EventArgs e)
        {
            if(!(this.instrument is InstrumentHP8903))
            {
                this.instrument = new InstrumentHP8903();
            }
            _tabElements = new List<TabElement>();
            foreach (TabPage page in this.tbxControl.TabPages)
            {
                this._tabElements.Add(new TabElement(page, true));
            }
            // Load analyzer type combobox
            UpdateEnumComboBox<AudioAnalyzerType>(this.cbxType, ((InstrumentHP8903)this.instrument).analyzerType);

            this.chkACLevel.Checked = ((InstrumentHP8903)this.instrument).enabled_AC;
            this.chkDCLevel.Checked = ((InstrumentHP8903)this.instrument).enabled_DC;
            this.chkDistortion.Checked = ((InstrumentHP8903)this.instrument).enabled_Distortion;
            this.chkDistortionLevel.Checked = ((InstrumentHP8903)this.instrument).enabled_DistortionLevel;
            this.chkSNR.Checked = ((InstrumentHP8903)this.instrument).enabled_SNR;
            this.chkSINAD.Checked = ((InstrumentHP8903)this.instrument).enabled_SINAD;

            UpdateEnumComboBox<FrequencyUnits>(this.cbxFrequencyUnit, ((InstrumentHP8903)this.instrument).GeneratorFrequencyUnit);
            UpdateEnumComboBox<AmplitudeUnits>(this.cbxAmplitudeUnit, ((InstrumentHP8903)this.instrument).GeneratorAmplitudeUnit);
            UpdateEnumComboBox<SourceOutputImpedance>(this.cbxOutputImpedance, ((InstrumentHP8903)this.instrument).GeneratorImpedance);
            this.numFrequency.Value = Convert.ToDecimal(((InstrumentHP8903)this.instrument).GeneratorFrequency);
            this.numAmplitudeValue.Value = Convert.ToDecimal(((InstrumentHP8903)this.instrument).GeneratorAmplitude);

            UpdateEnumComboBox<InputRangeAC>(this.cbxAC_Range, ((InstrumentHP8903)this.instrument).AC_InputRange);
            UpdateEnumComboBox<ScaleType>(this.cbxAC_LinLog, ((InstrumentHP8903)this.instrument).AC_ScaleType);
            this.chkAC_Ratio.Checked = ((InstrumentHP8903)this.instrument).AC_Ratio;
            UpdateEnumComboBox<PostNotchGain>(this.cbxAC_PostNotchGain, ((InstrumentHP8903)this.instrument).AC_PostNotchGain);
            UpdateEnumComboBox<DetectorType>(this.cbxAC_DetectorType, ((InstrumentHP8903)this.instrument).AC_DetectorType);
            this.chkAC_HoldNotchTuning.Checked = ((InstrumentHP8903)this.instrument).AC_HoldNotchTuning;
            this.chkAC_PowerDisplay.Checked = ((InstrumentHP8903)this.instrument).AC_PowerDisplay;
            this.numAC_PowerLoad.Value = ((InstrumentHP8903)this.instrument).AC_PowerOhms;
            this.chkAC_HPLeft.Checked = ((InstrumentHP8903)this.instrument).AC_HPLeft;
            this.chkAC_HPRight.Checked = ((InstrumentHP8903)this.instrument).AC_HPRight;
            this.chkAC_LP30kHz.Checked = ((InstrumentHP8903)this.instrument).AC_LP30kHz;
            this.chkAC_LP80kHz.Checked = ((InstrumentHP8903)this.instrument).AC_LP80kHz;

            UpdateEnumComboBox<InputRangeDC>(this.cbxDC_Range, ((InstrumentHP8903)this.instrument).DC_InputRange);
            UpdateEnumComboBox<ScaleType>(this.cbxDC_LinLog, ((InstrumentHP8903)this.instrument).DC_ScaleType);
            this.chkDC_Ratio.Checked = ((InstrumentHP8903)this.instrument).DC_Ratio;
            
            UpdateEnumComboBox<InputRangeAC>(this.cbxDist_Range, ((InstrumentHP8903)this.instrument).Dist_InputRange);
            UpdateEnumComboBox<ScaleType>(this.cbxDist_LinLog, ((InstrumentHP8903)this.instrument).Dist_ScaleType);
            this.chkDist_Ratio.Checked = ((InstrumentHP8903)this.instrument).Dist_Ratio;
            UpdateEnumComboBox<PostNotchGain>(this.cbxDist_PostNotchGain, ((InstrumentHP8903)this.instrument).Dist_PostNotchGain);
            UpdateEnumComboBox<DetectorType>(this.cbxDist_DetectorType, ((InstrumentHP8903)this.instrument).Dist_DetectorType);
            this.chkDist_HoldNotchTuning.Checked = ((InstrumentHP8903)this.instrument).Dist_HoldNotchTuning;
            this.chkDist_HPLeft.Checked = ((InstrumentHP8903)this.instrument).Dist_HPLeft;
            this.chkDist_HPRight.Checked = ((InstrumentHP8903)this.instrument).Dist_HPRight;
            this.chkDist_LP30kHz.Checked = ((InstrumentHP8903)this.instrument).Dist_LP30kHz;
            this.chkDist_LP80kHz.Checked = ((InstrumentHP8903)this.instrument).Dist_LP80kHz;
            
            UpdateEnumComboBox<InputRangeAC>(this.cbxDistLevel_Range, ((InstrumentHP8903)this.instrument).DistLevel_InputRange);
            UpdateEnumComboBox<ScaleType>(this.cbxDistLevel_LinLog, ((InstrumentHP8903)this.instrument).DistLevel_ScaleType);
            this.chkDistLevel_Ratio.Checked = ((InstrumentHP8903)this.instrument).DistLevel_Ratio;
            UpdateEnumComboBox<PostNotchGain>(this.cbxDistLevel_PostNotchGain, ((InstrumentHP8903)this.instrument).DistLevel_PostNotchGain);
            UpdateEnumComboBox<DetectorType>(this.cbxDistLevel_DetectorType, ((InstrumentHP8903)this.instrument).DistLevel_DetectorType);
            this.chkDistLevel_HoldNotchTuning.Checked = ((InstrumentHP8903)this.instrument).DistLevel_HoldNotchTuning;
            this.chkDistLevel_HPLeft.Checked = ((InstrumentHP8903)this.instrument).DistLevel_HPLeft;
            this.chkDistLevel_HPRight.Checked = ((InstrumentHP8903)this.instrument).DistLevel_HPRight;
            this.chkDistLevel_LP30kHz.Checked = ((InstrumentHP8903)this.instrument).DistLevel_LP30kHz;
            this.chkDistLevel_LP80kHz.Checked = ((InstrumentHP8903)this.instrument).DistLevel_LP80kHz;

            UpdateEnumComboBox<InputRangeAC>(this.cbxSNR_Range, ((InstrumentHP8903)this.instrument).SNR_InputRange);
            UpdateEnumComboBox<ScaleType>(this.cbxSNR_LinLog, ((InstrumentHP8903)this.instrument).SNR_ScaleType);
            this.chkSNR_Ratio.Checked = ((InstrumentHP8903)this.instrument).SNR_Ratio;
            UpdateEnumComboBox<PostNotchGain>(this.cbxSNR_PostNotchGain, ((InstrumentHP8903)this.instrument).SNR_PostNotchGain);
            UpdateEnumComboBox<DetectorType>(this.cbxSNR_DetectorType, ((InstrumentHP8903)this.instrument).SNR_DetectorType);
            this.chkSNR_HoldNotchTuning.Checked = ((InstrumentHP8903)this.instrument).SNR_HoldNotchTuning;
            this.chkSNR_HPLeft.Checked = ((InstrumentHP8903)this.instrument).SNR_HPLeft;
            this.chkSNR_HPRight.Checked = ((InstrumentHP8903)this.instrument).SNR_HPRight;
            this.chkSNR_LP30kHz.Checked = ((InstrumentHP8903)this.instrument).SNR_LP30kHz;
            this.chkSNR_LP80kHz.Checked = ((InstrumentHP8903)this.instrument).SNR_LP80kHz;
            UpdateEnumComboBox<SNRDelay>(this.cbxSNR_Delay, ((InstrumentHP8903)this.instrument).SNR_Delay);

            UpdateEnumComboBox<InputRangeAC>(this.cbxSINAD_Range, ((InstrumentHP8903)this.instrument).SINAD_InputRange);
            UpdateEnumComboBox<ScaleType>(this.cbxSINAD_LinLog, ((InstrumentHP8903)this.instrument).SINAD_ScaleType);
            this.chkSINAD_Ratio.Checked = ((InstrumentHP8903)this.instrument).SINAD_Ratio;
            UpdateEnumComboBox<PostNotchGain>(this.cbxSINAD_PostNotchGain, ((InstrumentHP8903)this.instrument).SINAD_PostNotchGain);
            UpdateEnumComboBox<DetectorType>(this.cbxSINAD_DetectorType, ((InstrumentHP8903)this.instrument).SINAD_DetectorType);
            this.chkSINAD_HoldNotchTuning.Checked = ((InstrumentHP8903)this.instrument).SINAD_HoldNotchTuning;
            this.chkSINAD_HPLeft.Checked = ((InstrumentHP8903)this.instrument).SINAD_HPLeft;
            this.chkSINAD_HPRight.Checked = ((InstrumentHP8903)this.instrument).SINAD_HPRight;
            this.chkSINAD_LP30kHz.Checked = ((InstrumentHP8903)this.instrument).SINAD_LP30kHz;
            this.chkSINAD_LP80kHz.Checked = ((InstrumentHP8903)this.instrument).SINAD_LP80kHz;
            UpdateEnumComboBox<SinadRange>(this.cbxSINAD_SINADRange, ((InstrumentHP8903)this.instrument).SINAD_Range);

            this.IsLoaded = true;
            UpdateVisualElements();
        }
        private void UpdateVisualElements()
        {
            TabPage Selected;
            if (IsLoaded)
            {
                switch (((AudioAnalyzerType)this.cbxType.SelectedItem))
                {
                    case AudioAnalyzerType.HP8903A:
                        this._tabElements[1].Enabled = true;
                        break;
                    case AudioAnalyzerType.HP8903B:
                        this._tabElements[1].Enabled = true;
                        break;
                    case AudioAnalyzerType.HB8903E:
                        this._tabElements[1].Enabled = false;
                        break;
                }
                this._tabElements[2].Enabled = chkACLevel.Checked;
                this._tabElements[3].Enabled = chkDCLevel.Checked;
                this._tabElements[4].Enabled = chkDistortion.Checked;
                this._tabElements[5].Enabled = chkDistortionLevel.Checked;
                this._tabElements[6].Enabled = chkSNR.Checked;
                this._tabElements[7].Enabled = chkSINAD.Checked;
                this._tabElements[8].Enabled = chkFrequency.Checked;

                // Save Selection
                Selected = this.tbxControl.SelectedTab;

                this.tbxControl.TabPages.Clear();
                foreach (TabElement element in this._tabElements)
                {
                    if (element.Enabled)
                    {
                        this.tbxControl.TabPages.Add(element.Page);
                        // Restore selection
                        if(element.Page == Selected)
                        {
                            this.tbxControl.SelectedTab = Selected;
                        }
                    }
                }
                this.numFrequency.Enabled = this.cbxSourceFrequency.SelectedIndex < 1;
                this.cbxFrequencyUnit.Enabled = this.cbxSourceFrequency.SelectedIndex < 1;
                this.numAmplitudeValue.Enabled = this.cbxSourceAmplitude.SelectedIndex < 1;
                this.cbxAmplitudeUnit.Enabled = this.cbxSourceAmplitude.SelectedIndex < 1;

                this.numAC_PowerLoad.Enabled = this.chkAC_PowerDisplay.Checked;
            }
        }
        private class TabElement
        {
            public TabElement() { }
            public TabElement(TabPage page, bool enabled)
            {
                this.Page = page;
                this.Enabled = enabled;
            }
            public bool Enabled;
            public TabPage Page;
        }

        private void CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            UpdateVisualElements();
        }

        private void cbxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateVisualElements();
        }

        public void SetAvailableResults(List<ResultDescriptor> resultDescriptors)
        {
            int ItemID;
            this.cbxSourceFrequency.Items.Clear();
            this.cbxSourceFrequency.Items.Add("Fixed Value");
            this.cbxSourceFrequency.SelectedIndex = 0;
            this.cbxSourceAmplitude.Items.Clear();
            this.cbxSourceAmplitude.Items.Add("Fixed Value");
            this.cbxSourceAmplitude.SelectedIndex = 0;
            foreach (ResultDescriptor rd in resultDescriptors)
            {
                if(rd.unit == MeasureUnit.hertz)
                {
                    ItemID = this.cbxSourceFrequency.Items.Add(rd);
                    if(base.instrument != null)
                    {
                        if(((InstrumentHP8903)base.instrument).frequencySourceDescriptor != null)
                        {
                            if(((InstrumentHP8903)base.instrument).frequencySourceDescriptor.SourceGuid == rd.SourceGuid && ((InstrumentHP8903)base.instrument).frequencySourceDescriptor.ChannelID == rd.ChannelID)
                            {
                                this.cbxSourceFrequency.SelectedIndex = ItemID;
                            }
                        }
                    }
                }
                if(new[] { MeasureUnit.volt, MeasureUnit.dBm, MeasureUnit.dBu}.Contains(rd.unit))
                {
                    ItemID = this.cbxSourceAmplitude.Items.Add(rd);
                    if (base.instrument != null)
                    {
                        if (((InstrumentHP8903)base.instrument).amplitudeSourceDescriptor != null)
                        {
                            if (((InstrumentHP8903)base.instrument).amplitudeSourceDescriptor.SourceGuid == rd.SourceGuid && ((InstrumentHP8903)base.instrument).amplitudeSourceDescriptor.ChannelID == rd.ChannelID)
                            {
                                this.cbxSourceAmplitude.SelectedIndex = ItemID;
                            }
                        }
                    }
                }
            }
        }

        private void cbxSourceFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateVisualElements();
        }

        private void cbxSourceAmplitude_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateVisualElements();
        }

        private FrequencyUnits prevFrequencyUnit;
        private void cbxFrequencyUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            decimal prevValue = this.numFrequency.Value;
            switch ((FrequencyUnits)this.cbxFrequencyUnit.SelectedItem)
            {
                case FrequencyUnits.Hz:
                    this.numFrequency.Minimum = 10;
                    this.numFrequency.Increment = 1;
                    this.numFrequency.Maximum = 100000;
                    this.numFrequency.DecimalPlaces = 0;
                    if(prevFrequencyUnit == FrequencyUnits.kHz)
                    {
                        this.numFrequency.Value = prevValue * 1000;
                    }
                    break;
                case FrequencyUnits.kHz:
                    this.numFrequency.Minimum = 0.01M;
                    this.numFrequency.Increment = 0.001M;
                    this.numFrequency.Maximum = 100;
                    this.numFrequency.DecimalPlaces = 3;
                    if (prevFrequencyUnit == FrequencyUnits.Hz)
                    {
                        this.numFrequency.Value = prevValue / 1000;
                    }
                    break;
            }
            if (this.cbxFrequencyUnit.SelectedItem is FrequencyUnits)
            {
                prevFrequencyUnit = (FrequencyUnits)this.cbxFrequencyUnit.SelectedItem;
            }
        }

        private AmplitudeUnits prevAmplitudeUnit;
        private void cbxAmplitudeUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            decimal prevValue = this.numAmplitudeValue.Value;

            switch ((AmplitudeUnits)this.cbxAmplitudeUnit.SelectedItem)
            {
                case AmplitudeUnits.V:
                    this.numAmplitudeValue.Minimum = 0;
                    this.numAmplitudeValue.Increment = 0.001M;
                    this.numAmplitudeValue.Maximum = 6;
                    this.numAmplitudeValue.DecimalPlaces = 3;
                    if(prevAmplitudeUnit == AmplitudeUnits.mV)
                    {
                        this.numAmplitudeValue.Value = prevValue / 1000;
                    }
                    if(prevAmplitudeUnit == AmplitudeUnits.dBm)
                    {
                        this.numAmplitudeValue.Value = Convert.ToDecimal(MeasureConvert.dBm2Voltage(Convert.ToDouble(prevValue), Convert.ToDouble(((SourceOutputImpedance)cbxOutputImpedance.SelectedItem))));
                    }
                    break;
                case AmplitudeUnits.mV:
                    this.numAmplitudeValue.Minimum = 0;
                    this.numAmplitudeValue.Increment = 1;
                    this.numAmplitudeValue.Maximum = 6000;
                    this.numAmplitudeValue.DecimalPlaces = 0;
                    if (prevAmplitudeUnit == AmplitudeUnits.V)
                    {
                        this.numAmplitudeValue.Value = prevValue * 1000;
                    }
                    if (prevAmplitudeUnit == AmplitudeUnits.dBm)
                    {
                        this.numAmplitudeValue.Value = Convert.ToDecimal(MeasureConvert.dBm2Voltage(Convert.ToDouble(prevValue), Convert.ToDouble(((SourceOutputImpedance)cbxOutputImpedance.SelectedItem)))) * 1000;
                    }
                    break;
                case AmplitudeUnits.dBm:
                    this.numAmplitudeValue.Minimum = -200;
                    this.numAmplitudeValue.Increment = 0.01M;
                    this.numAmplitudeValue.Maximum = 200;
                    this.numAmplitudeValue.DecimalPlaces = 0;
                    if (prevAmplitudeUnit == AmplitudeUnits.V)
                    {
                        this.numAmplitudeValue.Value = Convert.ToDecimal(MeasureConvert.Voltage2dBm(Convert.ToDouble(prevValue), Convert.ToDouble(((SourceOutputImpedance)cbxOutputImpedance.SelectedItem))));
                    }
                    if (prevAmplitudeUnit == AmplitudeUnits.mV)
                    {
                        this.numAmplitudeValue.Value = Convert.ToDecimal(MeasureConvert.Voltage2dBm(Convert.ToDouble(prevValue / 1000), Convert.ToDouble(((SourceOutputImpedance)cbxOutputImpedance.SelectedItem))));
                    }
                    break;
            }
            if(this.cbxAmplitudeUnit.SelectedItem is AmplitudeUnits)
            {
                prevAmplitudeUnit = (AmplitudeUnits)this.cbxAmplitudeUnit.SelectedItem;
            }
        }

        private void cbxOutputImpedance_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((SourceOutputImpedance)e.ListItem).GetDescription();
        }

        private void cbx_PostNotchGain_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((PostNotchGain)e.ListItem).GetDescription();
        }
        private void cbx_DetectorType_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((DetectorType)e.ListItem).GetDescription();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            ResultDescriptor resultDescriptor;
            ((InstrumentHP8903)this.instrument).sourceDescriptors = new List<ResultDescriptor>();

            ((InstrumentHP8903)this.instrument).analyzerType = (AudioAnalyzerType)this.cbxType.SelectedItem;

            ((InstrumentHP8903)this.instrument).enabled_AC = this.chkACLevel.Checked;
            ((InstrumentHP8903)this.instrument).enabled_DC = this.chkDCLevel.Checked;
            ((InstrumentHP8903)this.instrument).enabled_Distortion = this.chkDistortion.Checked;
            ((InstrumentHP8903)this.instrument).enabled_DistortionLevel = this.chkDistortionLevel.Checked;
            ((InstrumentHP8903)this.instrument).enabled_SNR = this.chkSNR.Checked;
            ((InstrumentHP8903)this.instrument).enabled_SINAD = this.chkSINAD.Checked;

            ((InstrumentHP8903)this.instrument).GeneratorFrequencyUnit = (FrequencyUnits)this.cbxFrequencyUnit.SelectedItem;
            ((InstrumentHP8903)this.instrument).GeneratorAmplitudeUnit = (AmplitudeUnits)this.cbxAmplitudeUnit.SelectedItem;
            ((InstrumentHP8903)this.instrument).GeneratorImpedance = (SourceOutputImpedance)this.cbxOutputImpedance.SelectedItem;
            ((InstrumentHP8903)this.instrument).GeneratorFrequency = Convert.ToDouble(this.numFrequency.Value);
            ((InstrumentHP8903)this.instrument).GeneratorAmplitude = Convert.ToDouble(this.numAmplitudeValue.Value);
            // Incomming result descriptors
            ((InstrumentHP8903)this.instrument).frequencySourceDescriptor = this.cbxSourceFrequency.SelectedItem as ResultDescriptor;
            ((InstrumentHP8903)this.instrument).amplitudeSourceDescriptor = this.cbxSourceAmplitude.SelectedItem as ResultDescriptor;

            ((InstrumentHP8903)this.instrument).AC_InputRange = (InputRangeAC)this.cbxAC_Range.SelectedItem;
            ((InstrumentHP8903)this.instrument).AC_ScaleType = (ScaleType)this.cbxAC_LinLog.SelectedItem;
            ((InstrumentHP8903)this.instrument).AC_Ratio = this.chkAC_Ratio.Checked;
            ((InstrumentHP8903)this.instrument).AC_PostNotchGain = (PostNotchGain)this.cbxAC_PostNotchGain.SelectedItem;
            ((InstrumentHP8903)this.instrument).AC_DetectorType = (DetectorType)this.cbxAC_DetectorType.SelectedItem;
            ((InstrumentHP8903)this.instrument).AC_HoldNotchTuning = this.chkAC_HoldNotchTuning.Checked;
            ((InstrumentHP8903)this.instrument).AC_PowerDisplay = this.chkAC_PowerDisplay.Checked;
            ((InstrumentHP8903)this.instrument).AC_PowerOhms = Convert.ToInt32(this.numAC_PowerLoad.Value);
            ((InstrumentHP8903)this.instrument).AC_HPLeft = this.chkAC_HPLeft.Checked;
            ((InstrumentHP8903)this.instrument).AC_HPRight = this.chkAC_HPRight.Checked;
            ((InstrumentHP8903)this.instrument).AC_LP30kHz = this.chkAC_LP30kHz.Checked;
            ((InstrumentHP8903)this.instrument).AC_LP80kHz = this.chkAC_LP80kHz.Checked;

            if (((InstrumentHP8903)this.instrument).enabled_AC)
            {
                resultDescriptor = new ResultDescriptor();
                resultDescriptor.SourceGuid = ((InstrumentHP8903)this.instrument).InstanceID;
                resultDescriptor.ChannelID = (int)Measurements.AC_Level;
                resultDescriptor.ChannelName = "AC Level";
                // Power???
                resultDescriptor.unit = ((InstrumentHP8903)this.instrument).AC_Ratio ? (((InstrumentHP8903)this.instrument).AC_ScaleType == ScaleType.Lin ? MeasureUnit.percent : MeasureUnit.decibel) : (((InstrumentHP8903)this.instrument).AC_ScaleType == ScaleType.Lin ? MeasureUnit.volt : MeasureUnit.dBm);
                ((InstrumentHP8903)this.instrument).sourceDescriptors.Add(resultDescriptor);
            }

            ((InstrumentHP8903)this.instrument).DC_InputRange = (InputRangeDC)this.cbxDC_Range.SelectedItem;
            ((InstrumentHP8903)this.instrument).DC_ScaleType = (ScaleType)this.cbxDC_LinLog.SelectedItem;
            ((InstrumentHP8903)this.instrument).DC_Ratio = this.chkDC_Ratio.Checked;

            if (((InstrumentHP8903)this.instrument).enabled_DC)
            {
                resultDescriptor = new ResultDescriptor();
                resultDescriptor.SourceGuid = ((InstrumentHP8903)this.instrument).InstanceID;
                resultDescriptor.ChannelID = (int)Measurements.DC_Level;
                resultDescriptor.ChannelName = "DC Level";
                // Power???
                resultDescriptor.unit = ((InstrumentHP8903)this.instrument).DC_Ratio ? (((InstrumentHP8903)this.instrument).DC_ScaleType == ScaleType.Lin ? MeasureUnit.percent : MeasureUnit.decibel) : (((InstrumentHP8903)this.instrument).DC_ScaleType == ScaleType.Lin ? MeasureUnit.volt : MeasureUnit.dBm);
                ((InstrumentHP8903)this.instrument).sourceDescriptors.Add(resultDescriptor);
            }

            ((InstrumentHP8903)this.instrument).Dist_InputRange = (InputRangeAC)this.cbxDist_Range.SelectedItem;
            ((InstrumentHP8903)this.instrument).Dist_ScaleType = (ScaleType)this.cbxDist_LinLog.SelectedItem;
            ((InstrumentHP8903)this.instrument).Dist_Ratio = this.chkDist_Ratio.Checked;
            ((InstrumentHP8903)this.instrument).Dist_PostNotchGain = (PostNotchGain)this.cbxDist_PostNotchGain.SelectedItem;
            ((InstrumentHP8903)this.instrument).Dist_DetectorType = (DetectorType)this.cbxDist_DetectorType.SelectedItem;
            ((InstrumentHP8903)this.instrument).Dist_HoldNotchTuning = this.chkDist_HoldNotchTuning.Checked;
            ((InstrumentHP8903)this.instrument).Dist_HPLeft = this.chkDist_HPLeft.Checked;
            ((InstrumentHP8903)this.instrument).Dist_HPRight = this.chkDist_HPRight.Checked;
            ((InstrumentHP8903)this.instrument).Dist_LP30kHz = this.chkDist_LP30kHz.Checked;
            ((InstrumentHP8903)this.instrument).Dist_LP80kHz = this.chkDist_LP80kHz.Checked;

            if (((InstrumentHP8903)this.instrument).enabled_Distortion)
            {
                resultDescriptor = new ResultDescriptor();
                resultDescriptor.SourceGuid = ((InstrumentHP8903)this.instrument).InstanceID;
                resultDescriptor.ChannelID = (int)Measurements.Distortion;
                resultDescriptor.ChannelName = "Distortion";
                resultDescriptor.unit = (((InstrumentHP8903)this.instrument).Dist_ScaleType == ScaleType.Lin ? MeasureUnit.percent : MeasureUnit.decibel);
                ((InstrumentHP8903)this.instrument).sourceDescriptors.Add(resultDescriptor);
            }

            ((InstrumentHP8903)this.instrument).DistLevel_InputRange = (InputRangeAC)this.cbxDistLevel_Range.SelectedItem;
            ((InstrumentHP8903)this.instrument).DistLevel_ScaleType = (ScaleType)this.cbxDistLevel_LinLog.SelectedItem;
            ((InstrumentHP8903)this.instrument).DistLevel_Ratio = this.chkDistLevel_Ratio.Checked;
            ((InstrumentHP8903)this.instrument).DistLevel_PostNotchGain = (PostNotchGain)this.cbxDistLevel_PostNotchGain.SelectedItem;
            ((InstrumentHP8903)this.instrument).DistLevel_DetectorType = (DetectorType)this.cbxDistLevel_DetectorType.SelectedItem;
            ((InstrumentHP8903)this.instrument).DistLevel_HoldNotchTuning = this.chkDistLevel_HoldNotchTuning.Checked;
            ((InstrumentHP8903)this.instrument).DistLevel_HPLeft = this.chkDistLevel_HPLeft.Checked;
            ((InstrumentHP8903)this.instrument).DistLevel_HPRight = this.chkDistLevel_HPRight.Checked;
            ((InstrumentHP8903)this.instrument).DistLevel_LP30kHz = this.chkDistLevel_LP30kHz.Checked;
            ((InstrumentHP8903)this.instrument).DistLevel_LP80kHz = this.chkDistLevel_LP80kHz.Checked;

            if (((InstrumentHP8903)this.instrument).enabled_DistortionLevel)
            {
                resultDescriptor = new ResultDescriptor();
                resultDescriptor.SourceGuid = ((InstrumentHP8903)this.instrument).InstanceID;
                resultDescriptor.ChannelID = (int)Measurements.Distortion_Level;
                resultDescriptor.ChannelName = "Distortion Level";
                resultDescriptor.unit = ((InstrumentHP8903)this.instrument).Dist_Ratio ? (((InstrumentHP8903)this.instrument).Dist_ScaleType == ScaleType.Lin ? MeasureUnit.percent : MeasureUnit.decibel) : (((InstrumentHP8903)this.instrument).Dist_ScaleType == ScaleType.Lin ? MeasureUnit.volt : MeasureUnit.dBm);
                ((InstrumentHP8903)this.instrument).sourceDescriptors.Add(resultDescriptor);
            }

            ((InstrumentHP8903)this.instrument).SNR_InputRange = (InputRangeAC)this.cbxSNR_Range.SelectedItem;
            ((InstrumentHP8903)this.instrument).SNR_ScaleType = (ScaleType)this.cbxSNR_LinLog.SelectedItem;
            ((InstrumentHP8903)this.instrument).SNR_Ratio = this.chkSNR_Ratio.Checked;
            ((InstrumentHP8903)this.instrument).SNR_PostNotchGain = (PostNotchGain)this.cbxSNR_PostNotchGain.SelectedItem;
            ((InstrumentHP8903)this.instrument).SNR_DetectorType = (DetectorType)this.cbxSNR_DetectorType.SelectedItem;
            ((InstrumentHP8903)this.instrument).SNR_HoldNotchTuning = this.chkSNR_HoldNotchTuning.Checked;
            ((InstrumentHP8903)this.instrument).SNR_HPLeft = this.chkSNR_HPLeft.Checked;
            ((InstrumentHP8903)this.instrument).SNR_HPRight = this.chkSNR_HPRight.Checked;
            ((InstrumentHP8903)this.instrument).SNR_LP30kHz = this.chkSNR_LP30kHz.Checked;
            ((InstrumentHP8903)this.instrument).SNR_LP80kHz = this.chkSNR_LP80kHz.Checked;
            ((InstrumentHP8903)this.instrument).SNR_Delay = (SNRDelay)this.cbxSNR_Delay.SelectedItem;

            if (((InstrumentHP8903)this.instrument).enabled_SNR)
            {
                resultDescriptor = new ResultDescriptor();
                resultDescriptor.SourceGuid = ((InstrumentHP8903)this.instrument).InstanceID;
                resultDescriptor.ChannelID = (int)Measurements.SNR;
                resultDescriptor.ChannelName = "SNR";
                resultDescriptor.unit = (((InstrumentHP8903)this.instrument).SNR_ScaleType == ScaleType.Lin ? MeasureUnit.percent : MeasureUnit.decibel);
                ((InstrumentHP8903)this.instrument).sourceDescriptors.Add(resultDescriptor);
            }

            ((InstrumentHP8903)this.instrument).SINAD_InputRange = (InputRangeAC)this.cbxSINAD_Range.SelectedItem;
            ((InstrumentHP8903)this.instrument).SINAD_ScaleType = (ScaleType)this.cbxSINAD_LinLog.SelectedItem;
            ((InstrumentHP8903)this.instrument).SINAD_Ratio = this.chkSINAD_Ratio.Checked;
            ((InstrumentHP8903)this.instrument).SINAD_PostNotchGain = (PostNotchGain)this.cbxSINAD_PostNotchGain.SelectedItem;
            ((InstrumentHP8903)this.instrument).SINAD_DetectorType = (DetectorType)this.cbxSINAD_DetectorType.SelectedItem;
            ((InstrumentHP8903)this.instrument).SINAD_HoldNotchTuning = this.chkSINAD_HoldNotchTuning.Checked;
            ((InstrumentHP8903)this.instrument).SINAD_HPLeft = this.chkSINAD_HPLeft.Checked;
            ((InstrumentHP8903)this.instrument).SINAD_HPRight = this.chkSINAD_HPRight.Checked;
            ((InstrumentHP8903)this.instrument).SINAD_LP30kHz = this.chkSINAD_LP30kHz.Checked;
            ((InstrumentHP8903)this.instrument).SINAD_LP80kHz = this.chkSINAD_LP80kHz.Checked;
            ((InstrumentHP8903)this.instrument).SINAD_Range = (SinadRange)this.cbxSINAD_SINADRange.SelectedItem;

            if (((InstrumentHP8903)this.instrument).enabled_SINAD)
            {
                resultDescriptor = new ResultDescriptor();
                resultDescriptor.SourceGuid = ((InstrumentHP8903)this.instrument).InstanceID;
                resultDescriptor.ChannelID = (int)Measurements.SINAD;
                resultDescriptor.ChannelName = "SINAD";
                resultDescriptor.unit = (((InstrumentHP8903)this.instrument).SINAD_ScaleType == ScaleType.Lin ? MeasureUnit.percent : MeasureUnit.decibel);
                ((InstrumentHP8903)this.instrument).sourceDescriptors.Add(resultDescriptor);
            }

            base.IsSaved = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            FormConnectionList.SetupConnection(ref this.instrument.conn);
        }
    }
}
