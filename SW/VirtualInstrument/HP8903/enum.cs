﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.ComponentModel;

namespace SUF.Instrument.HP8903
{
    public enum AudioAnalyzerType
    {
        HP8903A,
        HP8903B,
        HB8903E
    }

    public enum FrequencyUnits
    {
        [Command("HZ")]
        Hz,
        [Command("KZ")]
        kHz
    }

    public enum AmplitudeUnits
    {
        [Command("VL")]
        V,
        [Command("MV")]
        mV,
        [Command("DV")]
        dBm
    }

    public enum Ratio
    {
        [Command("R1")]
        On,
        [Command("R0")]
        Off
    }

    public enum ScaleType
    {
        [Command("LG")]
        Log,
        [Command("LN")]
        Lin
    }

    public enum SourceOutputImpedance
    {
        [Command("47.0SP"), Description("50ohm")]
        Ohm50 = 50,
        [Command("47.1SP"), Description("600ohm")]
        Ohm600 = 600
    }

    public enum InputRangeAC
    {
        [Command("1.0SP"), Description("Auto")]
        Auto,
        [Command("1.1SP"), Description("300V")]
        AC300V,
        [Command("1.2SP"), Description("189V")]
        AC189V,
        [Command("1.3SP"), Description("119V")]
        AC119V,
        [Command("1.4SP"), Description("75.4V")]
        AC75V4,
        [Command("1.5SP"), Description("47.6V")]
        AC47V6,
        [Command("1.6SP"), Description("30V")]
        AC30V,
        [Command("1.7SP"), Description("18.9V")]
        AC18V9,
        [Command("1.8SP"), Description("11.9V")]
        AC11V9,
        [Command("1.9SP"), Description("7.54V")]
        AC7V54,
        [Command("1.10SP"), Description("4.76V")]
        AC4V76,
        [Command("1.11SP"), Description("3V")]
        AC3V,
        [Command("1.12SP"), Description("1.89V")]
        AC1V89,
        [Command("1.13SP"), Description("1.19V")]
        AC1V19,
        [Command("1.14SP"), Description("754mV")]
        AC754mV,
        [Command("1.15SP"), Description("476mV")]
        AC476mV,
        [Command("1.16SP"), Description("300mV")]
        AC300mV,
        [Command("1.17SP"), Description("189mV")]
        AC189mV,
        [Command("1.18SP"), Description("119mV")]
        AC119mV,
        [Command("1.19SP"), Description("75.4mV")]
        AC75mV4,
    }

    public enum InputRangeDC
    {
        [Command("2.0SP"), Description("Auto")]
        Auto,
        [Command("2.1SP"), Description("300V")]
        DC300V,
        [Command("2.2SP"), Description("64V")]
        DC64V,
        [Command("2.3SP"), Description("16V")]
        DC16V,
        [Command("2.4SP"), Description("4V")]
        DC4V
    }

    public enum PostNotchGain
    {
        [Command("3.0SP"), Description("Auto")]
        Auto,
        [Command("3.1SP"), Description("0 dB")]
        DC300V,
        [Command("3.2SP"), Description("20 dB")]
        DC64V,
        [Command("3.3SP"), Description("40 dB")]
        DC16V,
        [Command("3.4SP"), Description("50 dB")]
        DC4V
    }

    public enum DetectorType
    {
        [Command("5.0SP"), Description("RMS (Fast)")]
        RMS_Fast,
        [Command("5.1SP"), Description("RMS (Slow)")]
        RMS_Slow,
        [Command("5.2SP"), Description("AVG (Fast)")]
        AVG_Fast,
        [Command("5.3SP"), Description("AVG (Slow)")]
        AVG_Slow,
        [Command("5.7SP"), Description("Quasi-peak")]
        QuasiPeak
    }

    public enum SinadRange
    {
        [Command("7.0SP"), Description("18 dB")]
        Range18dB,
        [Command("7.1SP"), Description("24 dB")]
        Range24dB
    }

    public enum SNRDelay
    {
        [Command("12.0SP"), Description("Auto")]
        Auto,
        [Command("12.1SP"), Description("200ms")]
        Delay200ms,
        [Command("12.2SP"), Description("400ms")]
        Delay400ms,
        [Command("12.3SP"), Description("600ms")]
        Delay600ms,
        [Command("12.4SP"), Description("800ms")]
        Delay800ms,
        [Command("12.5SP"), Description("1.0s")]
        Delay1s,
        [Command("12.6SP"), Description("1.2s")]
        Delay1s2,
        [Command("12.7SP"), Description("1.4s")]
        Delay1s4,
        [Command("12.8SP"), Description("1.6s")]
        Delay1s6,
        [Command("12.9SP"), Description("1.8s")]
        Delay1s8
    }

    public enum Measurements
    {
        [Command("S1")]
        DC_Level = 1,
        [Command("M1")]
        AC_Level = 0,
        [Command("M3")]
        Distortion = 2,
        [Command("S3")]
        Distortion_Level = 3,
        [Command("M2")]
        SINAD = 5,
        [Command("S2")]
        SNR = 4
    }

    public enum HP_Filters
    {
        [Command("H1")]
        Left,
        [Command("M2")]
        Right,
        [Command("H0")]
        Off
    }

    public enum LP_Filters
    {
        [Command("L1")]
        filter30kHz,
        [Command("L2")]
        filter80kHz,
        [Command("L0")]
        Off
    }


    public enum Trigger
    {
        [Command("T0")]
        FreeRun,
        [Command("T1")]
        Hold,
        [Command("T2")]
        Trigger,
        [Command("T3")]
        DelayedTrigger
    }

    public enum ReadDisplay
    {
        [Command("RL")]
        Left,
        [Command("RR")]
        Right
    }

    public enum Units
    {
        [Command("HZ")]
        Hz,
        [Command("KZ")]
        kHz,
        [Command("VL")]
        V,
        [Command("MV")]
        mV,
        [Command("DB")]
        dB,
        [Command("DV")]
        dBm
    }

    public enum GeneratorParam
    {
        [Command("FR")]
        Frequency,
        [Command("AP")]
        Amplitude
    }

    public enum NotchTuning
    {
        [Command("N0")]
        Automatic,
        [Command("N1")]
        Hold
    }
}
