﻿namespace SUF.Instrument.HP8903
{
    partial class FormHP8903InstrumentSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxFrequency = new System.Windows.Forms.GroupBox();
            this.cbxFrequencyUnit = new System.Windows.Forms.ComboBox();
            this.numFrequency = new System.Windows.Forms.NumericUpDown();
            this.lblFrequencyValue = new System.Windows.Forms.Label();
            this.lblFrequencySource = new System.Windows.Forms.Label();
            this.cbxSourceFrequency = new System.Windows.Forms.ComboBox();
            this.gbxAmplitude = new System.Windows.Forms.GroupBox();
            this.cbxAmplitudeUnit = new System.Windows.Forms.ComboBox();
            this.numAmplitudeValue = new System.Windows.Forms.NumericUpDown();
            this.lblAmplitudeValue = new System.Windows.Forms.Label();
            this.lblAmplitudeSource = new System.Windows.Forms.Label();
            this.cbxSourceAmplitude = new System.Windows.Forms.ComboBox();
            this.gbxFilter = new System.Windows.Forms.GroupBox();
            this.chkAC_LP80kHz = new System.Windows.Forms.CheckBox();
            this.chkAC_LP30kHz = new System.Windows.Forms.CheckBox();
            this.chkAC_HPRight = new System.Windows.Forms.CheckBox();
            this.chkAC_HPLeft = new System.Windows.Forms.CheckBox();
            this.chkAC_Ratio = new System.Windows.Forms.CheckBox();
            this.chkSNR = new System.Windows.Forms.CheckBox();
            this.chkSINAD = new System.Windows.Forms.CheckBox();
            this.chkDistortionLevel = new System.Windows.Forms.CheckBox();
            this.chkDistortion = new System.Windows.Forms.CheckBox();
            this.chkACLevel = new System.Windows.Forms.CheckBox();
            this.chkDCLevel = new System.Windows.Forms.CheckBox();
            this.btnConnection = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.chkFrequency = new System.Windows.Forms.CheckBox();
            this.tbxControl = new System.Windows.Forms.TabControl();
            this.tabSettings = new System.Windows.Forms.TabPage();
            this.cbxType = new System.Windows.Forms.ComboBox();
            this.lblType = new System.Windows.Forms.Label();
            this.tabGenerator = new System.Windows.Forms.TabPage();
            this.cbxOutputImpedance = new System.Windows.Forms.ComboBox();
            this.lblOutputImpedance = new System.Windows.Forms.Label();
            this.tabAC = new System.Windows.Forms.TabPage();
            this.lblAC_PowerLoad = new System.Windows.Forms.Label();
            this.numAC_PowerLoad = new System.Windows.Forms.NumericUpDown();
            this.chkAC_PowerDisplay = new System.Windows.Forms.CheckBox();
            this.chkAC_HoldNotchTuning = new System.Windows.Forms.CheckBox();
            this.cbxAC_DetectorType = new System.Windows.Forms.ComboBox();
            this.lblAC_DetectorType = new System.Windows.Forms.Label();
            this.cbxAC_PostNotchGain = new System.Windows.Forms.ComboBox();
            this.lblAC_PostNotchGain = new System.Windows.Forms.Label();
            this.cbxAC_LinLog = new System.Windows.Forms.ComboBox();
            this.cbxAC_Range = new System.Windows.Forms.ComboBox();
            this.lblACRange = new System.Windows.Forms.Label();
            this.tabDC = new System.Windows.Forms.TabPage();
            this.cbxDC_LinLog = new System.Windows.Forms.ComboBox();
            this.chkDC_Ratio = new System.Windows.Forms.CheckBox();
            this.cbxDC_Range = new System.Windows.Forms.ComboBox();
            this.lblDCRange = new System.Windows.Forms.Label();
            this.tabDistortion = new System.Windows.Forms.TabPage();
            this.chkDist_HoldNotchTuning = new System.Windows.Forms.CheckBox();
            this.cbxDist_DetectorType = new System.Windows.Forms.ComboBox();
            this.lblDist_DetectorType = new System.Windows.Forms.Label();
            this.cbxDist_PostNotchGain = new System.Windows.Forms.ComboBox();
            this.lblDist_PostNotchGain = new System.Windows.Forms.Label();
            this.cbxDist_LinLog = new System.Windows.Forms.ComboBox();
            this.gbxDist_Filter = new System.Windows.Forms.GroupBox();
            this.chkDist_LP80kHz = new System.Windows.Forms.CheckBox();
            this.chkDist_LP30kHz = new System.Windows.Forms.CheckBox();
            this.chkDist_HPRight = new System.Windows.Forms.CheckBox();
            this.chkDist_HPLeft = new System.Windows.Forms.CheckBox();
            this.chkDist_Ratio = new System.Windows.Forms.CheckBox();
            this.cbxDist_Range = new System.Windows.Forms.ComboBox();
            this.lblDist_Range = new System.Windows.Forms.Label();
            this.tabDistortionLevel = new System.Windows.Forms.TabPage();
            this.chkDistLevel_HoldNotchTuning = new System.Windows.Forms.CheckBox();
            this.cbxDistLevel_DetectorType = new System.Windows.Forms.ComboBox();
            this.lblDistLevel_DetectorType = new System.Windows.Forms.Label();
            this.cbxDistLevel_PostNotchGain = new System.Windows.Forms.ComboBox();
            this.lblDistLevel_PostNotchGain = new System.Windows.Forms.Label();
            this.cbxDistLevel_LinLog = new System.Windows.Forms.ComboBox();
            this.gbxDistLevel_Filter = new System.Windows.Forms.GroupBox();
            this.chkDistLevel_LP80kHz = new System.Windows.Forms.CheckBox();
            this.chkDistLevel_LP30kHz = new System.Windows.Forms.CheckBox();
            this.chkDistLevel_HPRight = new System.Windows.Forms.CheckBox();
            this.chkDistLevel_HPLeft = new System.Windows.Forms.CheckBox();
            this.chkDistLevel_Ratio = new System.Windows.Forms.CheckBox();
            this.cbxDistLevel_Range = new System.Windows.Forms.ComboBox();
            this.lblDistLevel_Range = new System.Windows.Forms.Label();
            this.tabSNR = new System.Windows.Forms.TabPage();
            this.chkSNR_HoldNotchTuning = new System.Windows.Forms.CheckBox();
            this.cbxSNR_DetectorType = new System.Windows.Forms.ComboBox();
            this.lblSNR_DetectorType = new System.Windows.Forms.Label();
            this.cbxSNR_PostNotchGain = new System.Windows.Forms.ComboBox();
            this.lblSNR_PostNotchGain = new System.Windows.Forms.Label();
            this.cbxSNR_LinLog = new System.Windows.Forms.ComboBox();
            this.gbxSNR_Filter = new System.Windows.Forms.GroupBox();
            this.chkSNR_LP80kHz = new System.Windows.Forms.CheckBox();
            this.chkSNR_LP30kHz = new System.Windows.Forms.CheckBox();
            this.chkSNR_HPRight = new System.Windows.Forms.CheckBox();
            this.chkSNR_HPLeft = new System.Windows.Forms.CheckBox();
            this.chkSNR_Ratio = new System.Windows.Forms.CheckBox();
            this.cbxSNR_Range = new System.Windows.Forms.ComboBox();
            this.lblSNR_Range = new System.Windows.Forms.Label();
            this.tabSINAD = new System.Windows.Forms.TabPage();
            this.cbxSINAD_SINADRange = new System.Windows.Forms.ComboBox();
            this.lblSINAD_SINADRange = new System.Windows.Forms.Label();
            this.chkSINAD_HoldNotchTuning = new System.Windows.Forms.CheckBox();
            this.cbxSINAD_DetectorType = new System.Windows.Forms.ComboBox();
            this.lblSINAD_DetectorType = new System.Windows.Forms.Label();
            this.cbxSINAD_PostNotchGain = new System.Windows.Forms.ComboBox();
            this.lblSINAD_PostNotchGain = new System.Windows.Forms.Label();
            this.cbxSINAD_LinLog = new System.Windows.Forms.ComboBox();
            this.gbxSINAD_Filter = new System.Windows.Forms.GroupBox();
            this.chkSINAD_LP80kHz = new System.Windows.Forms.CheckBox();
            this.chkSINAD_LP30kHz = new System.Windows.Forms.CheckBox();
            this.chkSINAD_HPRight = new System.Windows.Forms.CheckBox();
            this.chkSINAD_HPLeft = new System.Windows.Forms.CheckBox();
            this.chkSINAD_Ratio = new System.Windows.Forms.CheckBox();
            this.cbxSINAD_Range = new System.Windows.Forms.ComboBox();
            this.lblSINAD_Range = new System.Windows.Forms.Label();
            this.tabFrequency = new System.Windows.Forms.TabPage();
            this.lblSNR_Delay = new System.Windows.Forms.Label();
            this.cbxSNR_Delay = new System.Windows.Forms.ComboBox();
            this.gbxFrequency.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFrequency)).BeginInit();
            this.gbxAmplitude.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAmplitudeValue)).BeginInit();
            this.gbxFilter.SuspendLayout();
            this.tbxControl.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.tabGenerator.SuspendLayout();
            this.tabAC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAC_PowerLoad)).BeginInit();
            this.tabDC.SuspendLayout();
            this.tabDistortion.SuspendLayout();
            this.gbxDist_Filter.SuspendLayout();
            this.tabDistortionLevel.SuspendLayout();
            this.gbxDistLevel_Filter.SuspendLayout();
            this.tabSNR.SuspendLayout();
            this.gbxSNR_Filter.SuspendLayout();
            this.tabSINAD.SuspendLayout();
            this.gbxSINAD_Filter.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxFrequency
            // 
            this.gbxFrequency.Controls.Add(this.cbxFrequencyUnit);
            this.gbxFrequency.Controls.Add(this.numFrequency);
            this.gbxFrequency.Controls.Add(this.lblFrequencyValue);
            this.gbxFrequency.Controls.Add(this.lblFrequencySource);
            this.gbxFrequency.Controls.Add(this.cbxSourceFrequency);
            this.gbxFrequency.Location = new System.Drawing.Point(4, 11);
            this.gbxFrequency.Name = "gbxFrequency";
            this.gbxFrequency.Size = new System.Drawing.Size(209, 86);
            this.gbxFrequency.TabIndex = 3;
            this.gbxFrequency.TabStop = false;
            this.gbxFrequency.Text = "Frequency";
            // 
            // cbxFrequencyUnit
            // 
            this.cbxFrequencyUnit.FormattingEnabled = true;
            this.cbxFrequencyUnit.Location = new System.Drawing.Point(140, 47);
            this.cbxFrequencyUnit.Name = "cbxFrequencyUnit";
            this.cbxFrequencyUnit.Size = new System.Drawing.Size(53, 21);
            this.cbxFrequencyUnit.TabIndex = 4;
            this.cbxFrequencyUnit.SelectedIndexChanged += new System.EventHandler(this.cbxFrequencyUnit_SelectedIndexChanged);
            // 
            // numFrequency
            // 
            this.numFrequency.Location = new System.Drawing.Point(57, 48);
            this.numFrequency.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numFrequency.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numFrequency.Name = "numFrequency";
            this.numFrequency.Size = new System.Drawing.Size(71, 20);
            this.numFrequency.TabIndex = 3;
            this.numFrequency.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // lblFrequencyValue
            // 
            this.lblFrequencyValue.AutoSize = true;
            this.lblFrequencyValue.Location = new System.Drawing.Point(6, 50);
            this.lblFrequencyValue.Name = "lblFrequencyValue";
            this.lblFrequencyValue.Size = new System.Drawing.Size(37, 13);
            this.lblFrequencyValue.TabIndex = 2;
            this.lblFrequencyValue.Text = "Value:";
            // 
            // lblFrequencySource
            // 
            this.lblFrequencySource.AutoSize = true;
            this.lblFrequencySource.Location = new System.Drawing.Point(6, 22);
            this.lblFrequencySource.Name = "lblFrequencySource";
            this.lblFrequencySource.Size = new System.Drawing.Size(44, 13);
            this.lblFrequencySource.TabIndex = 1;
            this.lblFrequencySource.Text = "Source:";
            // 
            // cbxSourceFrequency
            // 
            this.cbxSourceFrequency.FormattingEnabled = true;
            this.cbxSourceFrequency.Location = new System.Drawing.Point(56, 20);
            this.cbxSourceFrequency.Name = "cbxSourceFrequency";
            this.cbxSourceFrequency.Size = new System.Drawing.Size(137, 21);
            this.cbxSourceFrequency.TabIndex = 0;
            this.cbxSourceFrequency.SelectedIndexChanged += new System.EventHandler(this.cbxSourceFrequency_SelectedIndexChanged);
            // 
            // gbxAmplitude
            // 
            this.gbxAmplitude.Controls.Add(this.cbxAmplitudeUnit);
            this.gbxAmplitude.Controls.Add(this.numAmplitudeValue);
            this.gbxAmplitude.Controls.Add(this.lblAmplitudeValue);
            this.gbxAmplitude.Controls.Add(this.lblAmplitudeSource);
            this.gbxAmplitude.Controls.Add(this.cbxSourceAmplitude);
            this.gbxAmplitude.Location = new System.Drawing.Point(4, 103);
            this.gbxAmplitude.Name = "gbxAmplitude";
            this.gbxAmplitude.Size = new System.Drawing.Size(209, 89);
            this.gbxAmplitude.TabIndex = 2;
            this.gbxAmplitude.TabStop = false;
            this.gbxAmplitude.Text = "Amplitude";
            // 
            // cbxAmplitudeUnit
            // 
            this.cbxAmplitudeUnit.FormattingEnabled = true;
            this.cbxAmplitudeUnit.Location = new System.Drawing.Point(140, 51);
            this.cbxAmplitudeUnit.Name = "cbxAmplitudeUnit";
            this.cbxAmplitudeUnit.Size = new System.Drawing.Size(53, 21);
            this.cbxAmplitudeUnit.TabIndex = 8;
            this.cbxAmplitudeUnit.SelectedIndexChanged += new System.EventHandler(this.cbxAmplitudeUnit_SelectedIndexChanged);
            // 
            // numAmplitudeValue
            // 
            this.numAmplitudeValue.Location = new System.Drawing.Point(57, 52);
            this.numAmplitudeValue.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numAmplitudeValue.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numAmplitudeValue.Name = "numAmplitudeValue";
            this.numAmplitudeValue.Size = new System.Drawing.Size(71, 20);
            this.numAmplitudeValue.TabIndex = 7;
            this.numAmplitudeValue.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // lblAmplitudeValue
            // 
            this.lblAmplitudeValue.AutoSize = true;
            this.lblAmplitudeValue.Location = new System.Drawing.Point(6, 55);
            this.lblAmplitudeValue.Name = "lblAmplitudeValue";
            this.lblAmplitudeValue.Size = new System.Drawing.Size(37, 13);
            this.lblAmplitudeValue.TabIndex = 6;
            this.lblAmplitudeValue.Text = "Value:";
            // 
            // lblAmplitudeSource
            // 
            this.lblAmplitudeSource.AutoSize = true;
            this.lblAmplitudeSource.Location = new System.Drawing.Point(6, 22);
            this.lblAmplitudeSource.Name = "lblAmplitudeSource";
            this.lblAmplitudeSource.Size = new System.Drawing.Size(44, 13);
            this.lblAmplitudeSource.TabIndex = 5;
            this.lblAmplitudeSource.Text = "Source:";
            // 
            // cbxSourceAmplitude
            // 
            this.cbxSourceAmplitude.FormattingEnabled = true;
            this.cbxSourceAmplitude.Location = new System.Drawing.Point(57, 20);
            this.cbxSourceAmplitude.Name = "cbxSourceAmplitude";
            this.cbxSourceAmplitude.Size = new System.Drawing.Size(136, 21);
            this.cbxSourceAmplitude.TabIndex = 1;
            this.cbxSourceAmplitude.SelectedIndexChanged += new System.EventHandler(this.cbxSourceAmplitude_SelectedIndexChanged);
            // 
            // gbxFilter
            // 
            this.gbxFilter.Controls.Add(this.chkAC_LP80kHz);
            this.gbxFilter.Controls.Add(this.chkAC_LP30kHz);
            this.gbxFilter.Controls.Add(this.chkAC_HPRight);
            this.gbxFilter.Controls.Add(this.chkAC_HPLeft);
            this.gbxFilter.Location = new System.Drawing.Point(15, 36);
            this.gbxFilter.Name = "gbxFilter";
            this.gbxFilter.Size = new System.Drawing.Size(129, 115);
            this.gbxFilter.TabIndex = 1;
            this.gbxFilter.TabStop = false;
            this.gbxFilter.Text = "Filter";
            // 
            // chkAC_LP80kHz
            // 
            this.chkAC_LP80kHz.AutoSize = true;
            this.chkAC_LP80kHz.Location = new System.Drawing.Point(7, 87);
            this.chkAC_LP80kHz.Name = "chkAC_LP80kHz";
            this.chkAC_LP80kHz.Size = new System.Drawing.Size(106, 17);
            this.chkAC_LP80kHz.TabIndex = 3;
            this.chkAC_LP80kHz.Text = "Low Pass 80kHz";
            this.chkAC_LP80kHz.UseVisualStyleBackColor = true;
            // 
            // chkAC_LP30kHz
            // 
            this.chkAC_LP30kHz.AutoSize = true;
            this.chkAC_LP30kHz.Location = new System.Drawing.Point(7, 64);
            this.chkAC_LP30kHz.Name = "chkAC_LP30kHz";
            this.chkAC_LP30kHz.Size = new System.Drawing.Size(106, 17);
            this.chkAC_LP30kHz.TabIndex = 2;
            this.chkAC_LP30kHz.Text = "Low Pass 30kHz";
            this.chkAC_LP30kHz.UseVisualStyleBackColor = true;
            // 
            // chkAC_HPRight
            // 
            this.chkAC_HPRight.AutoSize = true;
            this.chkAC_HPRight.Location = new System.Drawing.Point(7, 41);
            this.chkAC_HPRight.Name = "chkAC_HPRight";
            this.chkAC_HPRight.Size = new System.Drawing.Size(108, 17);
            this.chkAC_HPRight.TabIndex = 1;
            this.chkAC_HPRight.Text = "High Pass (Right)";
            this.chkAC_HPRight.UseVisualStyleBackColor = true;
            // 
            // chkAC_HPLeft
            // 
            this.chkAC_HPLeft.AutoSize = true;
            this.chkAC_HPLeft.Location = new System.Drawing.Point(7, 20);
            this.chkAC_HPLeft.Name = "chkAC_HPLeft";
            this.chkAC_HPLeft.Size = new System.Drawing.Size(101, 17);
            this.chkAC_HPLeft.TabIndex = 0;
            this.chkAC_HPLeft.Text = "High Pass (Left)";
            this.chkAC_HPLeft.UseVisualStyleBackColor = true;
            // 
            // chkAC_Ratio
            // 
            this.chkAC_Ratio.AutoSize = true;
            this.chkAC_Ratio.Location = new System.Drawing.Point(168, 36);
            this.chkAC_Ratio.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkAC_Ratio.Name = "chkAC_Ratio";
            this.chkAC_Ratio.Size = new System.Drawing.Size(51, 17);
            this.chkAC_Ratio.TabIndex = 8;
            this.chkAC_Ratio.Text = "Ratio";
            this.chkAC_Ratio.UseVisualStyleBackColor = true;
            // 
            // chkSNR
            // 
            this.chkSNR.AutoSize = true;
            this.chkSNR.Location = new System.Drawing.Point(13, 107);
            this.chkSNR.Name = "chkSNR";
            this.chkSNR.Size = new System.Drawing.Size(49, 17);
            this.chkSNR.TabIndex = 7;
            this.chkSNR.Text = "SNR";
            this.chkSNR.UseVisualStyleBackColor = true;
            this.chkSNR.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // chkSINAD
            // 
            this.chkSINAD.AutoSize = true;
            this.chkSINAD.Location = new System.Drawing.Point(13, 131);
            this.chkSINAD.Name = "chkSINAD";
            this.chkSINAD.Size = new System.Drawing.Size(59, 17);
            this.chkSINAD.TabIndex = 6;
            this.chkSINAD.Text = "SINAD";
            this.chkSINAD.UseVisualStyleBackColor = true;
            this.chkSINAD.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // chkDistortionLevel
            // 
            this.chkDistortionLevel.AutoSize = true;
            this.chkDistortionLevel.Location = new System.Drawing.Point(13, 84);
            this.chkDistortionLevel.Name = "chkDistortionLevel";
            this.chkDistortionLevel.Size = new System.Drawing.Size(99, 17);
            this.chkDistortionLevel.TabIndex = 5;
            this.chkDistortionLevel.Text = "Distortion Level";
            this.chkDistortionLevel.UseVisualStyleBackColor = true;
            this.chkDistortionLevel.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // chkDistortion
            // 
            this.chkDistortion.AutoSize = true;
            this.chkDistortion.Location = new System.Drawing.Point(13, 60);
            this.chkDistortion.Name = "chkDistortion";
            this.chkDistortion.Size = new System.Drawing.Size(70, 17);
            this.chkDistortion.TabIndex = 4;
            this.chkDistortion.Text = "Distortion";
            this.chkDistortion.UseVisualStyleBackColor = true;
            this.chkDistortion.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // chkACLevel
            // 
            this.chkACLevel.AutoSize = true;
            this.chkACLevel.Location = new System.Drawing.Point(13, 13);
            this.chkACLevel.Name = "chkACLevel";
            this.chkACLevel.Size = new System.Drawing.Size(69, 17);
            this.chkACLevel.TabIndex = 1;
            this.chkACLevel.Text = "AC Level";
            this.chkACLevel.UseVisualStyleBackColor = true;
            this.chkACLevel.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // chkDCLevel
            // 
            this.chkDCLevel.AutoSize = true;
            this.chkDCLevel.Location = new System.Drawing.Point(13, 37);
            this.chkDCLevel.Name = "chkDCLevel";
            this.chkDCLevel.Size = new System.Drawing.Size(70, 17);
            this.chkDCLevel.TabIndex = 0;
            this.chkDCLevel.Text = "DC Level";
            this.chkDCLevel.UseVisualStyleBackColor = true;
            this.chkDCLevel.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // btnConnection
            // 
            this.btnConnection.Location = new System.Drawing.Point(458, 262);
            this.btnConnection.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnConnection.Name = "btnConnection";
            this.btnConnection.Size = new System.Drawing.Size(69, 28);
            this.btnConnection.TabIndex = 11;
            this.btnConnection.Text = "Connection";
            this.btnConnection.UseVisualStyleBackColor = true;
            this.btnConnection.Click += new System.EventHandler(this.btnConnection_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(74, 262);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(56, 28);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(13, 262);
            this.btnOk.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(56, 28);
            this.btnOk.TabIndex = 9;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // chkFrequency
            // 
            this.chkFrequency.AutoSize = true;
            this.chkFrequency.Location = new System.Drawing.Point(13, 153);
            this.chkFrequency.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkFrequency.Name = "chkFrequency";
            this.chkFrequency.Size = new System.Drawing.Size(76, 17);
            this.chkFrequency.TabIndex = 17;
            this.chkFrequency.Text = "Frequency";
            this.chkFrequency.UseVisualStyleBackColor = true;
            this.chkFrequency.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // tbxControl
            // 
            this.tbxControl.Controls.Add(this.tabSettings);
            this.tbxControl.Controls.Add(this.tabGenerator);
            this.tbxControl.Controls.Add(this.tabAC);
            this.tbxControl.Controls.Add(this.tabDC);
            this.tbxControl.Controls.Add(this.tabDistortion);
            this.tbxControl.Controls.Add(this.tabDistortionLevel);
            this.tbxControl.Controls.Add(this.tabSNR);
            this.tbxControl.Controls.Add(this.tabSINAD);
            this.tbxControl.Controls.Add(this.tabFrequency);
            this.tbxControl.Location = new System.Drawing.Point(8, 15);
            this.tbxControl.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbxControl.Name = "tbxControl";
            this.tbxControl.SelectedIndex = 0;
            this.tbxControl.Size = new System.Drawing.Size(522, 242);
            this.tbxControl.TabIndex = 12;
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.cbxType);
            this.tabSettings.Controls.Add(this.lblType);
            this.tabSettings.Controls.Add(this.chkFrequency);
            this.tabSettings.Controls.Add(this.chkDCLevel);
            this.tabSettings.Controls.Add(this.chkACLevel);
            this.tabSettings.Controls.Add(this.chkDistortion);
            this.tabSettings.Controls.Add(this.chkDistortionLevel);
            this.tabSettings.Controls.Add(this.chkSINAD);
            this.tabSettings.Controls.Add(this.chkSNR);
            this.tabSettings.Location = new System.Drawing.Point(4, 22);
            this.tabSettings.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabSettings.Size = new System.Drawing.Size(514, 216);
            this.tabSettings.TabIndex = 0;
            this.tabSettings.Text = "Settings";
            this.tabSettings.UseVisualStyleBackColor = true;
            // 
            // cbxType
            // 
            this.cbxType.FormattingEnabled = true;
            this.cbxType.Location = new System.Drawing.Point(193, 11);
            this.cbxType.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxType.Name = "cbxType";
            this.cbxType.Size = new System.Drawing.Size(72, 21);
            this.cbxType.TabIndex = 19;
            this.cbxType.SelectedIndexChanged += new System.EventHandler(this.cbxType_SelectedIndexChanged);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(146, 13);
            this.lblType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 13);
            this.lblType.TabIndex = 18;
            this.lblType.Text = "Type:";
            // 
            // tabGenerator
            // 
            this.tabGenerator.Controls.Add(this.cbxOutputImpedance);
            this.tabGenerator.Controls.Add(this.lblOutputImpedance);
            this.tabGenerator.Controls.Add(this.gbxAmplitude);
            this.tabGenerator.Controls.Add(this.gbxFrequency);
            this.tabGenerator.Location = new System.Drawing.Point(4, 22);
            this.tabGenerator.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabGenerator.Name = "tabGenerator";
            this.tabGenerator.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabGenerator.Size = new System.Drawing.Size(514, 216);
            this.tabGenerator.TabIndex = 1;
            this.tabGenerator.Text = "Generator";
            this.tabGenerator.UseVisualStyleBackColor = true;
            // 
            // cbxOutputImpedance
            // 
            this.cbxOutputImpedance.FormattingEnabled = true;
            this.cbxOutputImpedance.Location = new System.Drawing.Point(323, 20);
            this.cbxOutputImpedance.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxOutputImpedance.Name = "cbxOutputImpedance";
            this.cbxOutputImpedance.Size = new System.Drawing.Size(74, 21);
            this.cbxOutputImpedance.TabIndex = 5;
            this.cbxOutputImpedance.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbxOutputImpedance_Format);
            // 
            // lblOutputImpedance
            // 
            this.lblOutputImpedance.AutoSize = true;
            this.lblOutputImpedance.Location = new System.Drawing.Point(218, 24);
            this.lblOutputImpedance.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOutputImpedance.Name = "lblOutputImpedance";
            this.lblOutputImpedance.Size = new System.Drawing.Size(95, 13);
            this.lblOutputImpedance.TabIndex = 4;
            this.lblOutputImpedance.Text = "Output Impedance";
            // 
            // tabAC
            // 
            this.tabAC.Controls.Add(this.lblAC_PowerLoad);
            this.tabAC.Controls.Add(this.numAC_PowerLoad);
            this.tabAC.Controls.Add(this.chkAC_PowerDisplay);
            this.tabAC.Controls.Add(this.chkAC_HoldNotchTuning);
            this.tabAC.Controls.Add(this.cbxAC_DetectorType);
            this.tabAC.Controls.Add(this.lblAC_DetectorType);
            this.tabAC.Controls.Add(this.cbxAC_PostNotchGain);
            this.tabAC.Controls.Add(this.lblAC_PostNotchGain);
            this.tabAC.Controls.Add(this.cbxAC_LinLog);
            this.tabAC.Controls.Add(this.cbxAC_Range);
            this.tabAC.Controls.Add(this.lblACRange);
            this.tabAC.Controls.Add(this.gbxFilter);
            this.tabAC.Controls.Add(this.chkAC_Ratio);
            this.tabAC.Location = new System.Drawing.Point(4, 22);
            this.tabAC.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabAC.Name = "tabAC";
            this.tabAC.Size = new System.Drawing.Size(514, 216);
            this.tabAC.TabIndex = 2;
            this.tabAC.Text = "AC";
            this.tabAC.UseVisualStyleBackColor = true;
            // 
            // lblAC_PowerLoad
            // 
            this.lblAC_PowerLoad.AutoSize = true;
            this.lblAC_PowerLoad.Location = new System.Drawing.Point(166, 151);
            this.lblAC_PowerLoad.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAC_PowerLoad.Name = "lblAC_PowerLoad";
            this.lblAC_PowerLoad.Size = new System.Drawing.Size(63, 13);
            this.lblAC_PowerLoad.TabIndex = 17;
            this.lblAC_PowerLoad.Text = "Load (ohm):";
            // 
            // numAC_PowerLoad
            // 
            this.numAC_PowerLoad.Location = new System.Drawing.Point(234, 150);
            this.numAC_PowerLoad.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.numAC_PowerLoad.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numAC_PowerLoad.Name = "numAC_PowerLoad";
            this.numAC_PowerLoad.Size = new System.Drawing.Size(90, 20);
            this.numAC_PowerLoad.TabIndex = 16;
            this.numAC_PowerLoad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chkAC_PowerDisplay
            // 
            this.chkAC_PowerDisplay.AutoSize = true;
            this.chkAC_PowerDisplay.Location = new System.Drawing.Point(168, 124);
            this.chkAC_PowerDisplay.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkAC_PowerDisplay.Name = "chkAC_PowerDisplay";
            this.chkAC_PowerDisplay.Size = new System.Drawing.Size(89, 17);
            this.chkAC_PowerDisplay.TabIndex = 15;
            this.chkAC_PowerDisplay.Text = "Power Result";
            this.chkAC_PowerDisplay.UseVisualStyleBackColor = true;
            this.chkAC_PowerDisplay.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // chkAC_HoldNotchTuning
            // 
            this.chkAC_HoldNotchTuning.AutoSize = true;
            this.chkAC_HoldNotchTuning.Location = new System.Drawing.Point(168, 102);
            this.chkAC_HoldNotchTuning.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkAC_HoldNotchTuning.Name = "chkAC_HoldNotchTuning";
            this.chkAC_HoldNotchTuning.Size = new System.Drawing.Size(116, 17);
            this.chkAC_HoldNotchTuning.TabIndex = 14;
            this.chkAC_HoldNotchTuning.Text = "Hold Notch Tuning";
            this.chkAC_HoldNotchTuning.UseVisualStyleBackColor = true;
            // 
            // cbxAC_DetectorType
            // 
            this.cbxAC_DetectorType.FormattingEnabled = true;
            this.cbxAC_DetectorType.Location = new System.Drawing.Point(256, 78);
            this.cbxAC_DetectorType.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxAC_DetectorType.Name = "cbxAC_DetectorType";
            this.cbxAC_DetectorType.Size = new System.Drawing.Size(92, 21);
            this.cbxAC_DetectorType.TabIndex = 13;
            this.cbxAC_DetectorType.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbx_DetectorType_Format);
            // 
            // lblAC_DetectorType
            // 
            this.lblAC_DetectorType.AutoSize = true;
            this.lblAC_DetectorType.Location = new System.Drawing.Point(166, 80);
            this.lblAC_DetectorType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAC_DetectorType.Name = "lblAC_DetectorType";
            this.lblAC_DetectorType.Size = new System.Drawing.Size(51, 13);
            this.lblAC_DetectorType.TabIndex = 12;
            this.lblAC_DetectorType.Text = "Detector:";
            // 
            // cbxAC_PostNotchGain
            // 
            this.cbxAC_PostNotchGain.FormattingEnabled = true;
            this.cbxAC_PostNotchGain.Location = new System.Drawing.Point(256, 54);
            this.cbxAC_PostNotchGain.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxAC_PostNotchGain.Name = "cbxAC_PostNotchGain";
            this.cbxAC_PostNotchGain.Size = new System.Drawing.Size(92, 21);
            this.cbxAC_PostNotchGain.TabIndex = 11;
            this.cbxAC_PostNotchGain.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbx_PostNotchGain_Format);
            // 
            // lblAC_PostNotchGain
            // 
            this.lblAC_PostNotchGain.AutoSize = true;
            this.lblAC_PostNotchGain.Location = new System.Drawing.Point(166, 58);
            this.lblAC_PostNotchGain.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAC_PostNotchGain.Name = "lblAC_PostNotchGain";
            this.lblAC_PostNotchGain.Size = new System.Drawing.Size(88, 13);
            this.lblAC_PostNotchGain.TabIndex = 10;
            this.lblAC_PostNotchGain.Text = "Post Notch Gain:";
            // 
            // cbxAC_LinLog
            // 
            this.cbxAC_LinLog.FormattingEnabled = true;
            this.cbxAC_LinLog.Location = new System.Drawing.Point(168, 10);
            this.cbxAC_LinLog.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxAC_LinLog.Name = "cbxAC_LinLog";
            this.cbxAC_LinLog.Size = new System.Drawing.Size(92, 21);
            this.cbxAC_LinLog.TabIndex = 9;
            // 
            // cbxAC_Range
            // 
            this.cbxAC_Range.FormattingEnabled = true;
            this.cbxAC_Range.Location = new System.Drawing.Point(58, 11);
            this.cbxAC_Range.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxAC_Range.Name = "cbxAC_Range";
            this.cbxAC_Range.Size = new System.Drawing.Size(92, 21);
            this.cbxAC_Range.TabIndex = 1;
            // 
            // lblACRange
            // 
            this.lblACRange.AutoSize = true;
            this.lblACRange.Location = new System.Drawing.Point(13, 15);
            this.lblACRange.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblACRange.Name = "lblACRange";
            this.lblACRange.Size = new System.Drawing.Size(42, 13);
            this.lblACRange.TabIndex = 0;
            this.lblACRange.Text = "Range:";
            // 
            // tabDC
            // 
            this.tabDC.Controls.Add(this.cbxDC_LinLog);
            this.tabDC.Controls.Add(this.chkDC_Ratio);
            this.tabDC.Controls.Add(this.cbxDC_Range);
            this.tabDC.Controls.Add(this.lblDCRange);
            this.tabDC.Location = new System.Drawing.Point(4, 22);
            this.tabDC.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabDC.Name = "tabDC";
            this.tabDC.Size = new System.Drawing.Size(514, 216);
            this.tabDC.TabIndex = 3;
            this.tabDC.Text = "DC";
            this.tabDC.UseVisualStyleBackColor = true;
            // 
            // cbxDC_LinLog
            // 
            this.cbxDC_LinLog.FormattingEnabled = true;
            this.cbxDC_LinLog.Location = new System.Drawing.Point(164, 10);
            this.cbxDC_LinLog.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxDC_LinLog.Name = "cbxDC_LinLog";
            this.cbxDC_LinLog.Size = new System.Drawing.Size(92, 21);
            this.cbxDC_LinLog.TabIndex = 11;
            // 
            // chkDC_Ratio
            // 
            this.chkDC_Ratio.AutoSize = true;
            this.chkDC_Ratio.Location = new System.Drawing.Point(164, 36);
            this.chkDC_Ratio.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkDC_Ratio.Name = "chkDC_Ratio";
            this.chkDC_Ratio.Size = new System.Drawing.Size(51, 17);
            this.chkDC_Ratio.TabIndex = 10;
            this.chkDC_Ratio.Text = "Ratio";
            this.chkDC_Ratio.UseVisualStyleBackColor = true;
            // 
            // cbxDC_Range
            // 
            this.cbxDC_Range.FormattingEnabled = true;
            this.cbxDC_Range.Location = new System.Drawing.Point(58, 10);
            this.cbxDC_Range.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxDC_Range.Name = "cbxDC_Range";
            this.cbxDC_Range.Size = new System.Drawing.Size(92, 21);
            this.cbxDC_Range.TabIndex = 3;
            // 
            // lblDCRange
            // 
            this.lblDCRange.AutoSize = true;
            this.lblDCRange.Location = new System.Drawing.Point(13, 12);
            this.lblDCRange.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDCRange.Name = "lblDCRange";
            this.lblDCRange.Size = new System.Drawing.Size(42, 13);
            this.lblDCRange.TabIndex = 2;
            this.lblDCRange.Text = "Range:";
            // 
            // tabDistortion
            // 
            this.tabDistortion.Controls.Add(this.chkDist_HoldNotchTuning);
            this.tabDistortion.Controls.Add(this.cbxDist_DetectorType);
            this.tabDistortion.Controls.Add(this.lblDist_DetectorType);
            this.tabDistortion.Controls.Add(this.cbxDist_PostNotchGain);
            this.tabDistortion.Controls.Add(this.lblDist_PostNotchGain);
            this.tabDistortion.Controls.Add(this.cbxDist_LinLog);
            this.tabDistortion.Controls.Add(this.gbxDist_Filter);
            this.tabDistortion.Controls.Add(this.chkDist_Ratio);
            this.tabDistortion.Controls.Add(this.cbxDist_Range);
            this.tabDistortion.Controls.Add(this.lblDist_Range);
            this.tabDistortion.Location = new System.Drawing.Point(4, 22);
            this.tabDistortion.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabDistortion.Name = "tabDistortion";
            this.tabDistortion.Size = new System.Drawing.Size(514, 216);
            this.tabDistortion.TabIndex = 4;
            this.tabDistortion.Text = "Distortion";
            this.tabDistortion.UseVisualStyleBackColor = true;
            // 
            // chkDist_HoldNotchTuning
            // 
            this.chkDist_HoldNotchTuning.AutoSize = true;
            this.chkDist_HoldNotchTuning.Location = new System.Drawing.Point(168, 103);
            this.chkDist_HoldNotchTuning.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkDist_HoldNotchTuning.Name = "chkDist_HoldNotchTuning";
            this.chkDist_HoldNotchTuning.Size = new System.Drawing.Size(116, 17);
            this.chkDist_HoldNotchTuning.TabIndex = 22;
            this.chkDist_HoldNotchTuning.Text = "Hold Notch Tuning";
            this.chkDist_HoldNotchTuning.UseVisualStyleBackColor = true;
            // 
            // cbxDist_DetectorType
            // 
            this.cbxDist_DetectorType.FormattingEnabled = true;
            this.cbxDist_DetectorType.Location = new System.Drawing.Point(256, 79);
            this.cbxDist_DetectorType.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxDist_DetectorType.Name = "cbxDist_DetectorType";
            this.cbxDist_DetectorType.Size = new System.Drawing.Size(92, 21);
            this.cbxDist_DetectorType.TabIndex = 21;
            this.cbxDist_DetectorType.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbx_DetectorType_Format);
            // 
            // lblDist_DetectorType
            // 
            this.lblDist_DetectorType.AutoSize = true;
            this.lblDist_DetectorType.Location = new System.Drawing.Point(166, 81);
            this.lblDist_DetectorType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDist_DetectorType.Name = "lblDist_DetectorType";
            this.lblDist_DetectorType.Size = new System.Drawing.Size(51, 13);
            this.lblDist_DetectorType.TabIndex = 20;
            this.lblDist_DetectorType.Text = "Detector:";
            // 
            // cbxDist_PostNotchGain
            // 
            this.cbxDist_PostNotchGain.FormattingEnabled = true;
            this.cbxDist_PostNotchGain.Location = new System.Drawing.Point(256, 54);
            this.cbxDist_PostNotchGain.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxDist_PostNotchGain.Name = "cbxDist_PostNotchGain";
            this.cbxDist_PostNotchGain.Size = new System.Drawing.Size(92, 21);
            this.cbxDist_PostNotchGain.TabIndex = 19;
            this.cbxDist_PostNotchGain.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbx_PostNotchGain_Format);
            // 
            // lblDist_PostNotchGain
            // 
            this.lblDist_PostNotchGain.AutoSize = true;
            this.lblDist_PostNotchGain.Location = new System.Drawing.Point(166, 59);
            this.lblDist_PostNotchGain.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDist_PostNotchGain.Name = "lblDist_PostNotchGain";
            this.lblDist_PostNotchGain.Size = new System.Drawing.Size(88, 13);
            this.lblDist_PostNotchGain.TabIndex = 18;
            this.lblDist_PostNotchGain.Text = "Post Notch Gain:";
            // 
            // cbxDist_LinLog
            // 
            this.cbxDist_LinLog.FormattingEnabled = true;
            this.cbxDist_LinLog.Location = new System.Drawing.Point(168, 11);
            this.cbxDist_LinLog.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxDist_LinLog.Name = "cbxDist_LinLog";
            this.cbxDist_LinLog.Size = new System.Drawing.Size(92, 21);
            this.cbxDist_LinLog.TabIndex = 17;
            // 
            // gbxDist_Filter
            // 
            this.gbxDist_Filter.Controls.Add(this.chkDist_LP80kHz);
            this.gbxDist_Filter.Controls.Add(this.chkDist_LP30kHz);
            this.gbxDist_Filter.Controls.Add(this.chkDist_HPRight);
            this.gbxDist_Filter.Controls.Add(this.chkDist_HPLeft);
            this.gbxDist_Filter.Location = new System.Drawing.Point(15, 37);
            this.gbxDist_Filter.Name = "gbxDist_Filter";
            this.gbxDist_Filter.Size = new System.Drawing.Size(129, 115);
            this.gbxDist_Filter.TabIndex = 15;
            this.gbxDist_Filter.TabStop = false;
            this.gbxDist_Filter.Text = "Filter";
            // 
            // chkDist_LP80kHz
            // 
            this.chkDist_LP80kHz.AutoSize = true;
            this.chkDist_LP80kHz.Location = new System.Drawing.Point(7, 87);
            this.chkDist_LP80kHz.Name = "chkDist_LP80kHz";
            this.chkDist_LP80kHz.Size = new System.Drawing.Size(106, 17);
            this.chkDist_LP80kHz.TabIndex = 3;
            this.chkDist_LP80kHz.Text = "Low Pass 80kHz";
            this.chkDist_LP80kHz.UseVisualStyleBackColor = true;
            // 
            // chkDist_LP30kHz
            // 
            this.chkDist_LP30kHz.AutoSize = true;
            this.chkDist_LP30kHz.Location = new System.Drawing.Point(7, 64);
            this.chkDist_LP30kHz.Name = "chkDist_LP30kHz";
            this.chkDist_LP30kHz.Size = new System.Drawing.Size(106, 17);
            this.chkDist_LP30kHz.TabIndex = 2;
            this.chkDist_LP30kHz.Text = "Low Pass 30kHz";
            this.chkDist_LP30kHz.UseVisualStyleBackColor = true;
            // 
            // chkDist_HPRight
            // 
            this.chkDist_HPRight.AutoSize = true;
            this.chkDist_HPRight.Location = new System.Drawing.Point(7, 41);
            this.chkDist_HPRight.Name = "chkDist_HPRight";
            this.chkDist_HPRight.Size = new System.Drawing.Size(108, 17);
            this.chkDist_HPRight.TabIndex = 1;
            this.chkDist_HPRight.Text = "High Pass (Right)";
            this.chkDist_HPRight.UseVisualStyleBackColor = true;
            // 
            // chkDist_HPLeft
            // 
            this.chkDist_HPLeft.AutoSize = true;
            this.chkDist_HPLeft.Location = new System.Drawing.Point(7, 20);
            this.chkDist_HPLeft.Name = "chkDist_HPLeft";
            this.chkDist_HPLeft.Size = new System.Drawing.Size(101, 17);
            this.chkDist_HPLeft.TabIndex = 0;
            this.chkDist_HPLeft.Text = "High Pass (Left)";
            this.chkDist_HPLeft.UseVisualStyleBackColor = true;
            // 
            // chkDist_Ratio
            // 
            this.chkDist_Ratio.AutoSize = true;
            this.chkDist_Ratio.Location = new System.Drawing.Point(168, 37);
            this.chkDist_Ratio.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkDist_Ratio.Name = "chkDist_Ratio";
            this.chkDist_Ratio.Size = new System.Drawing.Size(51, 17);
            this.chkDist_Ratio.TabIndex = 16;
            this.chkDist_Ratio.Text = "Ratio";
            this.chkDist_Ratio.UseVisualStyleBackColor = true;
            // 
            // cbxDist_Range
            // 
            this.cbxDist_Range.FormattingEnabled = true;
            this.cbxDist_Range.Location = new System.Drawing.Point(58, 11);
            this.cbxDist_Range.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxDist_Range.Name = "cbxDist_Range";
            this.cbxDist_Range.Size = new System.Drawing.Size(92, 21);
            this.cbxDist_Range.TabIndex = 3;
            // 
            // lblDist_Range
            // 
            this.lblDist_Range.AutoSize = true;
            this.lblDist_Range.Location = new System.Drawing.Point(13, 14);
            this.lblDist_Range.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDist_Range.Name = "lblDist_Range";
            this.lblDist_Range.Size = new System.Drawing.Size(42, 13);
            this.lblDist_Range.TabIndex = 2;
            this.lblDist_Range.Text = "Range:";
            // 
            // tabDistortionLevel
            // 
            this.tabDistortionLevel.Controls.Add(this.chkDistLevel_HoldNotchTuning);
            this.tabDistortionLevel.Controls.Add(this.cbxDistLevel_DetectorType);
            this.tabDistortionLevel.Controls.Add(this.lblDistLevel_DetectorType);
            this.tabDistortionLevel.Controls.Add(this.cbxDistLevel_PostNotchGain);
            this.tabDistortionLevel.Controls.Add(this.lblDistLevel_PostNotchGain);
            this.tabDistortionLevel.Controls.Add(this.cbxDistLevel_LinLog);
            this.tabDistortionLevel.Controls.Add(this.gbxDistLevel_Filter);
            this.tabDistortionLevel.Controls.Add(this.chkDistLevel_Ratio);
            this.tabDistortionLevel.Controls.Add(this.cbxDistLevel_Range);
            this.tabDistortionLevel.Controls.Add(this.lblDistLevel_Range);
            this.tabDistortionLevel.Location = new System.Drawing.Point(4, 22);
            this.tabDistortionLevel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabDistortionLevel.Name = "tabDistortionLevel";
            this.tabDistortionLevel.Size = new System.Drawing.Size(514, 216);
            this.tabDistortionLevel.TabIndex = 5;
            this.tabDistortionLevel.Text = "Distortion Level";
            this.tabDistortionLevel.UseVisualStyleBackColor = true;
            // 
            // chkDistLevel_HoldNotchTuning
            // 
            this.chkDistLevel_HoldNotchTuning.AutoSize = true;
            this.chkDistLevel_HoldNotchTuning.Location = new System.Drawing.Point(172, 103);
            this.chkDistLevel_HoldNotchTuning.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkDistLevel_HoldNotchTuning.Name = "chkDistLevel_HoldNotchTuning";
            this.chkDistLevel_HoldNotchTuning.Size = new System.Drawing.Size(116, 17);
            this.chkDistLevel_HoldNotchTuning.TabIndex = 30;
            this.chkDistLevel_HoldNotchTuning.Text = "Hold Notch Tuning";
            this.chkDistLevel_HoldNotchTuning.UseVisualStyleBackColor = true;
            // 
            // cbxDistLevel_DetectorType
            // 
            this.cbxDistLevel_DetectorType.FormattingEnabled = true;
            this.cbxDistLevel_DetectorType.Location = new System.Drawing.Point(261, 79);
            this.cbxDistLevel_DetectorType.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxDistLevel_DetectorType.Name = "cbxDistLevel_DetectorType";
            this.cbxDistLevel_DetectorType.Size = new System.Drawing.Size(92, 21);
            this.cbxDistLevel_DetectorType.TabIndex = 29;
            this.cbxDistLevel_DetectorType.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbx_DetectorType_Format);
            // 
            // lblDistLevel_DetectorType
            // 
            this.lblDistLevel_DetectorType.AutoSize = true;
            this.lblDistLevel_DetectorType.Location = new System.Drawing.Point(170, 81);
            this.lblDistLevel_DetectorType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDistLevel_DetectorType.Name = "lblDistLevel_DetectorType";
            this.lblDistLevel_DetectorType.Size = new System.Drawing.Size(51, 13);
            this.lblDistLevel_DetectorType.TabIndex = 28;
            this.lblDistLevel_DetectorType.Text = "Detector:";
            // 
            // cbxDistLevel_PostNotchGain
            // 
            this.cbxDistLevel_PostNotchGain.FormattingEnabled = true;
            this.cbxDistLevel_PostNotchGain.Location = new System.Drawing.Point(261, 54);
            this.cbxDistLevel_PostNotchGain.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxDistLevel_PostNotchGain.Name = "cbxDistLevel_PostNotchGain";
            this.cbxDistLevel_PostNotchGain.Size = new System.Drawing.Size(92, 21);
            this.cbxDistLevel_PostNotchGain.TabIndex = 27;
            this.cbxDistLevel_PostNotchGain.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbx_PostNotchGain_Format);
            // 
            // lblDistLevel_PostNotchGain
            // 
            this.lblDistLevel_PostNotchGain.AutoSize = true;
            this.lblDistLevel_PostNotchGain.Location = new System.Drawing.Point(170, 59);
            this.lblDistLevel_PostNotchGain.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDistLevel_PostNotchGain.Name = "lblDistLevel_PostNotchGain";
            this.lblDistLevel_PostNotchGain.Size = new System.Drawing.Size(88, 13);
            this.lblDistLevel_PostNotchGain.TabIndex = 26;
            this.lblDistLevel_PostNotchGain.Text = "Post Notch Gain:";
            // 
            // cbxDistLevel_LinLog
            // 
            this.cbxDistLevel_LinLog.FormattingEnabled = true;
            this.cbxDistLevel_LinLog.Location = new System.Drawing.Point(172, 11);
            this.cbxDistLevel_LinLog.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxDistLevel_LinLog.Name = "cbxDistLevel_LinLog";
            this.cbxDistLevel_LinLog.Size = new System.Drawing.Size(92, 21);
            this.cbxDistLevel_LinLog.TabIndex = 25;
            // 
            // gbxDistLevel_Filter
            // 
            this.gbxDistLevel_Filter.Controls.Add(this.chkDistLevel_LP80kHz);
            this.gbxDistLevel_Filter.Controls.Add(this.chkDistLevel_LP30kHz);
            this.gbxDistLevel_Filter.Controls.Add(this.chkDistLevel_HPRight);
            this.gbxDistLevel_Filter.Controls.Add(this.chkDistLevel_HPLeft);
            this.gbxDistLevel_Filter.Location = new System.Drawing.Point(20, 37);
            this.gbxDistLevel_Filter.Name = "gbxDistLevel_Filter";
            this.gbxDistLevel_Filter.Size = new System.Drawing.Size(129, 115);
            this.gbxDistLevel_Filter.TabIndex = 23;
            this.gbxDistLevel_Filter.TabStop = false;
            this.gbxDistLevel_Filter.Text = "Filter";
            // 
            // chkDistLevel_LP80kHz
            // 
            this.chkDistLevel_LP80kHz.AutoSize = true;
            this.chkDistLevel_LP80kHz.Location = new System.Drawing.Point(7, 87);
            this.chkDistLevel_LP80kHz.Name = "chkDistLevel_LP80kHz";
            this.chkDistLevel_LP80kHz.Size = new System.Drawing.Size(106, 17);
            this.chkDistLevel_LP80kHz.TabIndex = 3;
            this.chkDistLevel_LP80kHz.Text = "Low Pass 80kHz";
            this.chkDistLevel_LP80kHz.UseVisualStyleBackColor = true;
            // 
            // chkDistLevel_LP30kHz
            // 
            this.chkDistLevel_LP30kHz.AutoSize = true;
            this.chkDistLevel_LP30kHz.Location = new System.Drawing.Point(7, 64);
            this.chkDistLevel_LP30kHz.Name = "chkDistLevel_LP30kHz";
            this.chkDistLevel_LP30kHz.Size = new System.Drawing.Size(106, 17);
            this.chkDistLevel_LP30kHz.TabIndex = 2;
            this.chkDistLevel_LP30kHz.Text = "Low Pass 30kHz";
            this.chkDistLevel_LP30kHz.UseVisualStyleBackColor = true;
            // 
            // chkDistLevel_HPRight
            // 
            this.chkDistLevel_HPRight.AutoSize = true;
            this.chkDistLevel_HPRight.Location = new System.Drawing.Point(7, 41);
            this.chkDistLevel_HPRight.Name = "chkDistLevel_HPRight";
            this.chkDistLevel_HPRight.Size = new System.Drawing.Size(108, 17);
            this.chkDistLevel_HPRight.TabIndex = 1;
            this.chkDistLevel_HPRight.Text = "High Pass (Right)";
            this.chkDistLevel_HPRight.UseVisualStyleBackColor = true;
            // 
            // chkDistLevel_HPLeft
            // 
            this.chkDistLevel_HPLeft.AutoSize = true;
            this.chkDistLevel_HPLeft.Location = new System.Drawing.Point(7, 20);
            this.chkDistLevel_HPLeft.Name = "chkDistLevel_HPLeft";
            this.chkDistLevel_HPLeft.Size = new System.Drawing.Size(101, 17);
            this.chkDistLevel_HPLeft.TabIndex = 0;
            this.chkDistLevel_HPLeft.Text = "High Pass (Left)";
            this.chkDistLevel_HPLeft.UseVisualStyleBackColor = true;
            // 
            // chkDistLevel_Ratio
            // 
            this.chkDistLevel_Ratio.AutoSize = true;
            this.chkDistLevel_Ratio.Location = new System.Drawing.Point(172, 37);
            this.chkDistLevel_Ratio.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkDistLevel_Ratio.Name = "chkDistLevel_Ratio";
            this.chkDistLevel_Ratio.Size = new System.Drawing.Size(51, 17);
            this.chkDistLevel_Ratio.TabIndex = 24;
            this.chkDistLevel_Ratio.Text = "Ratio";
            this.chkDistLevel_Ratio.UseVisualStyleBackColor = true;
            // 
            // cbxDistLevel_Range
            // 
            this.cbxDistLevel_Range.FormattingEnabled = true;
            this.cbxDistLevel_Range.Location = new System.Drawing.Point(58, 11);
            this.cbxDistLevel_Range.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxDistLevel_Range.Name = "cbxDistLevel_Range";
            this.cbxDistLevel_Range.Size = new System.Drawing.Size(92, 21);
            this.cbxDistLevel_Range.TabIndex = 3;
            // 
            // lblDistLevel_Range
            // 
            this.lblDistLevel_Range.AutoSize = true;
            this.lblDistLevel_Range.Location = new System.Drawing.Point(13, 14);
            this.lblDistLevel_Range.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDistLevel_Range.Name = "lblDistLevel_Range";
            this.lblDistLevel_Range.Size = new System.Drawing.Size(42, 13);
            this.lblDistLevel_Range.TabIndex = 2;
            this.lblDistLevel_Range.Text = "Range:";
            // 
            // tabSNR
            // 
            this.tabSNR.Controls.Add(this.cbxSNR_Delay);
            this.tabSNR.Controls.Add(this.lblSNR_Delay);
            this.tabSNR.Controls.Add(this.chkSNR_HoldNotchTuning);
            this.tabSNR.Controls.Add(this.cbxSNR_DetectorType);
            this.tabSNR.Controls.Add(this.lblSNR_DetectorType);
            this.tabSNR.Controls.Add(this.cbxSNR_PostNotchGain);
            this.tabSNR.Controls.Add(this.lblSNR_PostNotchGain);
            this.tabSNR.Controls.Add(this.cbxSNR_LinLog);
            this.tabSNR.Controls.Add(this.gbxSNR_Filter);
            this.tabSNR.Controls.Add(this.chkSNR_Ratio);
            this.tabSNR.Controls.Add(this.cbxSNR_Range);
            this.tabSNR.Controls.Add(this.lblSNR_Range);
            this.tabSNR.Location = new System.Drawing.Point(4, 22);
            this.tabSNR.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabSNR.Name = "tabSNR";
            this.tabSNR.Size = new System.Drawing.Size(514, 216);
            this.tabSNR.TabIndex = 6;
            this.tabSNR.Text = "SNR";
            this.tabSNR.UseVisualStyleBackColor = true;
            // 
            // chkSNR_HoldNotchTuning
            // 
            this.chkSNR_HoldNotchTuning.AutoSize = true;
            this.chkSNR_HoldNotchTuning.Location = new System.Drawing.Point(173, 109);
            this.chkSNR_HoldNotchTuning.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkSNR_HoldNotchTuning.Name = "chkSNR_HoldNotchTuning";
            this.chkSNR_HoldNotchTuning.Size = new System.Drawing.Size(116, 17);
            this.chkSNR_HoldNotchTuning.TabIndex = 40;
            this.chkSNR_HoldNotchTuning.Text = "Hold Notch Tuning";
            this.chkSNR_HoldNotchTuning.UseVisualStyleBackColor = true;
            // 
            // cbxSNR_DetectorType
            // 
            this.cbxSNR_DetectorType.FormattingEnabled = true;
            this.cbxSNR_DetectorType.Location = new System.Drawing.Point(262, 85);
            this.cbxSNR_DetectorType.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxSNR_DetectorType.Name = "cbxSNR_DetectorType";
            this.cbxSNR_DetectorType.Size = new System.Drawing.Size(92, 21);
            this.cbxSNR_DetectorType.TabIndex = 39;
            // 
            // lblSNR_DetectorType
            // 
            this.lblSNR_DetectorType.AutoSize = true;
            this.lblSNR_DetectorType.Location = new System.Drawing.Point(171, 87);
            this.lblSNR_DetectorType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSNR_DetectorType.Name = "lblSNR_DetectorType";
            this.lblSNR_DetectorType.Size = new System.Drawing.Size(51, 13);
            this.lblSNR_DetectorType.TabIndex = 38;
            this.lblSNR_DetectorType.Text = "Detector:";
            // 
            // cbxSNR_PostNotchGain
            // 
            this.cbxSNR_PostNotchGain.FormattingEnabled = true;
            this.cbxSNR_PostNotchGain.Location = new System.Drawing.Point(262, 60);
            this.cbxSNR_PostNotchGain.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxSNR_PostNotchGain.Name = "cbxSNR_PostNotchGain";
            this.cbxSNR_PostNotchGain.Size = new System.Drawing.Size(92, 21);
            this.cbxSNR_PostNotchGain.TabIndex = 37;
            // 
            // lblSNR_PostNotchGain
            // 
            this.lblSNR_PostNotchGain.AutoSize = true;
            this.lblSNR_PostNotchGain.Location = new System.Drawing.Point(171, 65);
            this.lblSNR_PostNotchGain.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSNR_PostNotchGain.Name = "lblSNR_PostNotchGain";
            this.lblSNR_PostNotchGain.Size = new System.Drawing.Size(88, 13);
            this.lblSNR_PostNotchGain.TabIndex = 36;
            this.lblSNR_PostNotchGain.Text = "Post Notch Gain:";
            // 
            // cbxSNR_LinLog
            // 
            this.cbxSNR_LinLog.FormattingEnabled = true;
            this.cbxSNR_LinLog.Location = new System.Drawing.Point(173, 17);
            this.cbxSNR_LinLog.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxSNR_LinLog.Name = "cbxSNR_LinLog";
            this.cbxSNR_LinLog.Size = new System.Drawing.Size(92, 21);
            this.cbxSNR_LinLog.TabIndex = 35;
            // 
            // gbxSNR_Filter
            // 
            this.gbxSNR_Filter.Controls.Add(this.chkSNR_LP80kHz);
            this.gbxSNR_Filter.Controls.Add(this.chkSNR_LP30kHz);
            this.gbxSNR_Filter.Controls.Add(this.chkSNR_HPRight);
            this.gbxSNR_Filter.Controls.Add(this.chkSNR_HPLeft);
            this.gbxSNR_Filter.Location = new System.Drawing.Point(21, 43);
            this.gbxSNR_Filter.Name = "gbxSNR_Filter";
            this.gbxSNR_Filter.Size = new System.Drawing.Size(129, 115);
            this.gbxSNR_Filter.TabIndex = 33;
            this.gbxSNR_Filter.TabStop = false;
            this.gbxSNR_Filter.Text = "Filter";
            // 
            // chkSNR_LP80kHz
            // 
            this.chkSNR_LP80kHz.AutoSize = true;
            this.chkSNR_LP80kHz.Location = new System.Drawing.Point(7, 87);
            this.chkSNR_LP80kHz.Name = "chkSNR_LP80kHz";
            this.chkSNR_LP80kHz.Size = new System.Drawing.Size(106, 17);
            this.chkSNR_LP80kHz.TabIndex = 3;
            this.chkSNR_LP80kHz.Text = "Low Pass 80kHz";
            this.chkSNR_LP80kHz.UseVisualStyleBackColor = true;
            // 
            // chkSNR_LP30kHz
            // 
            this.chkSNR_LP30kHz.AutoSize = true;
            this.chkSNR_LP30kHz.Location = new System.Drawing.Point(7, 64);
            this.chkSNR_LP30kHz.Name = "chkSNR_LP30kHz";
            this.chkSNR_LP30kHz.Size = new System.Drawing.Size(106, 17);
            this.chkSNR_LP30kHz.TabIndex = 2;
            this.chkSNR_LP30kHz.Text = "Low Pass 30kHz";
            this.chkSNR_LP30kHz.UseVisualStyleBackColor = true;
            // 
            // chkSNR_HPRight
            // 
            this.chkSNR_HPRight.AutoSize = true;
            this.chkSNR_HPRight.Location = new System.Drawing.Point(7, 41);
            this.chkSNR_HPRight.Name = "chkSNR_HPRight";
            this.chkSNR_HPRight.Size = new System.Drawing.Size(108, 17);
            this.chkSNR_HPRight.TabIndex = 1;
            this.chkSNR_HPRight.Text = "High Pass (Right)";
            this.chkSNR_HPRight.UseVisualStyleBackColor = true;
            // 
            // chkSNR_HPLeft
            // 
            this.chkSNR_HPLeft.AutoSize = true;
            this.chkSNR_HPLeft.Location = new System.Drawing.Point(7, 20);
            this.chkSNR_HPLeft.Name = "chkSNR_HPLeft";
            this.chkSNR_HPLeft.Size = new System.Drawing.Size(101, 17);
            this.chkSNR_HPLeft.TabIndex = 0;
            this.chkSNR_HPLeft.Text = "High Pass (Left)";
            this.chkSNR_HPLeft.UseVisualStyleBackColor = true;
            // 
            // chkSNR_Ratio
            // 
            this.chkSNR_Ratio.AutoSize = true;
            this.chkSNR_Ratio.Location = new System.Drawing.Point(173, 43);
            this.chkSNR_Ratio.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkSNR_Ratio.Name = "chkSNR_Ratio";
            this.chkSNR_Ratio.Size = new System.Drawing.Size(51, 17);
            this.chkSNR_Ratio.TabIndex = 34;
            this.chkSNR_Ratio.Text = "Ratio";
            this.chkSNR_Ratio.UseVisualStyleBackColor = true;
            // 
            // cbxSNR_Range
            // 
            this.cbxSNR_Range.FormattingEnabled = true;
            this.cbxSNR_Range.Location = new System.Drawing.Point(58, 17);
            this.cbxSNR_Range.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxSNR_Range.Name = "cbxSNR_Range";
            this.cbxSNR_Range.Size = new System.Drawing.Size(92, 21);
            this.cbxSNR_Range.TabIndex = 3;
            // 
            // lblSNR_Range
            // 
            this.lblSNR_Range.AutoSize = true;
            this.lblSNR_Range.Location = new System.Drawing.Point(13, 20);
            this.lblSNR_Range.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSNR_Range.Name = "lblSNR_Range";
            this.lblSNR_Range.Size = new System.Drawing.Size(42, 13);
            this.lblSNR_Range.TabIndex = 2;
            this.lblSNR_Range.Text = "Range:";
            // 
            // tabSINAD
            // 
            this.tabSINAD.Controls.Add(this.cbxSINAD_SINADRange);
            this.tabSINAD.Controls.Add(this.lblSINAD_SINADRange);
            this.tabSINAD.Controls.Add(this.chkSINAD_HoldNotchTuning);
            this.tabSINAD.Controls.Add(this.cbxSINAD_DetectorType);
            this.tabSINAD.Controls.Add(this.lblSINAD_DetectorType);
            this.tabSINAD.Controls.Add(this.cbxSINAD_PostNotchGain);
            this.tabSINAD.Controls.Add(this.lblSINAD_PostNotchGain);
            this.tabSINAD.Controls.Add(this.cbxSINAD_LinLog);
            this.tabSINAD.Controls.Add(this.gbxSINAD_Filter);
            this.tabSINAD.Controls.Add(this.chkSINAD_Ratio);
            this.tabSINAD.Controls.Add(this.cbxSINAD_Range);
            this.tabSINAD.Controls.Add(this.lblSINAD_Range);
            this.tabSINAD.Location = new System.Drawing.Point(4, 22);
            this.tabSINAD.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabSINAD.Name = "tabSINAD";
            this.tabSINAD.Size = new System.Drawing.Size(514, 216);
            this.tabSINAD.TabIndex = 7;
            this.tabSINAD.Text = "SINAD";
            this.tabSINAD.UseVisualStyleBackColor = true;
            // 
            // cbxSINAD_SINADRange
            // 
            this.cbxSINAD_SINADRange.FormattingEnabled = true;
            this.cbxSINAD_SINADRange.Location = new System.Drawing.Point(262, 125);
            this.cbxSINAD_SINADRange.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxSINAD_SINADRange.Name = "cbxSINAD_SINADRange";
            this.cbxSINAD_SINADRange.Size = new System.Drawing.Size(92, 21);
            this.cbxSINAD_SINADRange.TabIndex = 50;
            // 
            // lblSINAD_SINADRange
            // 
            this.lblSINAD_SINADRange.AutoSize = true;
            this.lblSINAD_SINADRange.Location = new System.Drawing.Point(170, 128);
            this.lblSINAD_SINADRange.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSINAD_SINADRange.Name = "lblSINAD_SINADRange";
            this.lblSINAD_SINADRange.Size = new System.Drawing.Size(78, 13);
            this.lblSINAD_SINADRange.TabIndex = 49;
            this.lblSINAD_SINADRange.Text = "SINAD Range:";
            // 
            // chkSINAD_HoldNotchTuning
            // 
            this.chkSINAD_HoldNotchTuning.AutoSize = true;
            this.chkSINAD_HoldNotchTuning.Location = new System.Drawing.Point(173, 106);
            this.chkSINAD_HoldNotchTuning.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkSINAD_HoldNotchTuning.Name = "chkSINAD_HoldNotchTuning";
            this.chkSINAD_HoldNotchTuning.Size = new System.Drawing.Size(116, 17);
            this.chkSINAD_HoldNotchTuning.TabIndex = 48;
            this.chkSINAD_HoldNotchTuning.Text = "Hold Notch Tuning";
            this.chkSINAD_HoldNotchTuning.UseVisualStyleBackColor = true;
            // 
            // cbxSINAD_DetectorType
            // 
            this.cbxSINAD_DetectorType.FormattingEnabled = true;
            this.cbxSINAD_DetectorType.Location = new System.Drawing.Point(262, 82);
            this.cbxSINAD_DetectorType.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxSINAD_DetectorType.Name = "cbxSINAD_DetectorType";
            this.cbxSINAD_DetectorType.Size = new System.Drawing.Size(92, 21);
            this.cbxSINAD_DetectorType.TabIndex = 47;
            // 
            // lblSINAD_DetectorType
            // 
            this.lblSINAD_DetectorType.AutoSize = true;
            this.lblSINAD_DetectorType.Location = new System.Drawing.Point(171, 84);
            this.lblSINAD_DetectorType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSINAD_DetectorType.Name = "lblSINAD_DetectorType";
            this.lblSINAD_DetectorType.Size = new System.Drawing.Size(51, 13);
            this.lblSINAD_DetectorType.TabIndex = 46;
            this.lblSINAD_DetectorType.Text = "Detector:";
            // 
            // cbxSINAD_PostNotchGain
            // 
            this.cbxSINAD_PostNotchGain.FormattingEnabled = true;
            this.cbxSINAD_PostNotchGain.Location = new System.Drawing.Point(262, 57);
            this.cbxSINAD_PostNotchGain.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxSINAD_PostNotchGain.Name = "cbxSINAD_PostNotchGain";
            this.cbxSINAD_PostNotchGain.Size = new System.Drawing.Size(92, 21);
            this.cbxSINAD_PostNotchGain.TabIndex = 45;
            // 
            // lblSINAD_PostNotchGain
            // 
            this.lblSINAD_PostNotchGain.AutoSize = true;
            this.lblSINAD_PostNotchGain.Location = new System.Drawing.Point(171, 62);
            this.lblSINAD_PostNotchGain.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSINAD_PostNotchGain.Name = "lblSINAD_PostNotchGain";
            this.lblSINAD_PostNotchGain.Size = new System.Drawing.Size(88, 13);
            this.lblSINAD_PostNotchGain.TabIndex = 44;
            this.lblSINAD_PostNotchGain.Text = "Post Notch Gain:";
            // 
            // cbxSINAD_LinLog
            // 
            this.cbxSINAD_LinLog.FormattingEnabled = true;
            this.cbxSINAD_LinLog.Location = new System.Drawing.Point(173, 14);
            this.cbxSINAD_LinLog.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxSINAD_LinLog.Name = "cbxSINAD_LinLog";
            this.cbxSINAD_LinLog.Size = new System.Drawing.Size(92, 21);
            this.cbxSINAD_LinLog.TabIndex = 43;
            // 
            // gbxSINAD_Filter
            // 
            this.gbxSINAD_Filter.Controls.Add(this.chkSINAD_LP80kHz);
            this.gbxSINAD_Filter.Controls.Add(this.chkSINAD_LP30kHz);
            this.gbxSINAD_Filter.Controls.Add(this.chkSINAD_HPRight);
            this.gbxSINAD_Filter.Controls.Add(this.chkSINAD_HPLeft);
            this.gbxSINAD_Filter.Location = new System.Drawing.Point(21, 40);
            this.gbxSINAD_Filter.Name = "gbxSINAD_Filter";
            this.gbxSINAD_Filter.Size = new System.Drawing.Size(129, 115);
            this.gbxSINAD_Filter.TabIndex = 41;
            this.gbxSINAD_Filter.TabStop = false;
            this.gbxSINAD_Filter.Text = "Filter";
            // 
            // chkSINAD_LP80kHz
            // 
            this.chkSINAD_LP80kHz.AutoSize = true;
            this.chkSINAD_LP80kHz.Location = new System.Drawing.Point(7, 87);
            this.chkSINAD_LP80kHz.Name = "chkSINAD_LP80kHz";
            this.chkSINAD_LP80kHz.Size = new System.Drawing.Size(106, 17);
            this.chkSINAD_LP80kHz.TabIndex = 3;
            this.chkSINAD_LP80kHz.Text = "Low Pass 80kHz";
            this.chkSINAD_LP80kHz.UseVisualStyleBackColor = true;
            // 
            // chkSINAD_LP30kHz
            // 
            this.chkSINAD_LP30kHz.AutoSize = true;
            this.chkSINAD_LP30kHz.Location = new System.Drawing.Point(7, 64);
            this.chkSINAD_LP30kHz.Name = "chkSINAD_LP30kHz";
            this.chkSINAD_LP30kHz.Size = new System.Drawing.Size(106, 17);
            this.chkSINAD_LP30kHz.TabIndex = 2;
            this.chkSINAD_LP30kHz.Text = "Low Pass 30kHz";
            this.chkSINAD_LP30kHz.UseVisualStyleBackColor = true;
            // 
            // chkSINAD_HPRight
            // 
            this.chkSINAD_HPRight.AutoSize = true;
            this.chkSINAD_HPRight.Location = new System.Drawing.Point(7, 41);
            this.chkSINAD_HPRight.Name = "chkSINAD_HPRight";
            this.chkSINAD_HPRight.Size = new System.Drawing.Size(108, 17);
            this.chkSINAD_HPRight.TabIndex = 1;
            this.chkSINAD_HPRight.Text = "High Pass (Right)";
            this.chkSINAD_HPRight.UseVisualStyleBackColor = true;
            // 
            // chkSINAD_HPLeft
            // 
            this.chkSINAD_HPLeft.AutoSize = true;
            this.chkSINAD_HPLeft.Location = new System.Drawing.Point(7, 20);
            this.chkSINAD_HPLeft.Name = "chkSINAD_HPLeft";
            this.chkSINAD_HPLeft.Size = new System.Drawing.Size(101, 17);
            this.chkSINAD_HPLeft.TabIndex = 0;
            this.chkSINAD_HPLeft.Text = "High Pass (Left)";
            this.chkSINAD_HPLeft.UseVisualStyleBackColor = true;
            // 
            // chkSINAD_Ratio
            // 
            this.chkSINAD_Ratio.AutoSize = true;
            this.chkSINAD_Ratio.Location = new System.Drawing.Point(173, 40);
            this.chkSINAD_Ratio.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkSINAD_Ratio.Name = "chkSINAD_Ratio";
            this.chkSINAD_Ratio.Size = new System.Drawing.Size(51, 17);
            this.chkSINAD_Ratio.TabIndex = 42;
            this.chkSINAD_Ratio.Text = "Ratio";
            this.chkSINAD_Ratio.UseVisualStyleBackColor = true;
            // 
            // cbxSINAD_Range
            // 
            this.cbxSINAD_Range.FormattingEnabled = true;
            this.cbxSINAD_Range.Location = new System.Drawing.Point(58, 14);
            this.cbxSINAD_Range.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxSINAD_Range.Name = "cbxSINAD_Range";
            this.cbxSINAD_Range.Size = new System.Drawing.Size(92, 21);
            this.cbxSINAD_Range.TabIndex = 3;
            // 
            // lblSINAD_Range
            // 
            this.lblSINAD_Range.AutoSize = true;
            this.lblSINAD_Range.Location = new System.Drawing.Point(13, 16);
            this.lblSINAD_Range.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSINAD_Range.Name = "lblSINAD_Range";
            this.lblSINAD_Range.Size = new System.Drawing.Size(42, 13);
            this.lblSINAD_Range.TabIndex = 2;
            this.lblSINAD_Range.Text = "Range:";
            // 
            // tabFrequency
            // 
            this.tabFrequency.Location = new System.Drawing.Point(4, 22);
            this.tabFrequency.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabFrequency.Name = "tabFrequency";
            this.tabFrequency.Size = new System.Drawing.Size(514, 216);
            this.tabFrequency.TabIndex = 8;
            this.tabFrequency.Text = "Frequency";
            this.tabFrequency.UseVisualStyleBackColor = true;
            // 
            // lblSNR_Delay
            // 
            this.lblSNR_Delay.AutoSize = true;
            this.lblSNR_Delay.Location = new System.Drawing.Point(171, 131);
            this.lblSNR_Delay.Name = "lblSNR_Delay";
            this.lblSNR_Delay.Size = new System.Drawing.Size(37, 13);
            this.lblSNR_Delay.TabIndex = 41;
            this.lblSNR_Delay.Text = "Delay:";
            // 
            // cbxSNR_Delay
            // 
            this.cbxSNR_Delay.FormattingEnabled = true;
            this.cbxSNR_Delay.Location = new System.Drawing.Point(233, 128);
            this.cbxSNR_Delay.Name = "cbxSNR_Delay";
            this.cbxSNR_Delay.Size = new System.Drawing.Size(121, 21);
            this.cbxSNR_Delay.TabIndex = 42;
            // 
            // FormHP8903InstrumentSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 303);
            this.Controls.Add(this.tbxControl);
            this.Controls.Add(this.btnConnection);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Name = "FormHP8903InstrumentSettings";
            this.Text = "HP 8903B Audio Analyzer";
            this.Load += new System.EventHandler(this.FormHP8903InstrumentSettings_Load);
            this.gbxFrequency.ResumeLayout(false);
            this.gbxFrequency.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFrequency)).EndInit();
            this.gbxAmplitude.ResumeLayout(false);
            this.gbxAmplitude.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAmplitudeValue)).EndInit();
            this.gbxFilter.ResumeLayout(false);
            this.gbxFilter.PerformLayout();
            this.tbxControl.ResumeLayout(false);
            this.tabSettings.ResumeLayout(false);
            this.tabSettings.PerformLayout();
            this.tabGenerator.ResumeLayout(false);
            this.tabGenerator.PerformLayout();
            this.tabAC.ResumeLayout(false);
            this.tabAC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAC_PowerLoad)).EndInit();
            this.tabDC.ResumeLayout(false);
            this.tabDC.PerformLayout();
            this.tabDistortion.ResumeLayout(false);
            this.tabDistortion.PerformLayout();
            this.gbxDist_Filter.ResumeLayout(false);
            this.gbxDist_Filter.PerformLayout();
            this.tabDistortionLevel.ResumeLayout(false);
            this.tabDistortionLevel.PerformLayout();
            this.gbxDistLevel_Filter.ResumeLayout(false);
            this.gbxDistLevel_Filter.PerformLayout();
            this.tabSNR.ResumeLayout(false);
            this.tabSNR.PerformLayout();
            this.gbxSNR_Filter.ResumeLayout(false);
            this.gbxSNR_Filter.PerformLayout();
            this.tabSINAD.ResumeLayout(false);
            this.tabSINAD.PerformLayout();
            this.gbxSINAD_Filter.ResumeLayout(false);
            this.gbxSINAD_Filter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gbxFilter;
        private System.Windows.Forms.ComboBox cbxSourceAmplitude;
        private System.Windows.Forms.ComboBox cbxSourceFrequency;
        private System.Windows.Forms.GroupBox gbxFrequency;
        private System.Windows.Forms.ComboBox cbxFrequencyUnit;
        private System.Windows.Forms.NumericUpDown numFrequency;
        private System.Windows.Forms.Label lblFrequencyValue;
        private System.Windows.Forms.Label lblFrequencySource;
        private System.Windows.Forms.GroupBox gbxAmplitude;
        private System.Windows.Forms.ComboBox cbxAmplitudeUnit;
        private System.Windows.Forms.NumericUpDown numAmplitudeValue;
        private System.Windows.Forms.Label lblAmplitudeValue;
        private System.Windows.Forms.Label lblAmplitudeSource;
        private System.Windows.Forms.CheckBox chkAC_LP80kHz;
        private System.Windows.Forms.CheckBox chkAC_LP30kHz;
        private System.Windows.Forms.CheckBox chkAC_HPRight;
        private System.Windows.Forms.CheckBox chkAC_HPLeft;
        private System.Windows.Forms.CheckBox chkSNR;
        private System.Windows.Forms.CheckBox chkSINAD;
        private System.Windows.Forms.CheckBox chkDistortionLevel;
        private System.Windows.Forms.CheckBox chkDistortion;
        private System.Windows.Forms.CheckBox chkACLevel;
        private System.Windows.Forms.CheckBox chkDCLevel;
        private System.Windows.Forms.Button btnConnection;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.CheckBox chkAC_Ratio;
        private System.Windows.Forms.CheckBox chkFrequency;
        private System.Windows.Forms.TabControl tbxControl;
        private System.Windows.Forms.TabPage tabSettings;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.TabPage tabGenerator;
        private System.Windows.Forms.ComboBox cbxOutputImpedance;
        private System.Windows.Forms.Label lblOutputImpedance;
        private System.Windows.Forms.TabPage tabAC;
        private System.Windows.Forms.TabPage tabDC;
        private System.Windows.Forms.TabPage tabDistortion;
        private System.Windows.Forms.TabPage tabDistortionLevel;
        private System.Windows.Forms.TabPage tabSNR;
        private System.Windows.Forms.TabPage tabSINAD;
        private System.Windows.Forms.TabPage tabFrequency;
        private System.Windows.Forms.ComboBox cbxType;
        private System.Windows.Forms.ComboBox cbxAC_Range;
        private System.Windows.Forms.Label lblACRange;
        private System.Windows.Forms.ComboBox cbxDC_Range;
        private System.Windows.Forms.Label lblDCRange;
        private System.Windows.Forms.ComboBox cbxDist_Range;
        private System.Windows.Forms.Label lblDist_Range;
        private System.Windows.Forms.ComboBox cbxDistLevel_Range;
        private System.Windows.Forms.Label lblDistLevel_Range;
        private System.Windows.Forms.ComboBox cbxSNR_Range;
        private System.Windows.Forms.Label lblSNR_Range;
        private System.Windows.Forms.ComboBox cbxSINAD_Range;
        private System.Windows.Forms.Label lblSINAD_Range;
        private System.Windows.Forms.ComboBox cbxAC_LinLog;
        private System.Windows.Forms.ComboBox cbxAC_PostNotchGain;
        private System.Windows.Forms.Label lblAC_PostNotchGain;
        private System.Windows.Forms.ComboBox cbxAC_DetectorType;
        private System.Windows.Forms.Label lblAC_DetectorType;
        private System.Windows.Forms.CheckBox chkAC_HoldNotchTuning;
        private System.Windows.Forms.Label lblAC_PowerLoad;
        private System.Windows.Forms.NumericUpDown numAC_PowerLoad;
        private System.Windows.Forms.CheckBox chkAC_PowerDisplay;
        private System.Windows.Forms.ComboBox cbxDC_LinLog;
        private System.Windows.Forms.CheckBox chkDC_Ratio;
        private System.Windows.Forms.CheckBox chkDist_HoldNotchTuning;
        private System.Windows.Forms.ComboBox cbxDist_DetectorType;
        private System.Windows.Forms.Label lblDist_DetectorType;
        private System.Windows.Forms.ComboBox cbxDist_PostNotchGain;
        private System.Windows.Forms.Label lblDist_PostNotchGain;
        private System.Windows.Forms.ComboBox cbxDist_LinLog;
        private System.Windows.Forms.GroupBox gbxDist_Filter;
        private System.Windows.Forms.CheckBox chkDist_LP80kHz;
        private System.Windows.Forms.CheckBox chkDist_LP30kHz;
        private System.Windows.Forms.CheckBox chkDist_HPRight;
        private System.Windows.Forms.CheckBox chkDist_HPLeft;
        private System.Windows.Forms.CheckBox chkDist_Ratio;
        private System.Windows.Forms.CheckBox chkDistLevel_HoldNotchTuning;
        private System.Windows.Forms.ComboBox cbxDistLevel_DetectorType;
        private System.Windows.Forms.Label lblDistLevel_DetectorType;
        private System.Windows.Forms.ComboBox cbxDistLevel_PostNotchGain;
        private System.Windows.Forms.Label lblDistLevel_PostNotchGain;
        private System.Windows.Forms.ComboBox cbxDistLevel_LinLog;
        private System.Windows.Forms.GroupBox gbxDistLevel_Filter;
        private System.Windows.Forms.CheckBox chkDistLevel_LP80kHz;
        private System.Windows.Forms.CheckBox chkDistLevel_LP30kHz;
        private System.Windows.Forms.CheckBox chkDistLevel_HPRight;
        private System.Windows.Forms.CheckBox chkDistLevel_HPLeft;
        private System.Windows.Forms.CheckBox chkDistLevel_Ratio;
        private System.Windows.Forms.CheckBox chkSNR_HoldNotchTuning;
        private System.Windows.Forms.ComboBox cbxSNR_DetectorType;
        private System.Windows.Forms.Label lblSNR_DetectorType;
        private System.Windows.Forms.ComboBox cbxSNR_PostNotchGain;
        private System.Windows.Forms.Label lblSNR_PostNotchGain;
        private System.Windows.Forms.ComboBox cbxSNR_LinLog;
        private System.Windows.Forms.GroupBox gbxSNR_Filter;
        private System.Windows.Forms.CheckBox chkSNR_LP80kHz;
        private System.Windows.Forms.CheckBox chkSNR_LP30kHz;
        private System.Windows.Forms.CheckBox chkSNR_HPRight;
        private System.Windows.Forms.CheckBox chkSNR_HPLeft;
        private System.Windows.Forms.CheckBox chkSNR_Ratio;
        private System.Windows.Forms.CheckBox chkSINAD_HoldNotchTuning;
        private System.Windows.Forms.ComboBox cbxSINAD_DetectorType;
        private System.Windows.Forms.Label lblSINAD_DetectorType;
        private System.Windows.Forms.ComboBox cbxSINAD_PostNotchGain;
        private System.Windows.Forms.Label lblSINAD_PostNotchGain;
        private System.Windows.Forms.ComboBox cbxSINAD_LinLog;
        private System.Windows.Forms.GroupBox gbxSINAD_Filter;
        private System.Windows.Forms.CheckBox chkSINAD_LP80kHz;
        private System.Windows.Forms.CheckBox chkSINAD_LP30kHz;
        private System.Windows.Forms.CheckBox chkSINAD_HPRight;
        private System.Windows.Forms.CheckBox chkSINAD_HPLeft;
        private System.Windows.Forms.CheckBox chkSINAD_Ratio;
        private System.Windows.Forms.ComboBox cbxSINAD_SINADRange;
        private System.Windows.Forms.Label lblSINAD_SINADRange;
        private System.Windows.Forms.ComboBox cbxSNR_Delay;
        private System.Windows.Forms.Label lblSNR_Delay;
    }
}