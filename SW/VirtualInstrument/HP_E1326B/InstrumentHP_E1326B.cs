﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument.HP_E1326B
{
    [PluginDescriptor("HP E1326B/E1411B VXI Multimeter")]
    [PluginSettingsClass("SUF.Instrument.HP_E1326B.FormHP_E1326BInstrumentSettings")]
    public class InstrumentHP_E1326B : Instrument
    {
        // To be saved to config
        public LineFrequency LF = LineFrequency.Hz_50;
        private MeasurementFunction _func;
        public Range range = Range.VDC_Auto;
        public ResolutionPLC precision = ResolutionPLC.PLC_1;
        public Trigger trigger; // Required?
        public bool AutoZero = true;
        public int ResultID;

        public ResultDescriptor resultDescriptor;

        public InstrumentHP_E1326B()
        {
            base.sourceDescriptors = new List<ResultDescriptor>();
            resultDescriptor = new ResultDescriptor();
            resultDescriptor.SourceGuid = base.InstanceID;
            resultDescriptor.ChannelID = 0;
            base.sourceDescriptors.Add(resultDescriptor);
            this.func = MeasurementFunction.DC_Volts;
        }

        public MeasurementFunction func
        {
            get
            {
                return this._func;
            }
            set
            {
                this._func = value;
                this.resultDescriptor.ChannelName = value.ToString();
                this.resultDescriptor.unit = Function2Unit(value);
            }
        }
        private MeasureUnit Function2Unit(MeasurementFunction func)
        {
            switch (func)
            {
                case MeasurementFunction.AC_Volts:
                    return MeasureUnit.volt;
                case MeasurementFunction.DC_Volts:
                    return MeasureUnit.volt;
                case MeasurementFunction.Ohms_2wire:
                    return MeasureUnit.ohm;
                case MeasurementFunction.Ohms_4wire:
                    return MeasureUnit.ohm;
                case MeasurementFunction.Temp:
                    return MeasureUnit.celsius;
            }
            return MeasureUnit.unknown;
        }

        public override InstrumentRetObject Init()
        {
            return base.Setup(GPIB_Terminator.EOI, new string[] {
                "FORM ASC",
                LF.GetCommand(),
                "SAMP:COUN 1"
            });
        }
    }
}
