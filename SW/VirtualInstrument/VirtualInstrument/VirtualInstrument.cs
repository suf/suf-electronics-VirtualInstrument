﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;
// using SUF.Instrument.InstrumentType;

namespace VirtualInstrument
{
    public partial class frmVirtualInstrument : Form
    {
        public frmVirtualInstrument()
        {
            InitializeComponent();
            PluginLoader.Load();
            this.colControllerType.ValueType = typeof(ControllerPlugin);
            this.colControllerType.DisplayMember = "Name";
            this.colControllerType.ValueMember = "Value";
            this.colControllerType.DataSource = ControllerTypeCollection.GetControllerTypeCollection.ControllerTypes;

            this.colInstrumentType.ValueType = typeof(InstrumentPlugin);
            this.colInstrumentType.DisplayMember = "Name";
            this.colInstrumentType.ValueMember = "Value";
            this.colInstrumentType.DataSource = InstrumentTypeCollection.GetInstrumentTypeCollection.InstrumentTypes;

            this.colFilterType.ValueType = typeof(FilterPlugin);
            this.colFilterType.DisplayMember = "Name";
            this.colFilterType.ValueMember = "Value";
            this.colFilterType.DataSource = FilterTypeCollection.GetFilterTypeCollection.FilterTypes;

            this.colTargetType.ValueType = typeof(TargetPlugin);
            this.colTargetType.DisplayMember = "Name";
            this.colTargetType.ValueMember = "Value";
            this.colTargetType.DataSource = TargetTypeCollection.GetTargetTypeCollection.TargetTypes;

            IFTVisible(false);

            // AddControllerRow();
        }
        // private int _controllerId = 0;
        private void IFTVisible(bool visible)
        {
            this.dgvFilter.Visible = visible;
            this.dgvInstrument.Visible = visible;
            this.dgvTarget.Visible = visible;
            this.lblFilter.Visible = visible;
            this.lblInstrument.Visible = visible;
            this.lblTargets.Visible = visible;
        }
        private void AddInstrumentRow(Instrument instrument)
        {
            int rowindex = dgvInstrument.Rows.Add();
            dgvInstrument.Rows[rowindex].Tag = instrument;
            dgvInstrument.Rows[rowindex].Cells["colInstrumentName"].Value = instrument.Name;
            dgvInstrument.Rows[rowindex].Cells["colInstrumentType"].Value = InstrumentTypeCollection.GetInstrumentTypeCollection.GetInstrumentPlugin(instrument.GetType());
        }
        private void AddFilterRow(Filter filter)
        {
            int rowindex = dgvFilter.Rows.Add();
            dgvFilter.Rows[rowindex].Tag = filter;
            dgvFilter.Rows[rowindex].Cells["colFilterName"].Value = filter.Name;
            dgvFilter.Rows[rowindex].Cells["colFilterType"].Value = FilterTypeCollection.GetFilterTypeCollection.GetFilterPlugin(filter.GetType());
        }
        private void AddTargetRow(ITarget target)
        {
            int rowindex = dgvTarget.Rows.Add();
            dgvTarget.Rows[rowindex].Tag = target;
            dgvTarget.Rows[rowindex].Cells["colTargetName"].Value = target.GetName();
            dgvTarget.Rows[rowindex].Cells["colTargetType"].Value = TargetTypeCollection.GetTargetTypeCollection.GetTargetPlugin(target.GetType());
        }


        private void connectionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*
            FormConnectionList frm = new FormConnectionList();
            frm.ShowDialog();
            frm.Dispose();
            */
        }

        private void dgvController_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;
            ControllerSettingsFormBase settingsForm;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                switch(senderGrid.Columns[e.ColumnIndex].Name)
                {
                    case "colControllerSettings":
                        if(senderGrid.Rows[e.RowIndex].Cells["colControllerType"].Value != null)
                        {
                            settingsForm = ((ControllerPlugin)senderGrid.Rows[e.RowIndex].Cells["colControllerType"].Value).GetSettingsForm();
                            if (settingsForm != null)
                            {
                                settingsForm.controller = (Controller)senderGrid.Rows[e.RowIndex].Tag;
                                settingsForm.ShowDialog();
                                // handle settings
                                if(settingsForm.IsSaved)
                                {
                                    senderGrid.Rows[e.RowIndex].Tag = settingsForm.controller;
                                }
                                settingsForm.Dispose();
                            }
                        }
                        break;
                    case "colAction":
                        if(senderGrid.Rows[e.RowIndex].Tag is Controller)
                        {
                            ControllerUpdateAllowed = false;
                            switch(((Controller)senderGrid.Rows[e.RowIndex].Tag).State)
                            {
                                case ControllerState.Running:
                                    ((Controller)senderGrid.Rows[e.RowIndex].Tag).Stop();
                                    ((DataGridViewButtonCell)senderGrid.Rows[e.RowIndex].Cells["colAction"]).UseColumnTextForButtonValue = false;
                                    senderGrid.Rows[e.RowIndex].Cells["colAction"].Value = "Start";
                                    break;
                                case ControllerState.Stopped:
                                    ((Controller)senderGrid.Rows[e.RowIndex].Tag).Start();
                                    ((DataGridViewButtonCell)senderGrid.Rows[e.RowIndex].Cells["colAction"]).UseColumnTextForButtonValue = false;
                                    senderGrid.Rows[e.RowIndex].Cells["colAction"].Value = "Stop";
                                    break;
                            }
                            ControllerUpdateAllowed = true;
                        }
                        break;
                }
            }
        }

        private void dgvController_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            ControllerUpdateAllowed = false;
            dgvInstrument.Rows.Clear();
            dgvFilter.Rows.Clear();
            dgvTarget.Rows.Clear();
            
            bool EnableControls = false;
            DataGridView senderGrid = (DataGridView)sender;
            if(e.RowIndex >= 0)
            {
                EnableControls = senderGrid.Rows[e.RowIndex].Tag != null;
            }
            IFTVisible(EnableControls);
            // populate other datagridviews with the stored data from the controller
            if(senderGrid.Rows[e.RowIndex].Tag != null)
            {
                foreach(Instrument instrument in ((Controller)senderGrid.Rows[e.RowIndex].Tag).instruments)
                {
                    AddInstrumentRow(instrument);
                }
                foreach (Filter filter in ((Controller)senderGrid.Rows[e.RowIndex].Tag).filters)
                {
                    AddFilterRow(filter);
                }
                foreach (ITarget target in ((Controller)senderGrid.Rows[e.RowIndex].Tag).targets)
                {
                    AddTargetRow(target);
                }
            }
            ControllerUpdateAllowed = true;
        }

        private void dgvController_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;
            Controller origController;
            if(senderGrid.Columns[e.ColumnIndex].Name == "colControllerType")
            {
                // Create or change the controller
                if (senderGrid.Rows[e.RowIndex].Cells["colControllerType"].Value != null)
                {
                    if (senderGrid.Rows[e.RowIndex].Tag == null)
                    {
                        senderGrid.Rows[e.RowIndex].Tag = ((ControllerPlugin)senderGrid.Rows[e.RowIndex].Cells["colControllerType"].Value).GetController();
                    }
                    else
                    {
                        if(senderGrid.Rows[e.RowIndex].Tag.GetType() != ((ControllerPlugin)senderGrid.Rows[e.RowIndex].Cells["colControllerType"].Value).ControllerType)
                        {
                            // If changed, copy data from the original one
                            origController = senderGrid.Rows[e.RowIndex].Tag as Controller;
                            senderGrid.Rows[e.RowIndex].Tag = ((ControllerPlugin)senderGrid.Rows[e.RowIndex].Cells["colControllerType"].Value).GetController();
                            if (origController != null)
                            {
                                ((Controller)senderGrid.Rows[e.RowIndex].Tag).FromController(origController);
                            }
                        }
                    }
                    if(senderGrid.CurrentRow.Index >= 0)
                    {
                        IFTVisible(senderGrid.Rows[senderGrid.CurrentRow.Index].Tag != null);
                    }
                }
            }
        }
        private bool ControllerUpdateAllowed = true;
        /// <summary>
        /// Clear all of the children objects in the controller and fill it from the current datagridviews
        /// </summary>
        private void CurrentControllerFillChildren()
        {
            if (ControllerUpdateAllowed)
            {
                if (dgvController.Rows[dgvController.CurrentRow.Index].Tag != null)
                {
                    ((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).resultDescriptors.Clear();
                    ((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).instruments.Clear();
                    foreach (ResultDescriptor rd in ((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).sourceDescriptors)
                    {
                        rd.SourceName = (string)dgvController.Rows[dgvController.CurrentRow.Index].Cells["colName"].Value;
                        ((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).resultDescriptors.Add(rd);
                    }
                    foreach (DataGridViewRow row in dgvInstrument.Rows)
                    {
                        if (row.Tag != null)
                        {
                            foreach(ResultDescriptor rd in ((Instrument)row.Tag).sourceDescriptors)
                            {
                                rd.SourceName = (string)row.Cells["colInstrumentName"].Value;
                                ((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).resultDescriptors.Add(rd);
                            }
                            ((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).instruments.Add((Instrument)row.Tag);
                        }
                    }
                    ((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).filters.Clear();
                    foreach (DataGridViewRow row in dgvFilter.Rows)
                    {
                        if (row.Tag != null)
                        {
                            foreach (ResultDescriptor rd in ((Filter)row.Tag).sourceDescriptors)
                            {
                                rd.SourceName = (string)row.Cells["colFilterName"].Value;
                                ((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).resultDescriptors.Add(rd);
                            }
                            ((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).filters.Add((Filter)row.Tag);
                        }
                    }
                    ((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).targets.Clear();
                    foreach (DataGridViewRow row in dgvTarget.Rows)
                    {
                        if (row.Tag != null)
                        {
                            ((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).targets.Add((ITarget)row.Tag);
                        }
                    }
                }
            }
        }
        private void dgvInstrument_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;

            switch(senderGrid.Columns[e.ColumnIndex].Name)
            {
                case "colInstrumentType":
                    if (senderGrid.Rows[e.RowIndex].Cells["colInstrumentType"].Value != null)
                    {
                        if (senderGrid.Rows[e.RowIndex].Tag == null)
                        {
                            senderGrid.Rows[e.RowIndex].Tag = ((InstrumentPlugin)senderGrid.Rows[e.RowIndex].Cells["colInstrumentType"].Value).GetInstrument();
                            CurrentControllerFillChildren();
                        }
                        else
                        {
                            if(senderGrid.Rows[e.RowIndex].Tag.GetType() != ((InstrumentPlugin)senderGrid.Rows[e.RowIndex].Cells["colInstrumentType"].Value).InstrumentType)
                            {
                                senderGrid.Rows[e.RowIndex].Tag = ((InstrumentPlugin)senderGrid.Rows[e.RowIndex].Cells["colInstrumentType"].Value).GetInstrument();
                                CurrentControllerFillChildren();
                            }
                        }
                    }
                    if(senderGrid.Rows[e.RowIndex].Tag != null)
                    {
                        ((Instrument)senderGrid.Rows[e.RowIndex].Tag).Name = (string)senderGrid.Rows[e.RowIndex].Cells["colInstrumentName"].Value;
                    }
                    break;
                case "colInstrumentName":
                    if (senderGrid.Rows[e.RowIndex].Tag != null)
                    {
                        ((Instrument)senderGrid.Rows[e.RowIndex].Tag).Name = (string)senderGrid.Rows[e.RowIndex].Cells["colInstrumentName"].Value;
                    }
                    break;
            }
        }

        private void dgvFilter_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;

            switch (senderGrid.Columns[e.ColumnIndex].Name)
            {
                case "colFilterType":
                    if (senderGrid.Rows[e.RowIndex].Cells["colFilterType"].Value != null)
                    {
                        if (senderGrid.Rows[e.RowIndex].Tag == null)
                        {
                            senderGrid.Rows[e.RowIndex].Tag = ((FilterPlugin)senderGrid.Rows[e.RowIndex].Cells["colFilterType"].Value).GetFilter();
                            CurrentControllerFillChildren();
                        }
                        else
                        {
                            if (senderGrid.Rows[e.RowIndex].Tag.GetType() != ((FilterPlugin)senderGrid.Rows[e.RowIndex].Cells["colFilterType"].Value).FilterType)
                            {
                                senderGrid.Rows[e.RowIndex].Tag = ((FilterPlugin)senderGrid.Rows[e.RowIndex].Cells["colFilterType"].Value).GetFilter();
                                CurrentControllerFillChildren();
                            }
                        }
                    }
                    if (senderGrid.Rows[e.RowIndex].Tag != null)
                    {
                        ((Filter)senderGrid.Rows[e.RowIndex].Tag).Name = (string)senderGrid.Rows[e.RowIndex].Cells["colFilterName"].Value;
                    }
                    break;
                case "colFilterName":
                    if (senderGrid.Rows[e.RowIndex].Tag != null)
                    {
                        ((Filter)senderGrid.Rows[e.RowIndex].Tag).Name = (string)senderGrid.Rows[e.RowIndex].Cells["colFilterName"].Value;
                    }
                    break;
            }

        }

        private void dgvTarget_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;

            switch (senderGrid.Columns[e.ColumnIndex].Name)
            {
                case "colTargetType":
                    if (senderGrid.Rows[e.RowIndex].Cells["colTargetType"].Value != null)
                    {
                        if (senderGrid.Rows[e.RowIndex].Tag == null)
                        {
                            senderGrid.Rows[e.RowIndex].Tag = ((TargetPlugin)senderGrid.Rows[e.RowIndex].Cells["colTargetType"].Value).GetTarget();
                            CurrentControllerFillChildren();
                        }
                        else
                        {
                            if (senderGrid.Rows[e.RowIndex].Tag.GetType() != ((TargetPlugin)senderGrid.Rows[e.RowIndex].Cells["colTargetType"].Value).TargetType)
                            {
                                senderGrid.Rows[e.RowIndex].Tag = ((TargetPlugin)senderGrid.Rows[e.RowIndex].Cells["colTargetType"].Value).GetTarget();
                                CurrentControllerFillChildren();
                            }
                        }
                    }
                    if (senderGrid.Rows[e.RowIndex].Tag != null)
                    {
                        ((ITarget)senderGrid.Rows[e.RowIndex].Tag).SetName((string)senderGrid.Rows[e.RowIndex].Cells["colTargetName"].Value);
                    }
                    break;
                case "colTargetName":
                    if (senderGrid.Rows[e.RowIndex].Tag != null)
                    {
                        ((ITarget)senderGrid.Rows[e.RowIndex].Tag).SetName((string)senderGrid.Rows[e.RowIndex].Cells["colTargetName"].Value);
                    }
                    break;
            }

        }

        private void dgvInstrument_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;
            InstrumentSettingsFormBase settingsForm;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                switch (senderGrid.Columns[e.ColumnIndex].Name)
                {
                    case "colInstrumentSettings":
                        if (senderGrid.Rows[e.RowIndex].Cells["colInstrumentType"].Value != null)
                        {
                            settingsForm = ((InstrumentPlugin)senderGrid.Rows[e.RowIndex].Cells["colInstrumentType"].Value).GetSettingsForm();
                            if (settingsForm != null)
                            {
                                settingsForm.instrument = (Instrument)senderGrid.Rows[e.RowIndex].Tag;
                                if (settingsForm is IResultConsumerSettings && settingsForm.instrument != null)
                                {
                                    ((IResultConsumerSettings)settingsForm).SetAvailableResults(((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).GetAvailableDescriptors(settingsForm.instrument.InstanceID));
                                }
                                settingsForm.ShowDialog();
                                // handle settings
                                if (settingsForm.IsSaved)
                                {
                                    senderGrid.Rows[e.RowIndex].Tag = settingsForm.instrument;
                                    // CurrentControllerFillChildren();
                                }
                                settingsForm.Dispose();
                            }
                        }
                        break;
                }
            }
        }

        private void dgvFilter_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;
            FilterSettingsFormBase settingsForm;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                switch (senderGrid.Columns[e.ColumnIndex].Name)
                {
                    case "colFilterSettings":
                        if (senderGrid.Rows[e.RowIndex].Cells["colFilterType"].Value != null)
                        {
                            settingsForm = ((FilterPlugin)senderGrid.Rows[e.RowIndex].Cells["colFilterType"].Value).GetSettingsForm();
                            if (settingsForm != null)
                            {
                                settingsForm.filter = (Filter)senderGrid.Rows[e.RowIndex].Tag;
                                if(settingsForm is IResultConsumerSettings && settingsForm.filter != null)
                                {
                                    ((IResultConsumerSettings)settingsForm).SetAvailableResults(((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).GetAvailableDescriptors(settingsForm.filter.InstanceID));
                                }
                                settingsForm.ShowDialog();
                                // handle settings
                                if (settingsForm.IsSaved)
                                {
                                    senderGrid.Rows[e.RowIndex].Tag = settingsForm.filter;
                                    // CurrentControllerFillChildren();
                                }
                                settingsForm.Dispose();
                            }
                        }
                        break;
                }
            }

        }

        private void dgvTarget_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView senderGrid = (DataGridView)sender;
            TargetSettingsFormBase settingsForm;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                switch (senderGrid.Columns[e.ColumnIndex].Name)
                {
                    case "colTargetSettings":
                        if (senderGrid.Rows[e.RowIndex].Cells["colTargetType"].Value != null)
                        {
                            settingsForm = ((TargetPlugin)senderGrid.Rows[e.RowIndex].Cells["colTargetType"].Value).GetSettingsForm();
                            if (settingsForm != null)
                            {
                                settingsForm.target = (ITarget)senderGrid.Rows[e.RowIndex].Tag;
                                ((IResultConsumerSettings)settingsForm).SetAvailableResults(((Controller)dgvController.Rows[dgvController.CurrentRow.Index].Tag).resultDescriptors);
                                settingsForm.ShowDialog();
                                // handle settings
                                if (settingsForm.IsSaved)
                                {
                                    senderGrid.Rows[e.RowIndex].Tag = settingsForm.target;
                                    // CurrentControllerFillChildren();
                                }
                                settingsForm.Dispose();
                            }
                        }
                        break;
                }
            }

        }
    }
}
