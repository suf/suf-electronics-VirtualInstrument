﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;

using SUF.Instrument;

namespace SUF.Instrument.HP3456A
{
    [PluginDescriptor("HP 3456A Multimeter")]
    [PluginSettingsClass("SUF.Instrument.HP3456A.FormHP3456AInstrumentSettings")]
    public class InstrumentHP3456A : Instrument
    {
        public ResultDescriptor resultDescriptor;
        public InstrumentHP3456A()
        {
            base.sourceDescriptors = new List<ResultDescriptor>();
            resultDescriptor = new ResultDescriptor();
            resultDescriptor.SourceGuid = base.InstanceID;
            resultDescriptor.ChannelID = 0;
            base.sourceDescriptors.Add(resultDescriptor);
            this.func = MeasurementFunction.DC_Volts;
        }
        private MeasurementFunction _func;
        public MeasurementFunction func
        {
            get
            {
                return this._func;
            }
            set
            {
                this._func = value;
                this.resultDescriptor.ChannelName = value.ToString();
                this.resultDescriptor.unit = Function2Unit(value);
            }
        }
        private MeasureUnit Function2Unit(MeasurementFunction func)
        {
            switch(func)
            {
                case MeasurementFunction.AC_Volts:
                    return MeasureUnit.volt;
                case MeasurementFunction.ADCV_Ratio:
                    return MeasureUnit.percent;
                case MeasurementFunction.ADC_Volts:
                    return MeasureUnit.volt;
                case MeasurementFunction.ADDCV_Ratio:
                    return MeasureUnit.percent;
                case MeasurementFunction.Ohms_2wire:
                    return MeasureUnit.ohm;
                case MeasurementFunction.Ohms_4wire:
                    return MeasureUnit.ohm;
                case MeasurementFunction.Ohms_2wire_OC:
                    return MeasureUnit.ohm;
                case MeasurementFunction.Ohms_4wire_OC:
                    return MeasureUnit.ohm;
                case MeasurementFunction.DC_Volts:
                    return MeasureUnit.volt;
                case MeasurementFunction.DDCV_Ratio:
                    return MeasureUnit.percent;
            }
            return MeasureUnit.unknown;
        }
        public Range range = Range.V_Auto;
        public Resolution precision = Resolution.Digit_5;
        public Trigger trigger; // Required?
        public bool FilterEnable = false;
        public bool AutoZero = true;

        public int ResultID;
        public override InstrumentRetObject Measure(MeasurementObject results)
        {
            string result;
            MeasurementValue value;
            InstrumentRetObject retObj = conn.Read(Trigger.Single.GetCommand(), out result);
            if(retObj.RetCode == InstrumentRetCode.OK)
            {
                value = new MeasurementValue(Convert.ToDouble(result, CultureInfo.InvariantCulture),MetricPrefix.none);
                results.Add(this.resultDescriptor, value);
            }
            return retObj;
        }
        public override InstrumentRetObject Init()
        {
            return Setup(GPIB_Terminator.CRLF, new string[] { "P0O1" + func.GetCommand() + range.GetCommand() + precision.GetCommand() + (AutoZero ? "Z1" : "Z0") + (FilterEnable ? "FL1" : "FL0") + Trigger.Hold.GetCommand() });
        }
    }
}
