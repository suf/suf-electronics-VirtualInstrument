﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.ComponentModel;
using SUF.Instrument;

namespace SUF.Instrument.HP3456A
{
    public static class ExtensionMethods
    {
        public static string GetRangePrefix<T>(this T e) where T : IConvertible
        {
            if (e is Enum)
            {
                Type type = e.GetType();
                Array values = System.Enum.GetValues(type);

                foreach (int val in values)
                {
                    if (val == e.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var memInfo = type.GetMember(type.GetEnumName(val));
                        var prefixAttribute = memInfo[0]
                            .GetCustomAttributes(typeof(RangePrefixAttribute), false)
                            .FirstOrDefault() as RangePrefixAttribute;

                        if (prefixAttribute != null)
                        {
                            return prefixAttribute.Prefix;
                        }
                    }
                }
            }
            return null; // could also return string.Empty
        }

    }
    public class RangePrefixAttribute : Attribute
    {
        public RangePrefixAttribute(string prefix)
        {
            this.Prefix = prefix;
        }
        public string Prefix;
    }
    public enum MeasurementFunction
    {
        [Command("S0F1"), Description("DC Volts"), RangePrefix("V")]
        DC_Volts,
        [Command("S0F2"), Description("AC Volts"), RangePrefix("V")]
        AC_Volts,
        [Command("S0F3"), Description("AC + DC Volts"), RangePrefix("V")]
        ADC_Volts,
        [Command("S0F4"), Description("2-Wire Ohms"), RangePrefix("R")]
        Ohms_2wire,
        [Command("S0F5"), Description("4-Wire Ohms"), RangePrefix("R")]
        Ohms_4wire,
        [Command("S1F1"), Description("DCV/DCV Ratio"), RangePrefix("V")]
        DDCV_Ratio,
        [Command("S1F2"), Description("ACV/DCV Ratio"), RangePrefix("V")]
        ADCV_Ratio,
        [Command("S1F3"), Description("ACV + DCV/DCV Ratio"), RangePrefix("V")]
        ADDCV_Ratio,
        [Command("S1F4"), Description("2-Wire Ohms (Offset Compensated)"), RangePrefix("R")]
        Ohms_2wire_OC,
        [Command("S1F5"), Description("4-Wire Ohms (Offset Compensated)"), RangePrefix("R")]
        Ohms_4wire_OC
    }
    public enum Range
    {
        [Command("R1"), Description("Auto")]
        V_Auto,
        [Command("R2"), Description("100mV")]
        V_100mV,
        [Command("R3"), Description("1V")]
        V_1V,
        [Command("R4"), Description("10V")]
        V_10V,
        [Command("R5"), Description("100V")]
        V_100V,
        [Command("R6"), Description("1000V")]
        V_1000V,
        [Command("R1"), Description("Auto")]
        R_Auto,
        [Command("R2"), Description("100ohm")]
        R_100ohm,
        [Command("R3"), Description("1Kohm")]
        R_1Kohm,
        [Command("R4"), Description("10Kohm")]
        R_10Kohm,
        [Command("R5"), Description("100Kohm")]
        R_100Kohm,
        [Command("R6"), Description("1Mohm")]
        R_1Mohm,
        [Command("R7"), Description("10Mohm")]
        R_10Mohm,
        [Command("R8"), Description("100Mohm")]
        R_100Mohm,
        [Command("R9"), Description("1Gohm")]
        R_1Gohm
    }

    public enum Resolution
    {
        [Command("W3GST"), Description("3")]
        Digit_3,
        [Command("W4GST"), Description("4")]
        Digit_4,
        [Command("W5GST"), Description("5")]
        Digit_5,
        [Command("W6GST"), Description("6")]
        Digit_6,
    }

    public enum Trigger
    {
        [Command("T1")]
        Internal,
        [Command("T2")]
        External,
        [Command("T3")]
        Single,
        [Command("T4")]
        Hold
    }
}
