﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;

namespace SUF.Instrument.HP3456A
{
    public partial class FormHP3456AInstrumentSettings : InstrumentSettingsFormBase
    {
        public FormHP3456AInstrumentSettings()
        {
            InitializeComponent();
        }
        // private Dictionary<MeasurementFunction, string> _funcPrefixLookup;

        private void cbxFunction_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateRanges();
        }
        private void UpdateRanges()
        {
            int ItemID;
            this.cbxRange.Items.Clear();
            this.cbxRange.SelectedIndex = -1;
            this.cbxRange.Text = ""; // change to a new auto???
            foreach (Range range in Enum.GetValues(typeof(Range)))
            {
                if (range.ToString().StartsWith(((MeasurementFunction)cbxFunction.SelectedItem).GetRangePrefix() + "_"))
                {
                    ItemID = this.cbxRange.Items.Add(range);
                    if ((Range)this.cbxRange.Items[ItemID] == ((InstrumentHP3456A)this.instrument).range)
                    {
                        this.cbxRange.SelectedIndex = ItemID;
                    }
                }
            }
            if(this.cbxRange.SelectedIndex == -1)
            {
                foreach(Range range in this.cbxRange.Items)
                {
                    if(range.ToString().EndsWith("_Auto"))
                    {
                        this.cbxRange.SelectedItem = range;
                    }
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            // fill the instrument
            ((InstrumentHP3456A)this.instrument).func = (MeasurementFunction)this.cbxFunction.SelectedItem;
            ((InstrumentHP3456A)this.instrument).range = (Range)this.cbxRange.SelectedItem;
            ((InstrumentHP3456A)this.instrument).precision = (Resolution)this.cbxPrecision.SelectedItem;
            ((InstrumentHP3456A)this.instrument).AutoZero = this.chkAutoZero.Checked;
            ((InstrumentHP3456A)this.instrument).FilterEnable = this.chkFilter.Checked;
            base.IsSaved = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormHP3456AInstrumentSettings_Load(object sender, EventArgs e)
        {
            if (base.instrument == null)
            {
                base.instrument = new InstrumentHP3456A();
            }
            UpdateEnumComboBox<MeasurementFunction>(this.cbxFunction, ((InstrumentHP3456A)this.instrument).func);
            UpdateRanges();
            UpdateEnumComboBox<Resolution>(this.cbxPrecision, ((InstrumentHP3456A)this.instrument).precision);
            this.chkAutoZero.Checked = ((InstrumentHP3456A)this.instrument).AutoZero;
            this.chkFilter.Checked = ((InstrumentHP3456A)this.instrument).FilterEnable;
        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            FormConnectionList.SetupConnection(ref this.instrument.conn);
        }

        private void CbxFunction_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((MeasurementFunction)e.ListItem).GetDescription();
        }

        private void CbxRange_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((Range)e.ListItem).GetDescription();
        }

        private void CbxResolution_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((Resolution)e.ListItem).GetDescription();
        }

    }
}
