﻿// ==============================================================================
// 
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// 
// © 2003 LaMarvin. All Rights Reserved.
// Distributed under CPOL v1.02 License
// Modified by SUF (Converted to C#)
// 
// FMI: http://www.vbinfozine.com/a_default.shtml
// ==============================================================================

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace SUF.Winforms.Controls
{
    [DefaultProperty("Color")]
    [DefaultEvent("ColorChanged")]
    public class ColorPicker : Control
    {

        // The CheckBox which is used to render the required button-like appearance
        // of the control.
        private CheckBox __CheckBox;

        private CheckBox _CheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CheckBox;
            }
            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CheckBox != null)
                {
                    // If the associated CheckBox is checked, the drop-down UI is displayed.
                    // Otherwise it is closed.
                    __CheckBox.CheckStateChanged -= OnCheckStateChanged;
                }
                __CheckBox = value;
                if (__CheckBox != null)
                {
                    __CheckBox.CheckStateChanged += OnCheckStateChanged;
                }
            }
        }

        // Should the control display the color's name?
        private bool _TextDisplayed = true;

        // The IWindowsFormsEditorService implementation - the meat of this code;-)
        private EditorService _EditorService;

        // The event is raised when the Color property changes.
        public event EventHandler ColorChanged;

        private const string DefaultColorName = "Black";

        public ColorPicker(Color c) : base()
        {
            // Init the CheckBox to have the correct button-like appearance.
            _CheckBox = new CheckBox();
            _CheckBox.Appearance = Appearance.Button;
            _CheckBox.Dock = DockStyle.Fill;
            _CheckBox.TextAlign = ContentAlignment.MiddleCenter;
            SetColor(c);
            Controls.Add(_CheckBox);
            _EditorService = new EditorService(this);
        }

        public ColorPicker() : this(Color.FromName(DefaultColorName))
        {
        }

        [Description("The currently selected color.")]
        [Category("Appearance")]
        [DefaultValue(typeof(Color), DefaultColorName)]
        public Color Color
        {
            get
            {
                return _CheckBox.BackColor;
            }
            set
            {
                SetColor(value);
                ColorChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        [Description("True meanse the control displays the currently selected color's name, False otherwise.")]
        [Category("Appearance")]
        [DefaultValue(true)]
        public bool TextDisplayed
        {
            get
            {
                return _TextDisplayed;
            }
            set
            {
                _TextDisplayed = value;
                SetColor(Color);
            }
        }

        // Sets the associated CheckBox color and Text according to the TextDisplayed property value.
        private void SetColor(Color c)
        {
            _CheckBox.BackColor = c;
            _CheckBox.ForeColor = GetInvertedColor(c);
            if (_TextDisplayed)
            {
                _CheckBox.Text = c.Name;
            }
            else
            {
                _CheckBox.Text = string.Empty;
            }
        }

        // Primitive color inversion.
        private Color GetInvertedColor(Color c)
        {
            if (c.R + c.G + c.B > 255 * 3 / 2)
            {
                return Color.Black;
            }
            else
            {
                return Color.White;
            }
        }

        private void OnCheckStateChanged(object sender, EventArgs e)
        {
            if (_CheckBox.CheckState == CheckState.Checked)
            {
                ShowDropDown();
            }
            else
            {
                CloseDropDown();
            }
        }

        private void ShowDropDown()
        {
            try
            {
                // This is the Color type editor - it displays the drop-down UI calling
                // our IWindowsFormsEditorService implementation.
                var Editor = new System.Drawing.Design.ColorEditor();

                // Display the UI.
                var C = Color;
                var NewValue = Editor.EditValue(_EditorService, C);

                // If the user didn't cancel the selection, remember the new color.
                if (NewValue is object && !_EditorService.Canceled)
                {
                    Color = (Color)NewValue;
                }

                // Finally, "pop-up" the associated CheckBox.
                _CheckBox.CheckState = CheckState.Unchecked;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
            }
        }

        private void CloseDropDown()
        {
            _EditorService.CloseDropDown();
        }


        // This is a simple Form descendant that hosts the drop-down control provided
        // by the ColorEditor class (in the call to DropDownControl).
        private class DropDownForm : Form
        {
            private bool _Canceled; // did the user cancel the color selection?
            private bool _CloseDropDownCalled; // was the form closed by calling the CloseDropDown method?

            public DropDownForm() : base()
            {
                FormBorderStyle = FormBorderStyle.None;
                ShowInTaskbar = false;
                KeyPreview = true;  // to catch the ESC key
                StartPosition = FormStartPosition.Manual;

                // The ColorUI control is hosted by a Panel, which provides the simple border frame
                // not available for Forms.
                var P = new Panel();
                P.BorderStyle = BorderStyle.FixedSingle;
                P.Dock = DockStyle.Fill;
                Controls.Add(P);
            }

            public void SetControl(Control ctl)
            {
                ((Panel)Controls[0]).Controls.Add(ctl);
            }

            public bool Canceled
            {
                get
                {
                    return _Canceled;
                }
            }

            public void CloseDropDown()
            {
                _CloseDropDownCalled = true;
                Hide();
            }

            protected override void OnKeyDown(KeyEventArgs e)
            {
                base.OnKeyDown(e);
                if (e.Modifiers == 0 && e.KeyCode == Keys.Escape)
                {
                    Hide();
                }
            }

            protected override void OnDeactivate(EventArgs e)
            {
                // We set the Owner to Nothing BEFORE calling the base class.
                // If we wouldn't do it, the Picker form would lose focus after 
                // the dropdown is closed.
                Owner = null;
                base.OnDeactivate(e);

                // If the form was closed by any other means as the CloseDropDown call, it is because
                // the user clicked outside the form, or pressed the ESC key.
                if (!_CloseDropDownCalled)
                {
                    _Canceled = true;
                }

                Hide();
            }
        }


        // This class actually hosts the ColorEditor.ColorUI by implementing the 
        // IWindowsFormsEditorService.
        private class EditorService : IWindowsFormsEditorService, IServiceProvider
        {

            // The associated color picker control.
            private ColorPicker _Picker;

            // Reference to the drop down, which hosts the ColorUI control.
            private DropDownForm _DropDownHolder;

            // Cached _DropDownHolder.Canceled flag in order to allow it to be inspected
            // by the owning ColorPicker control.
            private bool _Canceled;

            public EditorService(ColorPicker owner)
            {
                _Picker = owner;
            }

            public bool Canceled
            {
                get
                {
                    return _Canceled;
                }
            }

            public void CloseDropDown()
            {
                if (_DropDownHolder is object)
                {
                    _DropDownHolder.CloseDropDown();
                }
            }

            public void DropDownControl(Control control)
            {
                _Canceled = false;

                // Initialize the hosting form for the control.
                _DropDownHolder = new DropDownForm();
                _DropDownHolder.Bounds = control.Bounds;
                _DropDownHolder.SetControl(control);

                // Lookup a parent form for the Picker control and make the dropdown form to be owned
                // by it. This prevents to show the dropdown form icon when the user presses the At+Tab system 
                // key while the dropdown form is displayed.
                var PickerForm = GetParentForm(_Picker);
                if (PickerForm is object && PickerForm is Form)
                {
                    _DropDownHolder.Owner = (Form)PickerForm;
                }

                // Ensure the whole drop-down UI is displayed on the screen and show it.
                PositionDropDownHolder();
                _DropDownHolder.Show(); // ShowDialog would disable clicking outside the dropdown area!

                // Wait for the user to select a new color (in which case the ColorUI calls our CloseDropDown
                // method) or cancel the selection (no CloseDropDown called, the Cancel flag is set to True).
                DoModalLoop();

                // Remember the cancel flag and get rid of the drop down form.
                _Canceled = _DropDownHolder.Canceled;
                _DropDownHolder.Dispose();
                _DropDownHolder = null;
            }

            public DialogResult ShowDialog(Form dialog)
            {
                throw new NotSupportedException();
            }

            public object GetService(Type serviceType)
            {
                if (serviceType.Equals(typeof(IWindowsFormsEditorService)))
                {
                    return this;
                }

                return default;
            }

            private void DoModalLoop()
            {
                Debug.Assert(_DropDownHolder is object);
                while (_DropDownHolder.Visible)
                {
                    Application.DoEvents();
                    // The sollowing is the undocumented User32 call. For more information
                    // see the accompanying article at http://www.vbinfozine.com/a_colorpicker.shtml
                    MsgWaitForMultipleObjects(1, IntPtr.Zero, 1, 5, 255);
                }
            }


            // Don't forget (as I did:-) to declare the DllImport methods as Shared!
            // Otherwise you'll get an exception *at runtime*!
            [System.Runtime.InteropServices.DllImport("User32", SetLastError = true)]
            private static extern int MsgWaitForMultipleObjects(int nCount, IntPtr pHandles, short bWaitAll, int dwMilliseconds, int dwWakeMask);

            private void PositionDropDownHolder()
            {
                // Convert _Picker location to screen coordinates.
                var Loc = _Picker.Parent.PointToScreen(_Picker.Location);
                var ScreenRect = Screen.PrimaryScreen.WorkingArea;

                // Position the dropdown X coordinate in order to be displayed in its entirety.
                if (Loc.X < ScreenRect.X)
                {
                    Loc.X = ScreenRect.X;
                }
                else if (Loc.X + _DropDownHolder.Width > ScreenRect.Right)
                {
                    Loc.X = ScreenRect.Right - _DropDownHolder.Width;
                }

                // Do the same for the Y coordinate.
                if (Loc.Y + _Picker.Height + _DropDownHolder.Height > ScreenRect.Bottom)
                {
                    Loc.Offset(0, -_DropDownHolder.Height);  // dropdown will be above the picker control
                }
                else
                {
                    Loc.Offset(0, _Picker.Height);
                } // dropdown will be below the picker

                _DropDownHolder.Location = Loc;
            }

            private Control GetParentForm(Control ctl)
            {
                do
                {
                    if (ctl.Parent is null)
                    {
                        return ctl;
                    }
                    else
                    {
                        ctl = ctl.Parent;
                    }
                }
                while (true);
            }
        }

        // No need to display ForeColor and BackColor and Text in the property browser:

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override Color ForeColor
        {
            get
            {
                return base.ForeColor;
            }

            set
            {
                base.ForeColor = value;
            }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }

            set
            {
                base.BackColor = value;
            }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string Text
        {
            get
            {
                return base.Text;
            }

            set
            {
                base.Text = value;
            }
        }
    }
}