﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO.Ports;
using SUF.Instrument;

namespace SUF.Instrument.Serial
{
    public partial class FormSerialConnectionSettings : ConnectionSettingsFormBase
    {
        // public bool IsSaved = false;
        // public SerialPort Com;
        // public SerialConnection conn;
        public FormSerialConnectionSettings()
        {
            InitializeComponent();
        }
        private void BtnOk_Click(object sender, EventArgs e)
        {
            if(!((SerialConnection)this.connection).ComPort.IsOpen)
            {
                if (cbxPort.SelectedItem != null)
                {
                    if(((SerialConnection)this.connection).ComPort.PortName != ((SerialInfo)cbxPort.SelectedItem).Name)
                    {
                        // changing the port name, must be removed from the global list to release
                        SerialCollection.GetSerialCollection.Remove(((SerialConnection)this.connection).ComPort.PortName);
                        ((SerialConnection)this.connection).ComPort.PortName = ((SerialInfo)cbxPort.SelectedItem).Name;
                    }
                    if (!SerialCollection.GetSerialCollection.Contains(((SerialConnection)this.connection).ComPort.PortName))
                    {
                        SerialCollection.GetSerialCollection.Add(((SerialConnection)this.connection).ComPort);
                    }
                    ((SerialConnection)this.connection).ComPort.BaudRate = ((int)cbxBaud.SelectedItem);
                    ((SerialConnection)this.connection).ComPort.DataBits = ((int)cbxBits.SelectedItem);
                    ((SerialConnection)this.connection).ComPort.Handshake = ((Handshake)cbxHandshake.SelectedItem);
                    ((SerialConnection)this.connection).ComPort.Parity = ((Parity)cbxParity.SelectedItem);
                    ((SerialConnection)this.connection).ComPort.StopBits = ((StopBits)cbxStopBits.SelectedItem);
                    ((SerialConnection)this.connection).ComPort.RtsEnable = chkRtsDtr.Checked;
                    ((SerialConnection)this.connection).ComPort.DtrEnable = chkRtsDtr.Checked;
                    ((SerialConnection)this.connection).Name = this.tbxName.Text;
                    IsSaved = true;
                }
            }
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormSerialConnectionSettings_Load(object sender, EventArgs e)
        {
            int ItemID;
            if (this.connection == null)
            {
                this.connection = new SerialConnection();
            }
            if(((SerialConnection)this.connection).ComPort == null)
            {
                ((SerialConnection)this.connection).ComPort = new SerialPortEx();
            }
            else
            {
                // populate only, if the port was pre-existing
                tbxName.Text = ((SerialConnection)base.connection).Name;
            }
            // populate the dialog

            // Port
            cbxPort.Items.Clear();
            foreach (SerialInfo comPort in SerialInfo.GetCOMPortsInfo())
            {
                // if it is our own port or not on the list
                if (!SerialCollection.GetSerialCollection.Contains(comPort.Name) || ((SerialConnection)this.connection).ComPort.PortName == comPort.Name)
                {
                    ItemID = cbxPort.Items.Add(comPort);
                    if (((SerialConnection)this.connection).ComPort.PortName == comPort.Name)
                    {
                        cbxPort.SelectedIndex = ItemID;
                    }
                }
            }
            cbxPort.Enabled = !((SerialConnection)this.connection).ComPort.IsOpen;

            // Baud
            int[] BaudRates = { 115200, 57600, 38400, 19200, 9600, 4800, 2400, 1200, 600, 300 };
            foreach (int item in BaudRates)
            {
                ItemID = cbxBaud.Items.Add(item);
                if (item == ((SerialConnection)this.connection).ComPort.BaudRate)
                {
                    cbxBaud.SelectedIndex = ItemID;
                }
            }
            cbxBaud.Enabled = !((SerialConnection)this.connection).ComPort.IsOpen;
            // Data Bits
            int[] Bits = { 8, 7 };
            foreach (int item in Bits)
            {
                ItemID = cbxBits.Items.Add(item);
                if (item == ((SerialConnection)this.connection).ComPort.DataBits)
                {
                    cbxBits.SelectedIndex = ItemID;
                }
            }
            cbxBits.Enabled = !((SerialConnection)this.connection).ComPort.IsOpen;
            // Handshake
            foreach (Handshake item in Enum.GetValues(typeof(Handshake)))
            {
                ItemID = cbxHandshake.Items.Add(item);
                if (item == ((SerialConnection)this.connection).ComPort.Handshake)
                {
                    cbxHandshake.SelectedIndex = ItemID;
                }
            }
            cbxHandshake.Enabled = !((SerialConnection)this.connection).ComPort.IsOpen;
            // Parity
            foreach (Parity item in Enum.GetValues(typeof(Parity)))
            {
                ItemID = cbxParity.Items.Add(item);
                if (item == ((SerialConnection)this.connection).ComPort.Parity)
                {
                    cbxParity.SelectedIndex = ItemID;
                }
            }
            cbxParity.Enabled = !((SerialConnection)this.connection).ComPort.IsOpen;
            // Stop bits
            foreach (StopBits item in Enum.GetValues(typeof(StopBits)))
            {
                ItemID = cbxStopBits.Items.Add(item);
                if (item == ((SerialConnection)this.connection).ComPort.StopBits)
                {
                    cbxStopBits.SelectedIndex = ItemID;
                }
            }
            cbxStopBits.Enabled = !((SerialConnection)this.connection).ComPort.IsOpen;
            // RTS/DTR
            chkRtsDtr.Checked = ((SerialConnection)this.connection).ComPort.RtsEnable;
            chkRtsDtr.Enabled = !((SerialConnection)this.connection).ComPort.IsOpen;
            /*
            if (base.connection is SerialConnection)
            {
                tbxName.Text = ((SerialConnection)base.connection).Name;
            }
            */
        }
    }
}
