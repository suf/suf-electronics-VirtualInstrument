﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;

namespace SUF.Instrument.Serial
{
    public partial class FormSerialPortList : Form
    {
        public bool IsSaved = false;
        public SerialPortEx port;
        public FormSerialPortList()
        {
            InitializeComponent();

            this.lbxConnection.DisplayMember = "PortName";
            this.lbxConnection.ValueMember = "Value";
            this.lbxConnection.DataSource = SerialPortCollection.GetSerialPortCollection.SerialPorts;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FormSerialConnectionSettings serialDialog = new FormSerialConnectionSettings();
            serialDialog.ShowDialog();
            if(serialDialog.IsSaved)
            {
                SerialPortCollection.GetSerialPortCollection.SerialPorts.Add(((SerialConnection)serialDialog.connection).ComPort);
                SerialPortCollection.GetSerialPortCollection.SerialPorts.ResetBindings();
            }
            serialDialog.Dispose();
        }

        private void lbxConnection_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.lbxConnection.IndexFromPoint(e.Location) >= 0)
            {
                EditPort((SerialPortEx)this.lbxConnection.Items[this.lbxConnection.IndexFromPoint(e.Location)]);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditPort((SerialPortEx)this.lbxConnection.SelectedItem);
        }

        private void EditPort(SerialPortEx port)
        {
            if (!port.IsOpen)
            {
                FormSerialConnectionSettings serialDialog = new FormSerialConnectionSettings();
                serialDialog.connection = new SerialConnection();
                ((SerialConnection)serialDialog.connection).ComPort = port;
                // this.port = port;
                serialDialog.ShowDialog();
                serialDialog.Dispose();
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {

            this.port = (SerialPortEx)this.lbxConnection.SelectedItem;
            this.IsSaved = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {

        }
        public static void SetupSerialPort(ref SerialPortEx port)
        {
            FormSerialPortList serialDialog = new FormSerialPortList();
            if (port != null)
            {
                serialDialog.port = port;
            }
            serialDialog.ShowDialog();
            if (serialDialog.IsSaved)
            {
                port = serialDialog.port;
            }
            serialDialog.Dispose();
        }

        private void FormConnectionList_Load(object sender, EventArgs e)
        {
            if(this.port != null)
            {
                foreach(SerialPortEx port in this.lbxConnection.Items)
                {
                    if(port == this.port)
                    {
                        this.lbxConnection.SelectedItem = this.port;
                        break;
                    }
                }
            }
        }
    }
}
