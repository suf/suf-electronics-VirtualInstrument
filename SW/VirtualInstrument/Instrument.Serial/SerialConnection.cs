﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SUF.Instrument;
using System.IO;
using System.IO.Ports;

namespace SUF.Instrument.Serial
{
    [PluginDescriptor("Serial")]
    [PluginSettingsClass("SUF.Instrument.Serial.FormSerialConnectionSettings")]
    public class SerialConnection : Connection
    {
        public SerialPortEx ComPort;
        private string _name = "";
        public override bool IsOpen
        {
            get
            {
                return ComPort.IsOpen;
            }
        }

        public override InstrumentRetObject Close()
        {
            if (ComPort.IsOpen)
            {
                ComPort.Close();
            }
            return new InstrumentRetObject(InstrumentRetCode.OK);
        }

        public override InstrumentRetObject Open()
        {
            InstrumentRetObject retObj = new InstrumentRetObject(InstrumentRetCode.OK);

            if (!ComPort.IsOpen)
            {
                try
                {
                    ComPort.Open();
                }
                catch(Exception e)
                {
                    retObj.RetCode = InstrumentRetCode.PHYOpenError;
                    retObj.RetInfo = new object[] { e };
                    return retObj;
                }
            }
            else
            {
                retObj.RetCode = InstrumentRetCode.PHYAlreadyOpen;
            }
            return retObj;
        }

        public override InstrumentRetObject Read(string Command, out string value)
        {
            InstrumentRetObject retObj = new InstrumentRetObject(InstrumentRetCode.OK);
            value = "";
            if (ComPort.IsOpen)
            {
                try
                {
                    // later handle the timeout here, use read char one-by-one instead of ReadLine
                    value = ComPort.ReadLine();
                }
                catch(Exception e)
                {
                    retObj.RetCode = InstrumentRetCode.PHYCommunicationError;
                    retObj.RetInfo = new object[] { e };
                    return retObj;
                }
            }
            else
            {
                retObj.RetCode = InstrumentRetCode.PHYNotOpen;
            }
            return retObj;
        }

        public override InstrumentRetObject Send(string Command)
        {
            InstrumentRetObject retObj = new InstrumentRetObject(InstrumentRetCode.OK);
            if (ComPort.IsOpen)
            {
                try
                {
                    ComPort.WriteLine(Command);
                }
                catch(Exception e)
                {
                    retObj.RetCode = InstrumentRetCode.PHYCommunicationError;
                    retObj.RetInfo = new object[] { e };
                }
            }
            else
            {
                retObj.RetCode = InstrumentRetCode.PHYNotOpen;
            }
            return retObj;
        }

        public override string Name
        {
            get
            {
                if (this._name == "")
                {
                    if (this.ComPort != null)
                    {
                        if (this.ComPort.PortName != "")
                        {
                            this._name = "SERIAL:" + this.ComPort.PortName;
                        }
                    }
                }
                return this._name;
            }
            set => this._name = value;
        }

        public override PhysicalConnectionType ConnectionType
        {
            get
            {
                return PhysicalConnectionType.Serial;
            }
        }
    }
}
