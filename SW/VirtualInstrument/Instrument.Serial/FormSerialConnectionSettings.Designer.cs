﻿namespace SUF.Instrument.Serial
{
    partial class FormSerialConnectionSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbxPort = new System.Windows.Forms.ComboBox();
            this.lblPort = new System.Windows.Forms.Label();
            this.lblBaud = new System.Windows.Forms.Label();
            this.lblHandshake = new System.Windows.Forms.Label();
            this.lblParity = new System.Windows.Forms.Label();
            this.lblStopBits = new System.Windows.Forms.Label();
            this.lblBits = new System.Windows.Forms.Label();
            this.chkRtsDtr = new System.Windows.Forms.CheckBox();
            this.cbxBaud = new System.Windows.Forms.ComboBox();
            this.cbxHandshake = new System.Windows.Forms.ComboBox();
            this.cbxParity = new System.Windows.Forms.ComboBox();
            this.cbxStopBits = new System.Windows.Forms.ComboBox();
            this.cbxBits = new System.Windows.Forms.ComboBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.tbxName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cbxPort
            // 
            this.cbxPort.FormattingEnabled = true;
            this.cbxPort.Location = new System.Drawing.Point(117, 44);
            this.cbxPort.Margin = new System.Windows.Forms.Padding(4);
            this.cbxPort.Name = "cbxPort";
            this.cbxPort.Size = new System.Drawing.Size(375, 24);
            this.cbxPort.TabIndex = 0;
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(17, 48);
            this.lblPort.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(38, 17);
            this.lblPort.TabIndex = 1;
            this.lblPort.Text = "Port:";
            // 
            // lblBaud
            // 
            this.lblBaud.AutoSize = true;
            this.lblBaud.Location = new System.Drawing.Point(17, 81);
            this.lblBaud.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBaud.Name = "lblBaud";
            this.lblBaud.Size = new System.Drawing.Size(45, 17);
            this.lblBaud.TabIndex = 2;
            this.lblBaud.Text = "Baud:";
            // 
            // lblHandshake
            // 
            this.lblHandshake.AutoSize = true;
            this.lblHandshake.Location = new System.Drawing.Point(17, 114);
            this.lblHandshake.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHandshake.Name = "lblHandshake";
            this.lblHandshake.Size = new System.Drawing.Size(84, 17);
            this.lblHandshake.TabIndex = 3;
            this.lblHandshake.Text = "Handshake:";
            // 
            // lblParity
            // 
            this.lblParity.AutoSize = true;
            this.lblParity.Location = new System.Drawing.Point(17, 148);
            this.lblParity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblParity.Name = "lblParity";
            this.lblParity.Size = new System.Drawing.Size(48, 17);
            this.lblParity.TabIndex = 4;
            this.lblParity.Text = "Parity:";
            // 
            // lblStopBits
            // 
            this.lblStopBits.AutoSize = true;
            this.lblStopBits.Location = new System.Drawing.Point(287, 114);
            this.lblStopBits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStopBits.Name = "lblStopBits";
            this.lblStopBits.Size = new System.Drawing.Size(67, 17);
            this.lblStopBits.TabIndex = 5;
            this.lblStopBits.Text = "Stop bits:";
            // 
            // lblBits
            // 
            this.lblBits.AutoSize = true;
            this.lblBits.Location = new System.Drawing.Point(287, 81);
            this.lblBits.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBits.Name = "lblBits";
            this.lblBits.Size = new System.Drawing.Size(35, 17);
            this.lblBits.TabIndex = 6;
            this.lblBits.Text = "Bits:";
            // 
            // chkRtsDtr
            // 
            this.chkRtsDtr.AutoSize = true;
            this.chkRtsDtr.Location = new System.Drawing.Point(363, 146);
            this.chkRtsDtr.Margin = new System.Windows.Forms.Padding(4);
            this.chkRtsDtr.Name = "chkRtsDtr";
            this.chkRtsDtr.Size = new System.Drawing.Size(91, 21);
            this.chkRtsDtr.TabIndex = 7;
            this.chkRtsDtr.Text = "RTS/DTR";
            this.chkRtsDtr.UseVisualStyleBackColor = true;
            // 
            // cbxBaud
            // 
            this.cbxBaud.FormattingEnabled = true;
            this.cbxBaud.Location = new System.Drawing.Point(117, 78);
            this.cbxBaud.Margin = new System.Windows.Forms.Padding(4);
            this.cbxBaud.Name = "cbxBaud";
            this.cbxBaud.Size = new System.Drawing.Size(160, 24);
            this.cbxBaud.TabIndex = 8;
            // 
            // cbxHandshake
            // 
            this.cbxHandshake.FormattingEnabled = true;
            this.cbxHandshake.Location = new System.Drawing.Point(117, 111);
            this.cbxHandshake.Margin = new System.Windows.Forms.Padding(4);
            this.cbxHandshake.Name = "cbxHandshake";
            this.cbxHandshake.Size = new System.Drawing.Size(160, 24);
            this.cbxHandshake.TabIndex = 9;
            // 
            // cbxParity
            // 
            this.cbxParity.FormattingEnabled = true;
            this.cbxParity.Location = new System.Drawing.Point(117, 144);
            this.cbxParity.Margin = new System.Windows.Forms.Padding(4);
            this.cbxParity.Name = "cbxParity";
            this.cbxParity.Size = new System.Drawing.Size(160, 24);
            this.cbxParity.TabIndex = 10;
            // 
            // cbxStopBits
            // 
            this.cbxStopBits.FormattingEnabled = true;
            this.cbxStopBits.Location = new System.Drawing.Point(363, 111);
            this.cbxStopBits.Margin = new System.Windows.Forms.Padding(4);
            this.cbxStopBits.Name = "cbxStopBits";
            this.cbxStopBits.Size = new System.Drawing.Size(129, 24);
            this.cbxStopBits.TabIndex = 11;
            // 
            // cbxBits
            // 
            this.cbxBits.FormattingEnabled = true;
            this.cbxBits.Items.AddRange(new object[] {
            "8",
            "7"});
            this.cbxBits.Location = new System.Drawing.Point(363, 78);
            this.cbxBits.Margin = new System.Windows.Forms.Padding(4);
            this.cbxBits.Name = "cbxBits";
            this.cbxBits.Size = new System.Drawing.Size(129, 24);
            this.cbxBits.TabIndex = 12;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(16, 192);
            this.btnOk.Margin = new System.Windows.Forms.Padding(4);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(100, 39);
            this.btnOk.TabIndex = 13;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(124, 192);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 39);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(16, 16);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 17);
            this.lblName.TabIndex = 15;
            this.lblName.Text = "Name:";
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(117, 13);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(376, 22);
            this.tbxName.TabIndex = 16;
            // 
            // FormSerialConnectionSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 246);
            this.Controls.Add(this.tbxName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.cbxBits);
            this.Controls.Add(this.cbxStopBits);
            this.Controls.Add(this.cbxParity);
            this.Controls.Add(this.cbxHandshake);
            this.Controls.Add(this.cbxBaud);
            this.Controls.Add(this.chkRtsDtr);
            this.Controls.Add(this.lblBits);
            this.Controls.Add(this.lblStopBits);
            this.Controls.Add(this.lblParity);
            this.Controls.Add(this.lblHandshake);
            this.Controls.Add(this.lblBaud);
            this.Controls.Add(this.lblPort);
            this.Controls.Add(this.cbxPort);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormSerialConnectionSettings";
            this.Text = "Serial Settings";
            this.Load += new System.EventHandler(this.FormSerialConnectionSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxPort;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.Label lblBaud;
        private System.Windows.Forms.Label lblHandshake;
        private System.Windows.Forms.Label lblParity;
        private System.Windows.Forms.Label lblStopBits;
        private System.Windows.Forms.Label lblBits;
        private System.Windows.Forms.CheckBox chkRtsDtr;
        private System.Windows.Forms.ComboBox cbxBaud;
        private System.Windows.Forms.ComboBox cbxHandshake;
        private System.Windows.Forms.ComboBox cbxParity;
        private System.Windows.Forms.ComboBox cbxStopBits;
        private System.Windows.Forms.ComboBox cbxBits;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox tbxName;
    }
}