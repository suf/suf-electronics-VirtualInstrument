﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    public abstract class Connection
    {
        public abstract InstrumentRetObject Read(string Command, out string value);
        // public abstract InstrumentRetCode Read(out string value);
        public abstract InstrumentRetObject Send(string Command);
        public abstract InstrumentRetObject Open();
        public abstract InstrumentRetObject Close();
        public abstract bool IsOpen{get;}
        public virtual Connection Value
        {
            get
            {
                return this;
            }
        }
        public virtual string Name
        {
            get
            {
                return this.ToString();
            }
            set { }
        }
        public abstract PhysicalConnectionType ConnectionType
        {
            get;
        }
        public GPIB_Terminator ReadTerminator = GPIB_Terminator.EOI;
    }
}
