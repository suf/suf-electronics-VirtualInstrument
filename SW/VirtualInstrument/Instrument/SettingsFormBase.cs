﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SUF.Instrument
{
    public class SettingsFormBase : Form
    {
        public bool IsSaved = false;
        protected void UpdateEnumComboBox<T>(ComboBox cbx)
        {
            cbx.Items.Clear();
            foreach (T cbxEnum in Enum.GetValues(typeof(T)))
            {
                cbx.Items.Add(cbxEnum);
            }
        }
        protected void UpdateEnumComboBox<T>(ComboBox cbx, T value)
        {
            int ItemID;
            cbx.Items.Clear();
            foreach (T cbxEnum in Enum.GetValues(typeof(T)))
            {
                ItemID = cbx.Items.Add(cbxEnum);
                if (((T)cbx.Items[ItemID]).Equals(value))
                {
                    cbx.SelectedIndex = ItemID;
                }
            }
        }
    }
}
