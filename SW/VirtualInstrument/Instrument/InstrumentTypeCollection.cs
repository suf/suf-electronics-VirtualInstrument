﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Collection of the Instrument plugins
    /// </summary>
    public sealed class InstrumentTypeCollection  : IEnumerable<InstrumentPlugin>
    {
        private InstrumentTypeCollection()
        {
            this.InstrumentTypes = new List<InstrumentPlugin>();
        }
        private static readonly Lazy<InstrumentTypeCollection> lazy = new Lazy<InstrumentTypeCollection>(() => new InstrumentTypeCollection());
        public static InstrumentTypeCollection GetInstrumentTypeCollection
        {
            get
            {
                return lazy.Value;
            }
        }
        public List<InstrumentPlugin> InstrumentTypes;
        public int Add(Type instrumentType, string descriptor)
        {
            InstrumentTypes.Add(new InstrumentPlugin(instrumentType, descriptor));
            return InstrumentTypes.Count - 1;
        }
        public InstrumentPlugin GetInstrumentPlugin(Type InstrumentType)
        {
            foreach(InstrumentPlugin plugin in this.InstrumentTypes)
            {
                if(InstrumentType == plugin.InstrumentType)
                {
                    return plugin;
                }
            }
            return null;
        }
        public void AddSettingsType(int InstrumentID, Type SettingsType)
        {
            InstrumentTypes[InstrumentID].SetSettingsType(SettingsType);
        }
        public IEnumerator<InstrumentPlugin> GetEnumerator()
        {
            return this.InstrumentTypes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.InstrumentTypes.GetEnumerator();
        }

    }
    public class InstrumentPlugin
    {
        public InstrumentPlugin(Type instrumentType, string descriptor)
        {
            _instrumentType = instrumentType;
            _descriptor = descriptor;
        }
        private Type _instrumentType;
        private Type _instrumentSettingsType;
        private string _descriptor;
        public string Name
        {
            get
            {
                return (_descriptor != null) ? _descriptor : _instrumentType.ToString();
         
            }
        }
        public Type InstrumentType
        {
            get
            {
                return this._instrumentType;
            }
        }

        public Instrument GetInstrument()
        {
            return (Instrument)Activator.CreateInstance(_instrumentType);
        }
        public InstrumentSettingsFormBase GetSettingsForm()
        {
            if (this._instrumentSettingsType != null)
            {
                return (InstrumentSettingsFormBase)Activator.CreateInstance(_instrumentSettingsType);
            }
            else
            {
                return null;
            }
        }
        public void SetSettingsType(Type settingsType)
        {
            this._instrumentSettingsType = settingsType;
        }
        public override string ToString()
        {
            return Name;
        }
        public InstrumentPlugin Value
        {
            get
            {
                return this;
            }
        }
    }
}
