﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Base type of the instrument (measuring) objects
    /// </summary>
    public class Instrument : ResultProvider
    {
        public Connection conn;
        // protected bool IsInitialized;
        public virtual InstrumentRetObject Measure(MeasurementObject results)
        { return null; }
        public virtual InstrumentRetObject Init()
        { return null; }
        protected InstrumentRetObject Setup(GPIB_Terminator readTerminator, string[] initalCommands)
        {
            InstrumentRetObject retObj = new InstrumentRetObject();
            retObj.RetCode = InstrumentRetCode.OK;
            if (conn != null)
            {
                conn.ReadTerminator = readTerminator;
                if (!conn.IsOpen)
                {
                    retObj = conn.Open();
                    if (retObj.RetCode != InstrumentRetCode.OK)
                    {
                        return retObj;
                    }
                }
                if(!conn.IsOpen)
                {
                    retObj.RetCode = InstrumentRetCode.PHYOpenError;
                }
            }
            else
            {
                retObj.RetCode = InstrumentRetCode.MissingPHY;
            }
            if(retObj.RetCode == InstrumentRetCode.OK)
            {
                foreach(string Command in initalCommands)
                {
                    retObj = conn.Send(Command);
                    if(retObj.RetCode != InstrumentRetCode.OK)
                    {
                        return retObj;
                    }
                }
            }
            return retObj;
        }
    }
}
