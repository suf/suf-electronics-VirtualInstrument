﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Collection of the connection plugins
    /// </summary>
    public sealed class ConnectionTypeCollection : IEnumerable<ConnectionPlugin>
    {
        private ConnectionTypeCollection()
        {
            this.ConnectionTypes = new List<ConnectionPlugin>();
        }
        private static readonly Lazy<ConnectionTypeCollection> lazy = new Lazy<ConnectionTypeCollection>(() => new ConnectionTypeCollection());
        public static ConnectionTypeCollection GetConnectionTypeCollection
        {
            get
            {
                return lazy.Value;
            }
        }
        public List<ConnectionPlugin> ConnectionTypes;
        public int Add(Type connectionType, string descriptor)
        {
            ConnectionTypes.Add(new ConnectionPlugin(connectionType, descriptor));
            return ConnectionTypes.Count - 1;
        }
        public ConnectionPlugin GetConnectionPlugin(Type ConnectionType)
        {
            foreach (ConnectionPlugin plugin in this.ConnectionTypes)
            {
                if (ConnectionType == plugin.ConnectionType)
                {
                    return plugin;
                }
            }
            return null;
        }

        public void AddSettingsType(int ConnectionID, Type SettingsType)
        {
            ConnectionTypes[ConnectionID].SetSettingsType(SettingsType);
        }
        public IEnumerator<ConnectionPlugin> GetEnumerator()
        {
            return this.ConnectionTypes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.ConnectionTypes.GetEnumerator();
        }

    }
    public class ConnectionPlugin
    {
        public ConnectionPlugin(Type connectionType, string descriptor)
        {
            _connectionType = connectionType;
            _descriptor = descriptor;
        }
        private Type _connectionType;
        private Type _connectionSettingsType;
        private string _descriptor;
        public string Name
        {
            get
            {
                return (_descriptor != null) ? _descriptor : _connectionType.ToString();
            }
        }
        public Type ConnectionType
        {
            get
            {
                return this._connectionType;
            }
        }
        public Connection GetConnection()
        {
            return (Connection)Activator.CreateInstance(_connectionType);
        }
        public ConnectionSettingsFormBase GetSettingsForm()
        {
            if (this._connectionSettingsType != null)
            {
                return (ConnectionSettingsFormBase)Activator.CreateInstance(_connectionSettingsType);
            }
            else
            {
                return null;
            }
        }
        public void SetSettingsType(Type settingsType)
        {
            this._connectionSettingsType = settingsType;
        }
        public override string ToString()
        {
            return Name;
        }
        public ConnectionPlugin Value
        {
            get
            {
                return this;
            }
        }
    }

}
