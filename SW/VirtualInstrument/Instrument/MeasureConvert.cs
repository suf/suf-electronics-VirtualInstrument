﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    public static class MeasureConvert
    {
        public static double Voltage2dBu(double voltage)
        {
            return 20 * Math.Log10(voltage / 0.775);
        }
        public static double dBu2Voltage(double dBu)
        {
            return 0.775 * Math.Pow(10, dBu / 20);
        }

        public static double Voltage2dBV(double voltage)
        {
            return 20 * Math.Log10(voltage);
        }
        public static double dBV2Voltage(double dBV)
        {
            return Math.Pow(10, dBV / 20);
        }

        public static double Voltage2dBm(double voltage, double load)
        {
            return 10 * Math.Log(Math.Pow(voltage, 2) / load / 0.001);
        }
        public static double dBm2Voltage(double dBm, double load)
        {
            return Math.Sqrt(load * Math.Pow(10, dBm / 10 - 3));
        }
    }
}
