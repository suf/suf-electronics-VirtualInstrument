﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    [AttributeUsage(AttributeTargets.Class)]
    public class PluginDescriptorAttribute : Attribute
    {
        private string _descriptor;
        public PluginDescriptorAttribute(string descriptor)
        {
            _descriptor = descriptor;
        }
        public string Descriptor
        {
            get
            {
                return _descriptor;
            }
            set
            {
                _descriptor = value;
            }
        }
    }
    [AttributeUsage(AttributeTargets.Class)]
    public class PluginSettingsClassAttribute : Attribute
    {
        private string _classname;
        public PluginSettingsClassAttribute(string classname)
        {
            _classname = classname;
        }
        public string ClassName
        {
            get
            {
                return _classname;
            }
            set
            {
                _classname = value;
            }
        }
    }
}
