﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Base class of all the classes able to generate measurement result
    /// Controllers, Instruments, Filters
    /// Basically it generate GUID for all instances. It is used by the MeasurementResultDescriptor ???
    /// </summary>
    public class ResultProvider
    {
        // Configuration parameters - need to be saved
        public string Name;
        public List<ResultDescriptor> sourceDescriptors = new List<ResultDescriptor>();
        protected Guid _guid = Guid.NewGuid();

        public Guid InstanceID
        {
            get
            {
                return this._guid;
            }
        }
        public ResultDescriptor GetResultDescriptor(int ChannelID)
        {
            foreach(ResultDescriptor descriptor in sourceDescriptors)
            {
                if(descriptor.ChannelID == ChannelID)
                {
                    return descriptor;
                }
            }
            return null;
        }
    }
}
