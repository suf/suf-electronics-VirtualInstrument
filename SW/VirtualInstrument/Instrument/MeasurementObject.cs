﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace SUF.Instrument
{
    public class MeasurementObject
    {
        public MeasurementObject()
        {
            this._values = new Dictionary<Guid, Dictionary<int, MeasurementValue>>();
        }
        public DateTime TimeStamp;
        public int SequenceNumber;
        private Dictionary<Guid, Dictionary<int,MeasurementValue>> _values;

        public void Add(Guid SourceGuid, int ChannelID, MeasurementValue value)
        {
            if (!this._values.ContainsKey(SourceGuid))
            {
                this._values.Add(SourceGuid, new Dictionary<int, MeasurementValue>());
            }
            if (!this._values[SourceGuid].ContainsKey(ChannelID))
            {
                this._values[SourceGuid].Add(ChannelID, value);
            }
            else
            {
                this._values[SourceGuid][ChannelID] = value;
            }
        }
        public void Add(ResultDescriptor descriptor, MeasurementValue value)
        {
            this.Add(descriptor.SourceGuid, descriptor.ChannelID, value);
        }

        public MeasurementValue Get(Guid SourceGuid, int ChannelID)
        {
            if(this._values.ContainsKey(SourceGuid))
            {
                if(this._values[SourceGuid].ContainsKey(ChannelID))
                {
                    return this._values[SourceGuid][ChannelID];
                }
            }
            return null;
        }

        public MeasurementValue Get(ResultDescriptor descriptor)
        {
            return this.Get(descriptor.SourceGuid, descriptor.ChannelID);
        }

        /*
        public MeasurementObject()
        {
            this.Values = new Dictionary<int,MeasurementValue>();
        }
        public DateTime TimeStamp;
        public int SequenceNumber;
        public Dictionary<int, MeasurementValue> Values;
        */
    }

    public class MeasurementValue
    {
        public MeasurementValue(double value)
        {
            this.value = value;
        }
        public MeasurementValue(double value, MetricPrefix prefix)
        {
            // convert from prefix
            this.value = value * Math.Pow(10, Convert.ToDouble(prefix));
        }

        public double value;
        public override string ToString()
        {
            // calculate the prefix
            // use logarithm to find the correct value
            return value.ToString();
        }
        public string ToString(int digits)
        {
            // calculate the prefix
            return DoubleToMeasurementString(this.value, digits);
        }
        public static string DoubleToMeasurementString(double value, int digits)
        {
            string retValue;
            int exponent = 0;
            double mantissa = 0;
            if (value != 0)
            {
                exponent = Convert.ToInt32(Math.Floor(Math.Log10(Math.Abs(value)) / 3) * 3);
                mantissa = value / Math.Pow(10, exponent);
            }
            MetricPrefix prefix = (MetricPrefix)exponent;
            retValue = mantissa.ToString("0." + new String('0', digits), CultureInfo.InvariantCulture).Substring(0,digits+(value < 0 ? 2 : 1)) + " " + prefix.GetDescription();
            return retValue;
        }
    }
    public class ResultDescriptor
    {
        public ResultDescriptor() { }
        public Guid SourceGuid;
        public int ChannelID;
        public string SourceName;
        public string ChannelName;
        public MeasureUnit unit;
        // public string Description;
        // public string Name;
        public string Name
        {
            get
            {
                return SourceName + " | " + ChannelName;
            }
        }
        public override string ToString()
        {
            return this.Name;
        }
        public ResultDescriptor Value
        {
            get
            {
                return this;
            }
        }
    }

    public enum MetricPrefix
    {
        [Description("y")]
        yocto = -24,
        [Description("z")]
        zepto = -21,
        [Description("a")]
        atto = -18,
        [Description("f")]
        femto = -15,
        [Description("p")]
        pico = -12,
        [Description("n")]
        nano = -9,
        [Description("u")]
        micro = -6,
        [Description("m")]
        milli = -3,
        [Description("c")]
        centi = -2,
        [Description("d")]
        deci = -1,
        [Description("")]
        none = 0,
        [Description("da")]
        deca = 1,
        [Description("y")]
        hecto = 2,
        [Description("k")]
        kilo = 3,
        [Description("M")]
        mega = 6,
        [Description("G")]
        giga = 9,
        [Description("T")]
        tera = 12,
        [Description("P")]
        peta = 15,
        [Description("E")]
        exa = 18,
        [Description("Z")]
        zeta = 21,
        [Description("Y")]
        yotta = 24
    }
    public enum MeasureUnit
    {
        [Description("None")]
        none,
        [Description("Unknown")]
        unknown,
        [Description("V")]
        volt,
        [Description("A")]
        ampere,
        [Description("hz")]
        hertz,
        [Description("ohm")]
        ohm,
        [Description("F")]
        farad,
        [Description("H")]
        henry,
        [Description("°K")]
        kelvin,
        [Description("°F")]
        farenheit,
        [Description("°C")]
        celsius,
        [Description("°")]
        degree,
        [Description("%")]
        percent,
        [Description("dB")]
        decibel,
        [Description("dBu")]
        dBu,
        [Description("dBm")]
        dBm,
        [Description("dBV")]
        dBV,
        [Description("W")]
        watt,
        [Description("s")]
        second
    }
}
