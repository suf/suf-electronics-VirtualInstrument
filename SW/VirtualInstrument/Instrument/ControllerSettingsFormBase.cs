﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace SUF.Instrument
{
    public class ControllerSettingsFormBase : SettingsFormBase
    {
        public Controller controller;
        public virtual void Init(Controller controller) { }
    }
}
