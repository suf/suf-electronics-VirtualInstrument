﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SUF.Instrument;

namespace SUF.Instrument
{
    public partial class FormConnection : Form
    {
        public Connection connection;
        public bool IsSaved = false;
        public FormConnection()
        {
            InitializeComponent();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            IsSaved = true;
            this.Close();
        }

        private void BtnSettings_Click(object sender, EventArgs e)
        {
            ConnectionSettingsFormBase settingsForm = ((ConnectionPlugin)this.cbxConnType.SelectedItem).GetSettingsForm();
            settingsForm.ShowDialog();
            if(settingsForm.IsSaved)
            {
                this.connection = settingsForm.connection;
            }
            settingsForm.Dispose();
        }

        private void FormConnection_Load(object sender, EventArgs e)
        {
            this.cbxConnType.DisplayMember = "Name";
            this.cbxConnType.ValueMember = "Value";
            this.cbxConnType.DataSource = ConnectionTypeCollection.GetConnectionTypeCollection.ConnectionTypes;
            if(this.connection is Connection)
            {
                this.cbxConnType.SelectedValue = ConnectionTypeCollection.GetConnectionTypeCollection.GetConnectionPlugin(this.connection.GetType());
            }
        }
    }
}
