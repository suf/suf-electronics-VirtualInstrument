﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.ComponentModel;

namespace SUF.Instrument
{
    public static class ExtensionMethods
    {
        public static string GetDescription<T>(this T e) where T : IConvertible
        {
            if (e is Enum)
            {
                Type type = e.GetType();
                Array values = System.Enum.GetValues(type);

                foreach (int val in values)
                {
                    if (val == e.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var memInfo = type.GetMember(type.GetEnumName(val));
                        var descriptionAttribute = memInfo[0]
                            .GetCustomAttributes(typeof(DescriptionAttribute), false)
                            .FirstOrDefault() as DescriptionAttribute;

                        if (descriptionAttribute != null)
                        {
                            return descriptionAttribute.Description;
                        }
                    }
                }
            }
            return e.ToString(); // could also return string.Empty
        }
        public static string GetCommand<T>(this T e) where T : IConvertible
        {
            if (e is Enum)
            {
                Type type = e.GetType();
                Array values = System.Enum.GetValues(type);

                foreach (int val in values)
                {
                    if (val == e.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var memInfo = type.GetMember(type.GetEnumName(val));
                        var commandAttribute = memInfo[0]
                            .GetCustomAttributes(typeof(CommandAttribute), false)
                            .FirstOrDefault() as CommandAttribute;

                        if (commandAttribute != null)
                        {
                            return commandAttribute.Command;
                        }
                    }
                }
            }
            return null; // could also return string.Empty
        }

    }
    public class CommandAttribute : Attribute
    {
        public CommandAttribute(string command)
        {
            this.Command = command;
        }
        public string Command;
    }

    public enum PhysicalConnectionType
    {
        GPIB,
        Serial,
        LAN,
        HID,
        Unknown
    }

    public enum GPIB_Terminator
    {
        [Description("\0")]
        EOI,
        [Description("\r")]
        CR,
        [Description("\n")]
        LF,
        [Description("\r\n")]
        CRLF
    }
}
