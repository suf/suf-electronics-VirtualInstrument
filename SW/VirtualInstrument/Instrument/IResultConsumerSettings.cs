﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    public interface IResultConsumerSettings
    {
        void SetAvailableResults(List<ResultDescriptor> resultDescriptors);
    }
}
