﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Reflection;

namespace SUF.Instrument
{
    public class PluginLoader
    {
        public static void Load()
        {
            int PluginID;
            string Descriptor = null;
            string SettingsTypeName = null;
            List<Type> pluginTypes = new List<Type>();
            Dictionary<string, int> ClassOnSettings = new Dictionary<string, int>();
            string PluginFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"plugins");
            if(Directory.Exists(PluginFolder))
            {
                foreach(FileInfo pluginFile in new DirectoryInfo(PluginFolder).GetFiles("*.dll"))
                {
                    pluginTypes.AddRange(Assembly.LoadFile(pluginFile.FullName).GetTypes());
                }
                foreach (Type pluginType in pluginTypes)
                {
                    // Collect relevant custom attributes
                    foreach (Attribute attribute in Attribute.GetCustomAttributes(pluginType))
                    {
                        if (attribute is PluginDescriptorAttribute)
                        {
                            Descriptor = ((PluginDescriptorAttribute)attribute).Descriptor;
                        }
                        if (attribute is PluginSettingsClassAttribute)
                        {
                            SettingsTypeName = ((PluginSettingsClassAttribute)attribute).ClassName;
                        }
                    }
                    // Add plugin to the singleton
                    if (pluginType.IsSubclassOf(typeof(Connection)))
                    {
                        PluginID = ConnectionTypeCollection.GetConnectionTypeCollection.Add(pluginType, Descriptor);
                        if (SettingsTypeName != null)
                        {
                            if (!ClassOnSettings.ContainsKey(SettingsTypeName))
                            {
                                ClassOnSettings.Add(SettingsTypeName, PluginID);
                            }
                        }
                    }
                    if (pluginType.IsSubclassOf(typeof(Controller)))
                    {
                        PluginID = ControllerTypeCollection.GetControllerTypeCollection.Add(pluginType, Descriptor);
                        if (SettingsTypeName != null)
                        {
                            if (!ClassOnSettings.ContainsKey(SettingsTypeName))
                            {
                                ClassOnSettings.Add(SettingsTypeName, PluginID);
                            }
                        }
                    }
                    if(pluginType.IsSubclassOf(typeof(Instrument)))
                    {
                        PluginID = InstrumentTypeCollection.GetInstrumentTypeCollection.Add(pluginType, Descriptor);
                        if (SettingsTypeName != null)
                        {
                            if (!ClassOnSettings.ContainsKey(SettingsTypeName))
                            {
                                ClassOnSettings.Add(SettingsTypeName, PluginID);
                            }
                        }
                    }
                    if (pluginType.IsSubclassOf(typeof(Filter)))
                    {
                        PluginID = FilterTypeCollection.GetFilterTypeCollection.Add(pluginType, Descriptor);
                        if (SettingsTypeName != null)
                        {
                            if (!ClassOnSettings.ContainsKey(SettingsTypeName))
                            {
                                ClassOnSettings.Add(SettingsTypeName, PluginID);
                            }
                        }
                    }
                    if (pluginType.GetInterfaces().Contains(typeof(ITarget)))
                    {
                        PluginID = TargetTypeCollection.GetTargetTypeCollection.Add(pluginType, Descriptor);
                        if (SettingsTypeName != null)
                        {
                            if (!ClassOnSettings.ContainsKey(SettingsTypeName))
                            {
                                ClassOnSettings.Add(SettingsTypeName, PluginID);
                            }
                        }
                    }
                    Descriptor = null;
                    SettingsTypeName = null;
                }
                foreach (Type pluginType in pluginTypes)
                {
                    // Connect settings form to the plugin type
                    if (pluginType.IsSubclassOf(typeof(ConnectionSettingsFormBase)))
                    {
                        if (ClassOnSettings.ContainsKey(pluginType.FullName))
                        {
                            ConnectionTypeCollection.GetConnectionTypeCollection.AddSettingsType(ClassOnSettings[pluginType.FullName], pluginType);
                        }
                    }
                    if (pluginType.IsSubclassOf(typeof(ControllerSettingsFormBase)))
                    {
                        if(ClassOnSettings.ContainsKey(pluginType.FullName))
                        {
                            ControllerTypeCollection.GetControllerTypeCollection.AddSettingsType(ClassOnSettings[pluginType.FullName], pluginType);
                        }
                    }
                    if (pluginType.IsSubclassOf(typeof(InstrumentSettingsFormBase)))
                    {
                        if (ClassOnSettings.ContainsKey(pluginType.FullName))
                        {
                            InstrumentTypeCollection.GetInstrumentTypeCollection.AddSettingsType(ClassOnSettings[pluginType.FullName], pluginType);
                        }
                    }
                    if (pluginType.IsSubclassOf(typeof(FilterSettingsFormBase)))
                    {
                        if (ClassOnSettings.ContainsKey(pluginType.FullName))
                        {
                            FilterTypeCollection.GetFilterTypeCollection.AddSettingsType(ClassOnSettings[pluginType.FullName], pluginType);
                        }
                    }
                    if (pluginType.IsSubclassOf(typeof(TargetSettingsFormBase)))
                    {
                        if (ClassOnSettings.ContainsKey(pluginType.FullName))
                        {
                            TargetTypeCollection.GetTargetTypeCollection.AddSettingsType(ClassOnSettings[pluginType.FullName], pluginType);
                        }
                    }
                }
            }
        }
    }
}
