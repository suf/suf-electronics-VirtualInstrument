﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUF.Instrument
{
    /// <summary>
    /// Controller base class
    /// </summary>
    public class Controller : ResultProvider
    {
        // Configuration parameters - need to be saved
        public List<Instrument> instruments = new List<Instrument>();
        public List<Filter> filters = new List<Filter>();
        public List<ITarget> targets = new List<ITarget>();
        public bool Chain;
        public bool Async;

        public List<ResultDescriptor> resultDescriptors = new List<ResultDescriptor>();
        public ControllerState State = ControllerState.Stopped;
        public virtual void Start() { }
        public virtual void Stop() { }
        /// <summary>
        /// This method copy all of the parameters from another controller object
        /// </summary>
        /// <param name="source"></param>
        public void FromController(Controller source)
        {
            base._guid = source._guid;
            base.Name = source.Name;

            this.instruments = source.instruments;
            this.filters = source.filters;
            this.targets = source.targets;
            // generated - no copy needed
            // this.resultDescriptors = source.resultDescriptors;
            this.Chain = source.Chain;
            this.Async = source.Async;
        }
        public List<ResultDescriptor> GetAvailableDescriptors(Guid ConsumerID)
        {
            // This is used only by the Instruments and Filters, where just a subset of results available based on the execution order
            List<ResultDescriptor> AvailableDescriptors = new List<ResultDescriptor>();
            foreach(ResultDescriptor current in this.resultDescriptors)
            {
                if(current.SourceGuid == ConsumerID)
                {
                    return AvailableDescriptors;
                }
                else
                {
                    AvailableDescriptors.Add(current);
                }
            }
            return AvailableDescriptors;
        }
        public Controller() { }
        // Helper methodes
        protected void InitDevices()
        {
            foreach (Instrument instrument in this.instruments)
            {
                if (instrument != null)
                {
                    instrument.Init();
                }
            }
            foreach (Filter filter in this.filters)
            {
                if (filter != null)
                {
                    filter.Init();
                }
            }
            foreach (ITarget target in this.targets)
            {
                if (target != null)
                {
                    target.Init();
                }
            }
        }
        protected void RunMeasurementCycle(MeasurementObject results)
        {
            foreach (Instrument instrument in this.instruments)
            {
                if (instrument != null)
                {
                    instrument.Measure(results);
                }
            }
            foreach (Filter filter in this.filters)
            {
                if (filter != null)
                {
                    filter.FilterResult(results);
                }
            }
            foreach (ITarget target in this.targets)
            {
                if (target != null)
                {
                    if (target is IResultConsumer)
                    {
                        ((IResultConsumer)target).ProcessResult(results);
                    }
                }
            }
        }
    }
    public enum ControllerState { Stopped, Paused, Running }
}
