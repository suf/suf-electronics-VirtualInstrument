﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SUF.Instrument;

namespace SUF.Instrument.Mock
{
    [PluginDescriptor("Mock 1V Source")]
    public class ContinousVoltageSource : Instrument
    {
        public ContinousVoltageSource()
        {
            ResultDescriptor src = new ResultDescriptor();
            src.ChannelID = 0;
            src.SourceGuid = this._guid;
            src.unit = MeasureUnit.volt;
            src.ChannelName = "Mock 1V";
            this.sourceDescriptors.Add(src);
        }
        public override InstrumentRetObject Init()
        {
            // IsInitialized = true;
            return new InstrumentRetObject(InstrumentRetCode.OK);
        }
        public override InstrumentRetObject Measure(MeasurementObject results)
        {
            results.Add(sourceDescriptors[0], new MeasurementValue(1));
            return new InstrumentRetObject(InstrumentRetCode.OK);
        }
    }
}
