﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;

using SUF.Instrument;

namespace SUF.Instrument.HP3478A
{
    [PluginDescriptor("HP 3478A Multimeter")]
    [PluginSettingsClass("SUF.Instrument.HP3478A.FormHP3478AInstrumentSettings")]
    public class InstrumentHP3478A : Instrument
    {
        public ResultDescriptor resultDescriptor;
        public InstrumentHP3478A()
        {
            base.sourceDescriptors = new List<ResultDescriptor>();
            resultDescriptor = new ResultDescriptor();
            resultDescriptor.SourceGuid = base.InstanceID;
            resultDescriptor.ChannelID = 0;
            base.sourceDescriptors.Add(resultDescriptor);
            this.func = MeasurementFunction.DC_Volts;
        }
        private MeasurementFunction _func;
        public MeasurementFunction func
        {
            get
            {
                return this._func;
            }
            set
            {
                this._func = value;
                this.resultDescriptor.ChannelName = value.ToString();
                this.resultDescriptor.unit = Function2Unit(value);
            }
        }
        private MeasureUnit Function2Unit(MeasurementFunction func)
        {
            switch(func)
            {
                case MeasurementFunction.AC_Current:
                    return MeasureUnit.ampere;
                case MeasurementFunction.AC_Volts:
                    return MeasureUnit.volt;
                case MeasurementFunction.DC_Current:
                    return MeasureUnit.ampere;
                case MeasurementFunction.DC_Volts:
                    return MeasureUnit.volt;
                case MeasurementFunction.Ohms_2wire:
                    return MeasureUnit.ohm;
                case MeasurementFunction.Ohms_4wire:
                    return MeasureUnit.ohm;
                case MeasurementFunction.Ohms_Extended:
                    return MeasureUnit.ohm;
            }
            return MeasureUnit.unknown;
        }
        public Range range = Range.DCV_Auto;
        public Resolution precision = Resolution.Digit_55;
        public Trigger trigger; // Required?
        public bool AutoZero = true;
        public int ResultID;
        public override InstrumentRetObject Measure(MeasurementObject results)
        {
            string result;
            MeasurementValue value;
            InstrumentRetObject retObj = conn.Read(Trigger.Single.GetCommand(), out result);
            if(retObj.RetCode == InstrumentRetCode.OK)
            {
                value = new MeasurementValue(Convert.ToDouble(result, CultureInfo.InvariantCulture),MetricPrefix.none);
                results.Add(this.resultDescriptor, value);
            }
            return retObj;
        }
        public override InstrumentRetObject Init()
        {
            return Setup(GPIB_Terminator.EOI, new string[] { func.GetCommand() + range.GetCommand() + precision.GetCommand() + (AutoZero ? "Z1" : "Z0") + Trigger.Hold.GetCommand() });
        }
    }
}
