﻿namespace SUF.Instrument.Standard
{
    partial class FormTargetDigital
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDisplay = new System.Windows.Forms.Label();
            this.lblMinText = new System.Windows.Forms.Label();
            this.lblMaxText = new System.Windows.Forms.Label();
            this.lblAvgText = new System.Windows.Forms.Label();
            this.btnAggregateReset = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.lblMinValue = new System.Windows.Forms.Label();
            this.lblMaxValue = new System.Windows.Forms.Label();
            this.lblAvgValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblDisplay
            // 
            this.lblDisplay.BackColor = System.Drawing.Color.Black;
            this.lblDisplay.Font = new System.Drawing.Font("Courier New", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblDisplay.ForeColor = System.Drawing.Color.Chartreuse;
            this.lblDisplay.Location = new System.Drawing.Point(50, 89);
            this.lblDisplay.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDisplay.Name = "lblDisplay";
            this.lblDisplay.Size = new System.Drawing.Size(456, 88);
            this.lblDisplay.TabIndex = 0;
            this.lblDisplay.Text = "0";
            this.lblDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMinText
            // 
            this.lblMinText.AutoSize = true;
            this.lblMinText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblMinText.Location = new System.Drawing.Point(9, 50);
            this.lblMinText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMinText.Name = "lblMinText";
            this.lblMinText.Size = new System.Drawing.Size(38, 20);
            this.lblMinText.TabIndex = 1;
            this.lblMinText.Text = "Min:";
            // 
            // lblMaxText
            // 
            this.lblMaxText.AutoSize = true;
            this.lblMaxText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblMaxText.Location = new System.Drawing.Point(172, 50);
            this.lblMaxText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMaxText.Name = "lblMaxText";
            this.lblMaxText.Size = new System.Drawing.Size(42, 20);
            this.lblMaxText.TabIndex = 2;
            this.lblMaxText.Text = "Max:";
            // 
            // lblAvgText
            // 
            this.lblAvgText.AutoSize = true;
            this.lblAvgText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblAvgText.Location = new System.Drawing.Point(342, 50);
            this.lblAvgText.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAvgText.Name = "lblAvgText";
            this.lblAvgText.Size = new System.Drawing.Size(40, 20);
            this.lblAvgText.TabIndex = 3;
            this.lblAvgText.Text = "Avg:";
            // 
            // btnAggregateReset
            // 
            this.btnAggregateReset.Location = new System.Drawing.Point(450, 11);
            this.btnAggregateReset.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAggregateReset.Name = "btnAggregateReset";
            this.btnAggregateReset.Size = new System.Drawing.Size(56, 24);
            this.btnAggregateReset.TabIndex = 4;
            this.btnAggregateReset.Text = "Reset";
            this.btnAggregateReset.UseVisualStyleBackColor = true;
            this.btnAggregateReset.Click += new System.EventHandler(this.BtnAggregateReset_Click);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblName.Location = new System.Drawing.Point(9, 7);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(75, 26);
            this.lblName.TabIndex = 5;
            this.lblName.Text = "Name";
            // 
            // lblMinValue
            // 
            this.lblMinValue.BackColor = System.Drawing.Color.Black;
            this.lblMinValue.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblMinValue.ForeColor = System.Drawing.Color.Chartreuse;
            this.lblMinValue.Location = new System.Drawing.Point(51, 45);
            this.lblMinValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMinValue.Name = "lblMinValue";
            this.lblMinValue.Size = new System.Drawing.Size(120, 32);
            this.lblMinValue.TabIndex = 6;
            this.lblMinValue.Text = "Min";
            this.lblMinValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMaxValue
            // 
            this.lblMaxValue.BackColor = System.Drawing.Color.Black;
            this.lblMaxValue.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblMaxValue.ForeColor = System.Drawing.Color.Chartreuse;
            this.lblMaxValue.Location = new System.Drawing.Point(218, 45);
            this.lblMaxValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMaxValue.Name = "lblMaxValue";
            this.lblMaxValue.Size = new System.Drawing.Size(120, 32);
            this.lblMaxValue.TabIndex = 7;
            this.lblMaxValue.Text = "Max";
            this.lblMaxValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAvgValue
            // 
            this.lblAvgValue.BackColor = System.Drawing.Color.Black;
            this.lblAvgValue.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblAvgValue.ForeColor = System.Drawing.Color.Chartreuse;
            this.lblAvgValue.Location = new System.Drawing.Point(386, 45);
            this.lblAvgValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAvgValue.Name = "lblAvgValue";
            this.lblAvgValue.Size = new System.Drawing.Size(120, 32);
            this.lblAvgValue.TabIndex = 8;
            this.lblAvgValue.Text = "Avg";
            this.lblAvgValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FormTargetDigital
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 199);
            this.Controls.Add(this.lblAvgValue);
            this.Controls.Add(this.lblMaxValue);
            this.Controls.Add(this.lblMinValue);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.btnAggregateReset);
            this.Controls.Add(this.lblAvgText);
            this.Controls.Add(this.lblMaxText);
            this.Controls.Add(this.lblMinText);
            this.Controls.Add(this.lblDisplay);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormTargetDigital";
            this.Text = "MT_FormDigit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormTargetDigital_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDisplay;
        private System.Windows.Forms.Label lblMinText;
        private System.Windows.Forms.Label lblMaxText;
        private System.Windows.Forms.Label lblAvgText;
        private System.Windows.Forms.Button btnAggregateReset;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblMinValue;
        private System.Windows.Forms.Label lblMaxValue;
        private System.Windows.Forms.Label lblAvgValue;
    }
}