﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SUF.Instrument.Standard
{
    [PluginDescriptor("Variable Controller")]
    [PluginSettingsClass("SUF.Instrument.Standard.FormControllerSettingsVariable")]
    public class ControllerVariable : Controller
    {
        public ControllerVariable()
        {
            this.sourceDescriptors.Add(new ResultDescriptor());
            this.sourceDescriptors[0].ChannelID = 0;
            this.sourceDescriptors[0].SourceGuid = this._guid;
            this.sourceDescriptors[0].unit = MeasureUnit.hertz;
        }
        public MetricPrefix StartPrefix = MetricPrefix.none;
        public MetricPrefix StopPrefix = MetricPrefix.none;
        public MetricPrefix StepPrefix = MetricPrefix.none;
        public StepType stepType;
        public decimal StartValue
        {
            set
            {
                this._StartValue = Convert.ToDouble(value) * Math.Pow(10, Convert.ToDouble(StartPrefix));
            }
            get
            {
                return Convert.ToDecimal(this._StartValue / Math.Pow(10, Convert.ToDouble(StartPrefix)));
            }
        }
        public decimal StopValue
        {
            set
            {
                this._StopValue = Convert.ToDouble(value) * Math.Pow(10, Convert.ToDouble(StopPrefix));
            }
            get
            {
                return Convert.ToDecimal(this._StopValue / Math.Pow(10, Convert.ToDouble(StopPrefix)));
            }
        }
        public decimal StepValue
        {
            set
            {
                this._StepValue = Convert.ToDouble(value) * Math.Pow(10, Convert.ToDouble(StepPrefix));
            }
            get
            {
                return Convert.ToDecimal(this._StepValue / Math.Pow(10, Convert.ToDouble(StepPrefix)));
            }
        }
        private double _StartValue;
        private double _StopValue;
        private double _StepValue;
        public bool IsLogarithmic;

        private List<double> MeasurementPoints;

        private CancellationTokenSource ControllerWorker_ts;
        private CancellationToken ControllerWorker_ct;
        public override void Start()
        {
            MeasurementPoints = new List<double>();
            // Calculate Measurement Points
            double CalcStep;
            double currpoint;
            double i = 0;
            switch(stepType)
            {
                case StepType.IncrementDecrement:
                    if(_StartValue < _StopValue)
                    {
                        for (currpoint = _StartValue; currpoint <= _StopValue; currpoint += Math.Abs(_StepValue))
                        {
                            MeasurementPoints.Add(currpoint);
                        }
                    }
                    else
                    {
                        for (currpoint = _StartValue; currpoint >= _StopValue; currpoint -= Math.Abs(_StepValue))
                        {
                            MeasurementPoints.Add(currpoint);
                        }
                    }
                    break;
                case StepType.NumberOfPoints:
                    CalcStep = Math.Abs((_StopValue - _StartValue) / _StepValue);
                    if (_StartValue < _StopValue)
                    {
                        for (currpoint = _StartValue; currpoint <= _StopValue; currpoint += CalcStep)
                        {
                            MeasurementPoints.Add(currpoint);
                        }
                    }
                    else
                    {
                        for (currpoint = _StartValue; currpoint >= _StopValue; currpoint -= CalcStep)
                        {
                            MeasurementPoints.Add(currpoint);
                        }
                    }
                    break;
                case StepType.PointsPerDecade:
                    if (_StartValue < _StopValue)
                    {
                        currpoint = _StartValue;
                        do
                        {
                            MeasurementPoints.Add(currpoint);
                            i++;
                            currpoint = _StartValue * Math.Pow(10, (double)i / this._StepValue);
                        }
                        while (currpoint <= _StopValue);
                    }
                    else
                    {
                        i = Math.Log10(_StartValue / _StopValue) * _StepValue;
                        currpoint = _StartValue;
                        do
                        {
                            MeasurementPoints.Add(currpoint);
                            i--;
                            currpoint = _StopValue * Math.Pow(10, (double)i / this._StepValue);
                        }
                        while (currpoint <= _StopValue);
                    }
                    break;
            }



            this.State = ControllerState.Running;
            // initialize the devices (instrument, filter, target)
            InitDevices();
            ControllerWorker_ts = new CancellationTokenSource();
            ControllerWorker_ct = ControllerWorker_ts.Token;
            Task.Run((Action)ControllerWorker, ControllerWorker_ct);
        }
        public override void Stop()
        {
            ControllerWorker_ts.Cancel();
        }
        private void ControllerWorker()
        {
            MeasurementObject results;
            // int SequenceNumber = 1;
            for(int SequenceNumber = 0; SequenceNumber < MeasurementPoints.Count && !ControllerWorker_ct.IsCancellationRequested; SequenceNumber++)
            {
                results = new MeasurementObject();
                results.TimeStamp = DateTime.Now;
                results.SequenceNumber = SequenceNumber;
                results.Add(this._guid, 0, new MeasurementValue(MeasurementPoints[SequenceNumber]));
                RunMeasurementCycle(results);
            }
            this.State = ControllerState.Stopped;
        }
    }
    public enum StepType
    {
        [Description("Number of Points")]
        NumberOfPoints,
        [Description("Points/Decade")]
        PointsPerDecade,
        [Description("Increment/Decrement")]
        IncrementDecrement
    }
}
