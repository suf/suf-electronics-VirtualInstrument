﻿namespace SUF.Instrument.Standard
{
    partial class FormControllerSettingsVariable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblStart = new System.Windows.Forms.Label();
            this.lblStop = new System.Windows.Forms.Label();
            this.numStart = new System.Windows.Forms.NumericUpDown();
            this.numStop = new System.Windows.Forms.NumericUpDown();
            this.numStep = new System.Windows.Forms.NumericUpDown();
            this.cbxStepType = new System.Windows.Forms.ComboBox();
            this.cbxStartUnitPrefix = new System.Windows.Forms.ComboBox();
            this.cbxStopUnitPrefix = new System.Windows.Forms.ComboBox();
            this.cbxUnit = new System.Windows.Forms.ComboBox();
            this.cbxStepUnitPrefix = new System.Windows.Forms.ComboBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.chkLog = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStep)).BeginInit();
            this.SuspendLayout();
            // 
            // lblStart
            // 
            this.lblStart.AutoSize = true;
            this.lblStart.Location = new System.Drawing.Point(12, 9);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(32, 13);
            this.lblStart.TabIndex = 0;
            this.lblStart.Text = "Start:";
            // 
            // lblStop
            // 
            this.lblStop.AutoSize = true;
            this.lblStop.Location = new System.Drawing.Point(12, 67);
            this.lblStop.Name = "lblStop";
            this.lblStop.Size = new System.Drawing.Size(32, 13);
            this.lblStop.TabIndex = 1;
            this.lblStop.Text = "Stop:";
            // 
            // numStart
            // 
            this.numStart.DecimalPlaces = 3;
            this.numStart.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numStart.Location = new System.Drawing.Point(140, 7);
            this.numStart.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numStart.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numStart.Name = "numStart";
            this.numStart.Size = new System.Drawing.Size(120, 20);
            this.numStart.TabIndex = 2;
            // 
            // numStop
            // 
            this.numStop.DecimalPlaces = 3;
            this.numStop.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numStop.Location = new System.Drawing.Point(139, 65);
            this.numStop.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numStop.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numStop.Name = "numStop";
            this.numStop.Size = new System.Drawing.Size(120, 20);
            this.numStop.TabIndex = 3;
            // 
            // numStep
            // 
            this.numStep.DecimalPlaces = 3;
            this.numStep.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numStep.Location = new System.Drawing.Point(139, 35);
            this.numStep.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numStep.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.numStep.Name = "numStep";
            this.numStep.Size = new System.Drawing.Size(120, 20);
            this.numStep.TabIndex = 4;
            // 
            // cbxStepType
            // 
            this.cbxStepType.FormattingEnabled = true;
            this.cbxStepType.Location = new System.Drawing.Point(12, 34);
            this.cbxStepType.Name = "cbxStepType";
            this.cbxStepType.Size = new System.Drawing.Size(121, 21);
            this.cbxStepType.TabIndex = 5;
            this.cbxStepType.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbxStepType_Format);
            // 
            // cbxStartUnitPrefix
            // 
            this.cbxStartUnitPrefix.FormattingEnabled = true;
            this.cbxStartUnitPrefix.Location = new System.Drawing.Point(266, 6);
            this.cbxStartUnitPrefix.Name = "cbxStartUnitPrefix";
            this.cbxStartUnitPrefix.Size = new System.Drawing.Size(51, 21);
            this.cbxStartUnitPrefix.TabIndex = 6;
            this.cbxStartUnitPrefix.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.UnitPrefix_Format);
            // 
            // cbxStopUnitPrefix
            // 
            this.cbxStopUnitPrefix.FormattingEnabled = true;
            this.cbxStopUnitPrefix.Location = new System.Drawing.Point(266, 64);
            this.cbxStopUnitPrefix.Name = "cbxStopUnitPrefix";
            this.cbxStopUnitPrefix.Size = new System.Drawing.Size(51, 21);
            this.cbxStopUnitPrefix.TabIndex = 7;
            this.cbxStopUnitPrefix.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.UnitPrefix_Format);
            // 
            // cbxUnit
            // 
            this.cbxUnit.FormattingEnabled = true;
            this.cbxUnit.Location = new System.Drawing.Point(334, 34);
            this.cbxUnit.Name = "cbxUnit";
            this.cbxUnit.Size = new System.Drawing.Size(121, 21);
            this.cbxUnit.TabIndex = 8;
            this.cbxUnit.SelectedIndexChanged += new System.EventHandler(this.cbxUnit_SelectedIndexChanged);
            this.cbxUnit.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbxUnit_Format);
            // 
            // cbxStepUnitPrefix
            // 
            this.cbxStepUnitPrefix.FormattingEnabled = true;
            this.cbxStepUnitPrefix.Location = new System.Drawing.Point(266, 35);
            this.cbxStepUnitPrefix.Name = "cbxStepUnitPrefix";
            this.cbxStepUnitPrefix.Size = new System.Drawing.Size(51, 21);
            this.cbxStepUnitPrefix.TabIndex = 9;
            this.cbxStepUnitPrefix.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.UnitPrefix_Format);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(12, 103);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(72, 23);
            this.btnOk.TabIndex = 10;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(90, 103);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // chkLog
            // 
            this.chkLog.AutoSize = true;
            this.chkLog.Location = new System.Drawing.Point(336, 10);
            this.chkLog.Name = "chkLog";
            this.chkLog.Size = new System.Drawing.Size(80, 17);
            this.chkLog.TabIndex = 12;
            this.chkLog.Text = "Logarithmic";
            this.chkLog.UseVisualStyleBackColor = true;
            this.chkLog.CheckedChanged += new System.EventHandler(this.chkLog_CheckedChanged);
            // 
            // FormControllerSettingsVariable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 132);
            this.Controls.Add(this.chkLog);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.cbxStepUnitPrefix);
            this.Controls.Add(this.cbxUnit);
            this.Controls.Add(this.cbxStopUnitPrefix);
            this.Controls.Add(this.cbxStartUnitPrefix);
            this.Controls.Add(this.cbxStepType);
            this.Controls.Add(this.numStep);
            this.Controls.Add(this.numStop);
            this.Controls.Add(this.numStart);
            this.Controls.Add(this.lblStop);
            this.Controls.Add(this.lblStart);
            this.Name = "FormControllerSettingsVariable";
            this.Text = "Variable Controller";
            this.Load += new System.EventHandler(this.FormControllerSettingsVariable_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStep)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.Label lblStop;
        private System.Windows.Forms.NumericUpDown numStart;
        private System.Windows.Forms.NumericUpDown numStop;
        private System.Windows.Forms.NumericUpDown numStep;
        private System.Windows.Forms.ComboBox cbxStepType;
        private System.Windows.Forms.ComboBox cbxStartUnitPrefix;
        private System.Windows.Forms.ComboBox cbxStopUnitPrefix;
        private System.Windows.Forms.ComboBox cbxUnit;
        private System.Windows.Forms.ComboBox cbxStepUnitPrefix;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox chkLog;
    }
}