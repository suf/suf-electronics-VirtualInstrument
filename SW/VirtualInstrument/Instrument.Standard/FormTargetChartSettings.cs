﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using SUF.Winforms.Controls;

namespace SUF.Instrument.Standard
{
    public partial class FormTargetChartSettings : TargetSettingsFormBase, IResultConsumerSettings
    {
        public FormTargetChartSettings()
        {
            InitializeComponent();
        }
        public void SetAvailableResults(List<ResultDescriptor> resultDescriptors)
        {
            DataGridViewRow row;
            if (base.target != null)
            {
                if (((FormTargetChart)base.target).ChartSeriesDescriptors != null)
                {
                    foreach (ChartSeriesDescriptor descriptor in ((FormTargetChart)base.target).ChartSeriesDescriptors)
                    {
                        if(descriptor.SeriesResultDescriptor != null)
                        {
                            foreach (ResultDescriptor rd in resultDescriptors)
                            {
                                if (descriptor.SeriesResultDescriptor.SourceGuid == rd.SourceGuid && descriptor.SeriesResultDescriptor.ChannelID == rd.ChannelID)
                                {
                                    row = (DataGridViewRow)this.dgvChartSettings.Rows[this.dgvChartSettings.NewRowIndex].Clone();
                                    row.Cells[0].Value = descriptor.SeriesResultDescriptor;
                                    row.Cells[1].Value = descriptor.ChartSeries.Color;
                                    this.dgvChartSettings.Rows.Add(row);
                                }
                            }
                        }
                    }
                }
            }
            this.colSource.ValueType = typeof(ResultDescriptor);
            this.colSource.DisplayMember = "Name";
            this.colSource.ValueMember = "Value";
            this.colSource.DataSource = resultDescriptors;

            int ItemID;
            this.cbxAxisXSource.Items.Clear();
            this.cbxAxisXSource.Items.Add("Sequence Number");
            foreach (ResultDescriptor rd in resultDescriptors)
            {
                ItemID = this.cbxAxisXSource.Items.Add(rd);
                if (base.target != null)
                {
                    if (((FormTargetChart)base.target).AxisXresultDescriptor != null)
                    {
                        if (((FormTargetChart)base.target).AxisXresultDescriptor.SourceGuid == rd.SourceGuid && ((FormTargetChart)base.target).AxisXresultDescriptor.ChannelID == rd.ChannelID)
                        {
                            this.cbxAxisXSource.SelectedIndex = ItemID;
                        }
                    }
                }
            }
        }

        private void FormTargetChartSettings_Load(object sender, EventArgs e)
        {
            if (base.target == null)
            {
                base.target = new FormTargetChart();
            }
            this.chkLogarithmic.Checked = ((FormTargetChart)base.target).AxisXLogarithmic;
        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            ChartSeriesDescriptor descriptor;
            DataGridViewRow row;
            ((FormTargetChart)base.target).ChartSeriesDescriptors.Clear();
            for(int i = 0; i < this.dgvChartSettings.Rows.Count;i++)
            {
                if(i != this.dgvChartSettings.NewRowIndex)
                {
                    row = this.dgvChartSettings.Rows[i];
                    descriptor = new ChartSeriesDescriptor();
                    descriptor.SeriesResultDescriptor = (ResultDescriptor)row.Cells["colSource"].Value;
                    descriptor.ChartSeries = new Series
                    {
                        Name = ((ResultDescriptor)row.Cells["colSource"].Value).Name,
                        Color = (System.Drawing.Color)row.Cells["colColor"].Value,
                        IsVisibleInLegend = false,
                        IsXValueIndexed = true,
                        ChartType = SeriesChartType.Line
                    };
                    ((FormTargetChart)base.target).ChartSeriesDescriptors.Add(descriptor);
                }
            }
            if(this.cbxAxisXSource.SelectedIndex > 0)
            {
                ((FormTargetChart)base.target).AxisXresultDescriptor = (ResultDescriptor)this.cbxAxisXSource.SelectedItem;
            }
            ((FormTargetChart)base.target).AxisXUseSequence = (this.cbxAxisXSource.SelectedIndex < 1);
            ((FormTargetChart)base.target).AxisXLogarithmic = this.chkLogarithmic.Checked;
            base.IsSaved = true;
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
