﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace SUF.Instrument.Standard
{
    [PluginDescriptor("Digital Display")]
    [PluginSettingsClass("SUF.Instrument.Standard.FormTargetDigitalSettings")]
    public partial class FormTargetDigital : Form, ITarget, IResultConsumer
    {
        public ResultDescriptor resultDescriptor;

        public int resultId;
        public MeasureUnit unit;
        public int Digits = 6;
        private double Min = Double.MaxValue;
        private double Max = Double.MinValue;
        private double Avg;
        private int AvgCount = 0;
        // private string _name;
        public FormTargetDigital()
        {
            InitializeComponent();
        }

        public string GetName()
        {
            return this.lblName.Text;
        }
        
        public void Init()
        {
            this.unit = resultDescriptor.unit;
            if(!this.Visible)
            {
                this.Show();
            }
            // also clear the collected data !!!
        }
        
        public void ProcessResult(MeasurementObject results)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<MeasurementObject>(ProcessResult), new object[] { results });
                return;
            }
            MeasurementValue mValue = results.Get(this.resultDescriptor);
            // handle the measurement prefix - maybe remove the prefix from the MeasurementValue object and calculate for display here?
            // this.lblDisplay.Text = results.Get(this.resultDescriptor).value.ToString() + " " + this.unit.GetDescription();

            // Main display
            this.lblDisplay.Text = mValue.ToString(this.Digits) + this.unit.GetDescription();
            // Min
            if(mValue.value < this.Min)
            {
                this.Min = mValue.value;
            }
            this.lblMinValue.Text = MeasurementValue.DoubleToMeasurementString(this.Min, this.Digits) + this.unit.GetDescription();
            // Max
            if (mValue.value > this.Max)
            {
                this.Max = mValue.value;
            }
            this.lblMaxValue.Text = MeasurementValue.DoubleToMeasurementString(this.Max, this.Digits) + this.unit.GetDescription();
            // Avg
            if(this.AvgCount == 0)
            {
                this.Avg = mValue.value;
            }
            else
            {
                this.Avg = ((this.Avg * this.AvgCount) + mValue.value) / (this.AvgCount + 1);
            }
            this.AvgCount++;
            this.lblAvgValue.Text = MeasurementValue.DoubleToMeasurementString(this.Avg, this.Digits) + this.unit.GetDescription();
        }

        public void SetName(string name)
        {
            this.lblName.Text = name;
        }

        private void FormTargetDigital_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            // Allow to hide it. Handle the reshow it in the target datagridvioew
        }

        private void BtnAggregateReset_Click(object sender, EventArgs e)
        {
            this.Min = Double.MaxValue;
            this.Max = Double.MinValue;
            this.AvgCount = 0;
        }
    }
}
