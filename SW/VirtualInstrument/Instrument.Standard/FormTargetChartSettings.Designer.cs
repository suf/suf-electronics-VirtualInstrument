﻿namespace SUF.Instrument.Standard
{
    partial class FormTargetChartSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.dgvChartSettings = new System.Windows.Forms.DataGridView();
            this.chkLogarithmic = new System.Windows.Forms.CheckBox();
            this.colSource = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colColor = new SUF.Winforms.Controls.DataGridViewColorPickerColumn();
            this.colLogarithmic = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cbxAxisXSource = new System.Windows.Forms.ComboBox();
            this.lblAxisXSource = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChartSettings)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(93, 420);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 30);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(12, 420);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 29);
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.BtnOk_Click);
            // 
            // dgvChartSettings
            // 
            this.dgvChartSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvChartSettings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colSource,
            this.colColor,
            this.colLogarithmic});
            this.dgvChartSettings.Location = new System.Drawing.Point(12, 61);
            this.dgvChartSettings.Name = "dgvChartSettings";
            this.dgvChartSettings.Size = new System.Drawing.Size(497, 353);
            this.dgvChartSettings.TabIndex = 6;
            // 
            // chkLogarithmic
            // 
            this.chkLogarithmic.AutoSize = true;
            this.chkLogarithmic.Location = new System.Drawing.Point(220, 8);
            this.chkLogarithmic.Name = "chkLogarithmic";
            this.chkLogarithmic.Size = new System.Drawing.Size(78, 17);
            this.chkLogarithmic.TabIndex = 7;
            this.chkLogarithmic.Text = "Logarithnic";
            this.chkLogarithmic.UseVisualStyleBackColor = true;
            // 
            // colSource
            // 
            this.colSource.HeaderText = "Source";
            this.colSource.Name = "colSource";
            // 
            // colColor
            // 
            this.colColor.HeaderText = "Color";
            this.colColor.Name = "colColor";
            // 
            // colLogarithmic
            // 
            this.colLogarithmic.HeaderText = "Logarithmic";
            this.colLogarithmic.Name = "colLogarithmic";
            // 
            // cbxAxisXSource
            // 
            this.cbxAxisXSource.FormattingEnabled = true;
            this.cbxAxisXSource.Location = new System.Drawing.Point(93, 6);
            this.cbxAxisXSource.Name = "cbxAxisXSource";
            this.cbxAxisXSource.Size = new System.Drawing.Size(121, 21);
            this.cbxAxisXSource.TabIndex = 8;
            // 
            // lblAxisXSource
            // 
            this.lblAxisXSource.AutoSize = true;
            this.lblAxisXSource.Location = new System.Drawing.Point(12, 9);
            this.lblAxisXSource.Name = "lblAxisXSource";
            this.lblAxisXSource.Size = new System.Drawing.Size(76, 13);
            this.lblAxisXSource.TabIndex = 9;
            this.lblAxisXSource.Text = "X Axis Source:";
            // 
            // FormTargetChartSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 464);
            this.Controls.Add(this.lblAxisXSource);
            this.Controls.Add(this.cbxAxisXSource);
            this.Controls.Add(this.chkLogarithmic);
            this.Controls.Add(this.dgvChartSettings);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Name = "FormTargetChartSettings";
            this.Text = "Chart Settings";
            this.Load += new System.EventHandler(this.FormTargetChartSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvChartSettings)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.DataGridView dgvChartSettings;
        private System.Windows.Forms.DataGridViewComboBoxColumn colSource;
        private Winforms.Controls.DataGridViewColorPickerColumn colColor;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colLogarithmic;
        private System.Windows.Forms.CheckBox chkLogarithmic;
        private System.Windows.Forms.ComboBox cbxAxisXSource;
        private System.Windows.Forms.Label lblAxisXSource;
    }
}