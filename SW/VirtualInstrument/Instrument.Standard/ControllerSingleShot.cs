﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SUF.Instrument;

namespace SUF.Instrument.Standard
{
    [PluginDescriptor("Single Shot Controller")]
    public class ControllerSingleShot : Controller
    {
        public ControllerSingleShot()
        {

        }
        public override void Start()
        {
            // initialize the measurement object
            MeasurementObject results = new MeasurementObject();
            results.TimeStamp = DateTime.Now;
            results.SequenceNumber = 1;
            // initialize the devices (instrument, filter, target)
            InitDevices();
            // Do the measurement sequence
            if (!this.Async)
            {
                RunMeasurementCycle(results);
                this.State = ControllerState.Stopped;
            }
        }
    }
}
