﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SUF.Instrument.Standard
{
    public partial class FormControllerSettingsVariable : ControllerSettingsFormBase
    {
        public FormControllerSettingsVariable()
        {
            InitializeComponent();
        }

        private void FormControllerSettingsVariable_Load(object sender, EventArgs e)
        {
            if(controller == null)
            {
                controller = new ControllerVariable();
            }
            UpdateEnumComboBox<MeasureUnit>(this.cbxUnit, ((ControllerVariable)this.controller).sourceDescriptors[0].unit);
            UpdateEnumComboBox<MetricPrefix>(this.cbxStartUnitPrefix, ((ControllerVariable)this.controller).StartPrefix);
            UpdateEnumComboBox<MetricPrefix>(this.cbxStopUnitPrefix, ((ControllerVariable)this.controller).StopPrefix);
            UpdateEnumComboBox<MetricPrefix>(this.cbxStepUnitPrefix, ((ControllerVariable)this.controller).StepPrefix);
            this.chkLog.Checked = ((ControllerVariable)this.controller).IsLogarithmic;
            this.IsLogarithmic = ((ControllerVariable)this.controller).IsLogarithmic;
            this.numStart.Value = ((ControllerVariable)this.controller).StartValue;
            this.numStop.Value = ((ControllerVariable)this.controller).StopValue;
            this.numStep.Value = ((ControllerVariable)this.controller).StepValue;
            UpdateVisualElements();
        }
        private bool UnitPrefixVisible
        {
            set
            {
                this.cbxStartUnitPrefix.Visible = value;
                this.cbxStopUnitPrefix.Visible = value;
                this.cbxStepUnitPrefix.Visible = value;
            }
        }
        private bool IsLogarithmic
        {
            set
            {
                StepType stepType = this.cbxStepType.SelectedItem is StepType ? ((StepType)this.cbxStepType.SelectedItem) : ((ControllerVariable)base.controller).stepType;
                if(value)
                {
                    this.cbxStepType.Items.Clear();
                    this.cbxStepType.Items.Add(StepType.PointsPerDecade);
                    this.cbxStepType.SelectedItem = StepType.PointsPerDecade;
                }
                else
                {
                    this.cbxStepType.Items.Clear();
                    this.cbxStepType.Items.Add(StepType.NumberOfPoints);
                    this.cbxStepType.Items.Add(StepType.IncrementDecrement);
                    this.cbxStepType.SelectedItem = stepType == StepType.PointsPerDecade ? StepType.NumberOfPoints : stepType;
                }
            }
        }
        private void UpdateVisualElements()
        {
            switch((MeasureUnit)this.cbxUnit.SelectedItem)
            {
                case MeasureUnit.ampere:
                    this.chkLog.Visible = true;
                    UnitPrefixVisible = true;
                    break;
                case MeasureUnit.celsius:
                    this.chkLog.Visible = false;
                    this.chkLog.Checked = false;
                    UnitPrefixVisible = false;
                    break;
                case MeasureUnit.dBm:
                    this.chkLog.Visible = false;
                    this.chkLog.Checked = false;
                    UnitPrefixVisible = false;
                    break;
                case MeasureUnit.dBu:
                    this.chkLog.Visible = false;
                    this.chkLog.Checked = false;
                    UnitPrefixVisible = false;
                    break;
                case MeasureUnit.dBV:
                    this.chkLog.Visible = false;
                    this.chkLog.Checked = false;
                    UnitPrefixVisible = false;
                    break;
                case MeasureUnit.decibel:
                    this.chkLog.Visible = false;
                    this.chkLog.Checked = false;
                    UnitPrefixVisible = false;
                    break;
                case MeasureUnit.degree:
                    this.chkLog.Visible = false;
                    this.chkLog.Checked = false;
                    UnitPrefixVisible = false;
                    break;
                case MeasureUnit.farad:
                    this.chkLog.Visible = true;
                    this.chkLog.Checked = false;
                    UnitPrefixVisible = true;
                    break;
                case MeasureUnit.farenheit:
                    this.chkLog.Visible = false;
                    this.chkLog.Checked = false;
                    UnitPrefixVisible = false;
                    break;
                case MeasureUnit.henry:
                    this.chkLog.Visible = true;
                    UnitPrefixVisible = true;
                    break;
                case MeasureUnit.hertz:
                    this.chkLog.Visible = true;
                    UnitPrefixVisible = true;
                    break;
                case MeasureUnit.kelvin:
                    this.chkLog.Visible = false;
                    this.chkLog.Checked = false;
                    UnitPrefixVisible = false;
                    break;
                case MeasureUnit.none:
                    this.chkLog.Visible = false;
                    this.chkLog.Checked = false;
                    UnitPrefixVisible = false;
                    break;
                case MeasureUnit.ohm:
                    this.chkLog.Visible = true;
                    UnitPrefixVisible = true;
                    break;
                case MeasureUnit.percent:
                    this.chkLog.Visible = false;
                    this.chkLog.Checked = false;
                    UnitPrefixVisible = false;
                    break;
                case MeasureUnit.second:
                    this.chkLog.Visible = false;
                    this.chkLog.Checked = false;
                    UnitPrefixVisible = true;
                    break;
                case MeasureUnit.unknown:
                    this.chkLog.Visible = true;
                    UnitPrefixVisible = true;
                    break;
                case MeasureUnit.volt:
                    this.chkLog.Visible = true;
                    UnitPrefixVisible = true;
                    break;
                case MeasureUnit.watt:
                    this.chkLog.Visible = true;
                    UnitPrefixVisible = true;
                    break;
            }
        }

        private void cbxUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateVisualElements();
        }

        private void chkLog_CheckedChanged(object sender, EventArgs e)
        {
            IsLogarithmic = this.chkLog.Checked;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            ((ControllerVariable)this.controller).sourceDescriptors[0].unit = (MeasureUnit)this.cbxUnit.SelectedItem;
            ((ControllerVariable)this.controller).sourceDescriptors[0].ChannelName = ((MeasureUnit)this.cbxUnit.SelectedItem).GetDescription();
            ((ControllerVariable)this.controller).StartPrefix = (MetricPrefix)this.cbxStartUnitPrefix.SelectedItem;
            ((ControllerVariable)this.controller).StopPrefix = (MetricPrefix)this.cbxStopUnitPrefix.SelectedItem;
            ((ControllerVariable)this.controller).StepPrefix = (MetricPrefix)this.cbxStepUnitPrefix.SelectedItem;
            ((ControllerVariable)this.controller).IsLogarithmic = this.chkLog.Checked;
            ((ControllerVariable)this.controller).stepType = (StepType)this.cbxStepType.SelectedItem;
            ((ControllerVariable)this.controller).StartValue = this.numStart.Value;
            ((ControllerVariable)this.controller).StopValue = this.numStop.Value;
            ((ControllerVariable)this.controller).StepValue = this.numStep.Value;
            base.IsSaved = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbxStepType_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((StepType)e.ListItem).GetDescription();
        }

        private void UnitPrefix_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((MetricPrefix)e.ListItem).GetDescription();
        }

        private void cbxUnit_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((MeasureUnit)e.ListItem).GetDescription();
        }
    }
}
